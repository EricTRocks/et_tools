"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# ET_GrabBake_1.0 Plugin

# Module imports
from win32com.client import constants as c
from win32com.client.dynamic import Dispatch as d
import sys
import traceback

# Globals
xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

DEBUG = 0

# ================
# Exceptions
# ================

def GrabBake_Exception( exc, exc_traceback ):
    log("Unexpected Naming Error Occured! ================================", 2)
    log("Message: " + traceback.format_exception_only(type(exc),exc)[0], 2)
    log("Line: " + str(exc_traceback.tb_lineno), 2)
    log("".join(["Full Strack Trace:\n"] + [x for x in traceback.format_tb(exc_traceback, 2)]), 2)

# ============ Plugin Registrar ============ #
def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric Thivierge"
    in_reg.Name = "ET_GrabBake"
    in_reg.Email = ""
    in_reg.URL = ""
    in_reg.Major = 1
    in_reg.Minor = 1
    
    # ====== Controls ====== #
    in_reg.RegisterMenu(c.siMenuMCPAnimationID,"ET_GrabBake_Menu",False)
    in_reg.RegisterCommand("ET_GrabBake","ET_GrabBake")
    
    return True


def ET_GrabBake_Menu_Init(in_ctxt):
    oMenu = in_ctxt.Source
    oMenu.AddCommandItem("ET_GrabBake","ET_GrabBake")
            
def ET_GrabBake_Init(in_ctxt):
    oCmd = in_ctxt.Source
    oCmd.Description = "Bakes positions of objects being picked up without using constraints."
    oCmd.ReturnValue = False
    
    return True
    
def ET_GrabBake_Execute():
    
    try:
        xsi.Preferences.SetPreferenceValue("scripting.cmdlog",False)
        xsi.BeginUndo("ET_GrabBake")
    
        oPlayControl = xsi.ActiveProject.Properties("Play Control")
        fInitFrame = oPlayControl.Parameters("Current").Value
        
        if collSel.Count < 2:
            strError = "You must have 2 transformable objects selected!"
            XSIUIToolkit.MsgBox(strError,c.siMsgOkOnly,"Grab Bake")
            raise Exception(strError)
        
        oObj1 = collSel(0)
        oObj2 = collSel(1)

        lAnimFrames, collKeyableAnimParams = getSmartBakeData(oObj1)

        # Create Modal Property and Parameters
        oProp = XSIFactory.CreateObject("CustomProperty")
        oProp.Name = "Grab Bake"
        oProp.AddParameter3("FirstFrame",c.siInt4,min(lAnimFrames),oPlayControl.Parameters("In").Value,oPlayControl.Parameters("Out").Value,False)
        oProp.AddParameter3("LastFrame",c.siInt4,max(lAnimFrames),oPlayControl.Parameters("In").Value,oPlayControl.Parameters("Out").Value,False)
        oProp.AddParameter3("bSmartBake",c.siBool,False,None,None,False)
        
        oLayout = oProp.PPGLayout
        oLayout.AddItem("FirstFrame")
        oLayout.AddItem("LastFrame")
        oLayout.AddItem("bSmartBake","Smart Bake")

        oModal = xsi.InspectObj(oProp,None,"Renamer",4,False)

        if oModal != False: # Check to see if the user cancels the Modal PPG
            raise Exception("User Cancelled.")
        
        intFirstFrame = oProp.Parameters("FirstFrame").Value
        intLastFrame = oProp.Parameters("LastFrame").Value
        bSmartBake = oProp.Parameters("bSmartBake").Value

        xformObj1 = oObj1.Kinematics.Global.GetTransform2(intFirstFrame)
        xformObj2 = oObj2.Kinematics.Global.GetTransform2(intFirstFrame)
        xformNew = XSIMath.CreateTransform()
    
        xformMapped = XSIMath.MapWorldPoseToObjectSpace(oObj1.Kinematics.Global.GetTransform2(intFirstFrame),xformObj2)

        for eachFrame in range(intFirstFrame,intLastFrame + 1):

            if bSmartBake and eachFrame not in lAnimFrames:
                continue

            oPlayControl.Parameters("Current").Value = eachFrame
            
            xformNew.SetIdentity()
            xformNew.Copy(xformMapped)
            xformNew.MulInPlace(oObj1.Kinematics.Global.GetTransform2(eachFrame))
            
            oObj2.Kinematics.Global.PutTransform2(eachFrame,xformNew)
            
            for oSrcParam in collKeyableAnimParams:
                oKeyParam = oObj2.Parameters(oSrcParam.ScriptName)

                if oKeyParam:
                    oSrcFCurve = oSrcParam.Source
                    xsi.SaveKey(oKeyParam,eachFrame)

                    oTgtFCurve = oKeyParam.Source

                    """
                    TODO: Implement tangent matching
                    if eachFrame in lAnimFrames and eachFrame in [round(x.Time) for x in oSrcFCurve.Keys]:
                        oSrcKey = oSrcFCurve.GetKey(eachFrame,0.1)
                        oTgtKey = oTgtFCurve.GetKey(eachFrame,0.1)

                        oTgtKey.LeftTanX = oSrcKey.LeftTanX
                        oTgtKey.LeftTanY = oSrcKey.LeftTanY
                        oTgtKey.RightTanX = oSrcKey.RightTanX
                        oTgtKey.RightTanY = oSrcKey.RightTanY
                    """

        oPlayControl.Parameters("Current").Value = fInitFrame
        
    except Exception, e:
        GrabBake_Exception(e, sys.exc_info()[2])
        return False

    finally:
        xsi.EndUndo()

# =================
# Helper Functions
# =================
def getSmartBakeData(oTgtObj):

    collKeyableAnimParams = d("XSI.Collection")
    [collKeyableAnimParams.Add(x) for x in oTgtObj.AnimatedParameters2(c.siFCurveSource, True) if x.Keyable]
    
    lAnimFrames = []
    for eachParam in collKeyableAnimParams:
        oFCurve = eachParam.Source
        intMinFrame = oFCurve.GetMinKeyFrame()
        intMaxFrame = oFCurve.GetMaxKeyFrame()
        
        for i in xrange(intMinFrame,intMaxFrame + 1):
            if oFCurve.KeyExists(i,0.1):
                lAnimFrames.append(i)

    lAnimFrames = list(set(lAnimFrames))
    lAnimFrames.sort()
    
    return lAnimFrames, collKeyableAnimParams