"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# etRenamerPyQt
import traceback
from etRenamerUtils import etRenamer
from PyQt4 import QtGui, QtCore

DEBUG = False


# ================
# Classes
# ================
class etRenamerPyQt(QtGui.QMainWindow):
    
    def __init__(self, parent):
        QtGui.QMainWindow.__init__(self, parent)

        self.renamer = etRenamer.etRenamer()
        self.setObjectName("etRenamerUI")
        self.setWindowTitle("etRenamer")
        self.setGeometry(200, 200, 200, 300)
        self.intCurrentFunction = 0
        self.dConfigInputs = {}
        
        self.rxObjectTypeName = QtCore.QRegExp()
        self.rxObjectTypeName.setPattern("^[a-zA-Z]+$")
        self.validatorObjectType = QtGui.QRegExpValidator(self.rxObjectTypeName, self)

        self.rxTokens = QtCore.QRegExp()
        self.rxTokens.setPattern("[\w\[\]]*")
        self.validatorTokens = QtGui.QRegExpValidator(self.rxTokens, self)

        self.rxNewName = QtCore.QRegExp()
        self.rxNewName.setPattern("^[a-zA-Z0-9_]+$")
        self.validatorNewName = QtGui.QRegExpValidator(self.rxNewName, self)

        self.strStyleSheet = """
                                QMainWindow#etRenamerUI { background-color: #111; color: #FFF; }

                                QTabWidget { border: 0; }
                                QTabWidget::pane { background-color: #333; }
                                QTabWidget:tab-bar { left: 6px; margin: 0; padding: 0; }
                                QTabBar:tab { color: #EEE; margin: 3px 0 0 3px; border-top-left-radius: 3px; border-top-right-radius: 3px; padding: 3px 6px; }
                                QTabBar::tab:selected { background-color: #333; }
                                QTabBar::tab:!selected { background-color: #222; }

                                QWidget#tabRename { background-color: #333; border: 0px; }
                                QWidget#tabConfig { background-color: #333; border: 0px; }

                                QGroupBox { 
                                            background-color: #222;
                                            color: #EEE;
                                            border: 1px solid #666;
                                            border-top-left-radius: 0px;
                                            border-top-right-radius: 3px;
                                            border-bottom-right-radius: 3px;
                                            border-bottom-left-radius: 3px;
                                            margin-top: 1.5em;
                                            }

                                QGroupBox::title { 
                                                    background-color: #222;
                                                    border: solid #666;
                                                    border-top-left-radius: 3px;
                                                    border-top-right-radius: 3px;
                                                    border-width: 1px 1px 0 1px;
                                                    color: #EEE;
                                                    subcontrol-origin: margin;
                                                    subcontrol-position: top left;
                                                    top: 4px;
                                                    padding: 3px 3px 0 6px;
                                                    }

                                QLabel { background-color: #222; color: #EEE; }

                                QLineEdit { background-color: #444; border-radius: 3px; color: #EEE; }
                                QLineEdit:hover { background-color: #555; }

                                QComboBox { background-color: #444; border: 0px solid #626262; border-radius: 3px; color: #EEE; }
                                QComboBox::drop-down { background-color: #555; border: 0px solid #626262; border-radius: 3px; color: #EEE; padding: 0 0 0 4px; }
                                QComboBox QAbstractItemView { background-color: #555; border: 0px solid #626262; border-radius: 3px; color: #EEE; padding: 0 0 0 4px; }
                                QComboBox:hover { background-color: #555; border: 0.5px outset #444; }
                                
                                QVBoxLayout#layoutRename { background-color: #222; color: #EEE; }
                                QVBoxLayout#layoutNaming { background-color: #222; color: #EEE; }

                                QPushButton { 
                                                background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1,
                                                    stop: 0 #555,
                                                    stop: 1 #444 );
                                                border: 0;
                                                border-radius: 3px;
                                                color: #EEE;
                                                padding: 6px 3px;
                                                }
                                QPushButton:hover { 
                                                    background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1,
                                                    stop: 0 #666,
                                                    stop: 1 #555 );
                                                    border: 0.5px outset #444; }
                                QPushButton:pressed { 
                                                        background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1,
                                                        stop: 0 #666,
                                                        stop: 1 #555 );
                                                        border: 0.5px outset #444; }

                            """
        self.setStyleSheet(self.strStyleSheet)

        self.initUI()

    
    def initUI(self):
        
        try:
            self.layoutTabWidget = QtGui.QTabWidget(self)
            self.layoutTabWidget.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

            # ===========
            # Rename Tab
            # ===========
            self.widgetRename = QtGui.QWidget(self)
            self.widgetRename.setObjectName("tabRename")
            self.layoutRename = QtGui.QVBoxLayout()
            self.layoutRename.setObjectName("layoutRename")
            self.layoutRename.setSpacing(5)

            # Name group
            self.groupName = QtGui.QGroupBox("Naming")
            self.groupName.setObjectName("groupName")
            self.layoutNaming = QtGui.QVBoxLayout()
            self.layoutNaming.setObjectName("layoutNaming")
            self.layoutNaming.setSpacing(5)

            # Function combo
            self.labelFunction = QtGui.QLabel("Function:", self)
            self.labelFunction.setObjectName("labelFunction")
            self.labelFunction.setMinimumWidth(75)
            self.comboFunction = QtGui.QComboBox(self)
            self.comboFunction.setMinimumWidth(150)
            self.comboFunction.addItem("Rename")
            self.comboFunction.addItem("PrefixSuffix")
            self.comboFunction.addItem("SearchAndReplace")
            self.comboFunction.addItem("Strip")
            self.layoutFunction = QtGui.QGridLayout()
            self.layoutFunction.setColumnStretch(1, 1)
            self.layoutFunction.addWidget(self.labelFunction)
            self.layoutFunction.addWidget(self.comboFunction)
            self.layoutNaming.addLayout(self.layoutFunction)

            # Prefix input
            self.labelPrefix = QtGui.QLabel("Prefix:", self)
            self.labelPrefix.setMinimumWidth(75)
            self.inputPrefix = QtGui.QLineEdit(self)
            self.inputPrefix.setValidator(self.validatorNewName)
            self.layoutPrefix = QtGui.QHBoxLayout()
            self.layoutPrefix.addWidget(self.labelPrefix)
            self.layoutPrefix.addWidget(self.inputPrefix)
            self.layoutNaming.addLayout(self.layoutPrefix)

            # Name input
            self.labelNewName = QtGui.QLabel("Name:", self)
            self.labelNewName.setMinimumWidth(75)
            self.inputNewName = QtGui.QLineEdit(self)
            self.inputNewName.setValidator(self.validatorNewName)
            self.layoutNewName = QtGui.QHBoxLayout()
            self.layoutNewName.addWidget(self.labelNewName)
            self.layoutNewName.addWidget(self.inputNewName)
            self.layoutNaming.addLayout(self.layoutNewName)

            # Suffix input
            self.labelSuffix = QtGui.QLabel("Suffix:", self)
            self.labelSuffix.setMinimumWidth(75)
            self.inputSuffix = QtGui.QLineEdit(self)
            self.inputSuffix.setValidator(self.validatorNewName)
            self.layoutSuffix = QtGui.QHBoxLayout()
            self.layoutSuffix.addWidget(self.labelSuffix)
            self.layoutSuffix.addWidget(self.inputSuffix)
            self.layoutNaming.addLayout(self.layoutSuffix)

            self.groupName.setLayout(self.layoutNaming)
            self.layoutRename.addWidget(self.groupName)

            # Search input
            self.labelSearch = QtGui.QLabel("Search For:", self)
            self.labelSearch.setMinimumWidth(75)
            self.inputSearch = QtGui.QLineEdit(self)
            self.layoutSearch = QtGui.QHBoxLayout()
            self.layoutSearch.addWidget(self.labelSearch)
            self.layoutSearch.addWidget(self.inputSearch)
            self.layoutNaming.addLayout(self.layoutSearch)

            # Replace input
            self.labelReplace = QtGui.QLabel("Replace With:", self)
            self.labelReplace.setMinimumWidth(75)
            self.inputReplace = QtGui.QLineEdit(self)
            self.layoutReplace = QtGui.QHBoxLayout()
            self.layoutReplace.addWidget(self.labelReplace)
            self.layoutReplace.addWidget(self.inputReplace)
            self.layoutNaming.addLayout(self.layoutReplace)

            # Strip Method input
            self.labelStripMethod = QtGui.QLabel("From Beginning:", self)
            self.labelStripMethod.setMinimumWidth(75)
            self.inputStripMethod = QtGui.QCheckBox(self)
            self.layoutStrip = QtGui.QHBoxLayout()
            self.layoutStrip.addWidget(self.labelStripMethod)
            self.layoutStrip.addWidget(self.inputStripMethod)
            self.layoutNaming.addLayout(self.layoutStrip)

            # Strip Count input
            self.labelStripCount = QtGui.QLabel("Strip Count:", self)
            self.labelStripCount.setMinimumWidth(75)
            self.inputStripCount = QtGui.QSpinBox(self)
            self.inputStripCount.setMinimum(1)
            self.layoutStripCount = QtGui.QHBoxLayout()
            self.layoutStripCount.addWidget(self.labelStripCount)
            self.layoutStripCount.addWidget(self.inputStripCount)
            self.layoutNaming.addLayout(self.layoutStripCount)

            self.layoutRename.addSpacing(10)

            # Tokens group
            self.groupTokens = QtGui.QGroupBox("Tokens")
            self.layoutTokens = QtGui.QVBoxLayout()
            self.layoutTokens.setSpacing(5)

            # Padding combo
            self.labelPadding = QtGui.QLabel("Padding:", self)
            self.labelPadding.setMinimumWidth(75)
            self.comboPadding = QtGui.QComboBox(self)
            self.comboPadding.setMinimumWidth(150)
            self.comboPadding.addItem("Auto")
            self.comboPadding.addItem("#")
            self.comboPadding.addItem("##")
            self.comboPadding.addItem("###")
            self.comboPadding.addItem("####")
            self.comboPadding.addItem("None")
            self.comboPadding.setCurrentIndex(0)
            self.layoutPadding = QtGui.QGridLayout()
            self.layoutPadding.setColumnStretch(1, 1)
            self.layoutPadding.addWidget(self.labelPadding, 0, 0)
            self.layoutPadding.addWidget(self.comboPadding, 0, 1)

            # Side combo
            self.labelSide = QtGui.QLabel("Side:", self)
            self.labelSide.setMinimumWidth(75)
            self.comboSide = QtGui.QComboBox(self)
            self.comboSide.setMinimumWidth(150)
            self.comboSide.addItem("L")
            self.comboSide.addItem("M")
            self.comboSide.addItem("R")
            self.comboSide.setCurrentIndex(1)
            self.layoutSide = QtGui.QGridLayout()
            self.layoutSide.setColumnStretch(1, 1)
            self.layoutSide.addWidget(self.labelSide, 0, 0)
            self.layoutSide.addWidget(self.comboSide, 0, 1)

            # Object Type combo
            self.labelObjectType = QtGui.QLabel("Object Type:", self)
            self.labelObjectType.setMinimumWidth(75)
            self.comboObjectType = QtGui.QComboBox(self)
            self.comboObjectType.setMinimumWidth(150)
            
            self.renamer.getNameTemplate()
            lObjectTypes = sorted(self.renamer.nameTemplate.keys())
            for eachType in lObjectTypes:
                self.comboObjectType.addItem(eachType)
            self.comboObjectType.setCurrentIndex(self.comboObjectType.findText("Control"))
            self.layoutObjectType = QtGui.QGridLayout()
            self.layoutObjectType.setColumnStretch(1, 1)
            self.layoutObjectType.addWidget(self.labelObjectType, 0, 0)
            self.layoutObjectType.addWidget(self.comboObjectType, 0, 1)

            self.layoutTokens.addLayout(self.layoutPadding)
            self.layoutTokens.addLayout(self.layoutSide)
            self.layoutTokens.addLayout(self.layoutObjectType)
            self.groupTokens.setLayout(self.layoutTokens)

            self.layoutRename.addWidget(self.groupTokens)
            self.layoutRename.addSpacing(10)

            self.buttonRename = QtGui.QPushButton("Rename", self)
            self.buttonRename.clicked.connect(self.buttonRenameClicked)

            self.layoutRename.addWidget(self.buttonRename)
            self.layoutRename.addStretch()

            self.widgetRename.setLayout(self.layoutRename)


            # ===========
            # Config Tab
            # ===========
            self.widgetConfig = QtGui.QWidget(self)
            self.widgetConfig.setObjectName("tabConfig")
            self.layoutConfig = QtGui.QVBoxLayout()
            self.layoutConfig.setObjectName("layoutConfig")

            # Tokens group
            self.groupConfigSettings = QtGui.QGroupBox("Settings")
            self.layoutConfigSettings = QtGui.QVBoxLayout()
            self.layoutConfigSettings.setSpacing(5)

            # Tokens field
            if "Tokens" in self.renamer.nameTemplate.keys():
                strTokenValue = self.renamer.nameTemplate["Tokens"]

                oLabelConfigObjType = QtGui.QLabel("Tokens: ", self)
                oLabelConfigObjType.setMinimumWidth(75)
                oInputConfigObjType = QtGui.QLineEdit(self)
                oInputConfigObjType.setObjectName("configTokens_field")
                oInputConfigObjType.setText(str(strTokenValue))
                oInputConfigObjType.setValidator(self.validatorTokens)

                self.dConfigInputs["configTokens"] = oInputConfigObjType

                oLayoutConfigObjType = QtGui.QHBoxLayout()
                oLayoutConfigObjType.addWidget(oLabelConfigObjType)
                oLayoutConfigObjType.addWidget(oInputConfigObjType)
                self.layoutConfigSettings.addLayout(oLayoutConfigObjType)

            dObjectTypes = self.renamer.nameTemplate
            for eachType in [x for x in sorted(dObjectTypes) if x != "Tokens"]:
                oLabelConfigObjType = QtGui.QLabel(eachType + ": ", self)
                oLabelConfigObjType.setMinimumWidth(75)
                oInputConfigObjType = QtGui.QLineEdit(self)
                oInputConfigObjType.setObjectName("config" + eachType + "_field")
                oInputConfigObjType.setText(str(dObjectTypes[eachType]))
                oInputConfigObjType.setValidator(self.validatorObjectType)

                self.dConfigInputs["config" + eachType] = oInputConfigObjType

                oLayoutConfigObjType = QtGui.QHBoxLayout()
                oLayoutConfigObjType.addWidget(oLabelConfigObjType)
                oLayoutConfigObjType.addWidget(oInputConfigObjType)
                self.layoutConfigSettings.addLayout(oLayoutConfigObjType)

            self.groupConfigSettings.setLayout(self.layoutConfigSettings)
            self.layoutConfig.addWidget(self.groupConfigSettings)

            self.widgetConfig.setLayout(self.layoutConfig)

            self.buttonConfigSave = QtGui.QPushButton("Save", self)
            self.buttonConfigSave.clicked.connect(self.buttonConfigSaveClicked)

            #self.connect(self.buttonConfigSave, QtCore.SIGNAL("clicked"), self.buttonConfigSaveClicked)

            self.layoutConfig.addWidget(self.buttonConfigSave)
            self.layoutConfig.addStretch()


            # Signals
            self.connect(self.comboFunction, QtCore.SIGNAL("currentIndexChanged(int)"), self.updateUI)

            # Set Main Layout
            self.layoutTabWidget.addTab(self.widgetRename, "Rename")
            self.layoutTabWidget.addTab(self.widgetConfig, "Config")
            
            self.setCentralWidget(self.layoutTabWidget)
            self.updateUI()
            self.adjustSize()

        except Exception, e:
            self.printException(e, sys.exc_info()[2])
            self.close()


    def buttonRenameClicked(self):

        try:
            lNames = self.get3DObjectNames()

            self.renamer.initNames = lNames
            self.renamer.prefix = self.inputPrefix.text()
            self.renamer.newName = self.inputNewName.text()
            self.renamer.suffix = self.inputSuffix.text()
            
            if self.comboPadding.currentText() == "Auto":
                self.renamer.padding = len(str(len(self.renamer.initNames)))
            elif self.comboPadding.currentText() == "None":
                self.renamer.padding = 0
            else:
                self.renamer.padding = self.comboPadding.currentIndex()

            self.renamer.side = self.comboSide.currentText()
            self.renamer.objectType = self.comboObjectType.currentText()
            self.renamer.searchFor = self.inputSearch.text()
            self.renamer.replaceWith = self.inputReplace.text()
            self.renamer.strToken = self.dConfigInputs["configTokens"].text()

            self.renamer.stripFromBegin = self.inputStripMethod.isChecked()
            self.renamer.strip = self.inputStripCount.value()

            if self.intCurrentFunction == 0:
                self.renamer.rename()                
            elif self.intCurrentFunction == 1:
                self.renamer.prefixSuffix()
            elif self.intCurrentFunction == 2:
                self.renamer.searchAndReplace()
            elif self.intCurrentFunction == 3:
                self.renamer.stripName()

            for i, eachName in enumerate(lNames):
                strNewName = self.renamer.outNames[i]
                self.rename3DObjects(strNewName, i, eachName)

            return


        except Exception, e:
            self.printException(e, sys.exc_info()[2])


    def rename3DObjects(self, strNewName, i, eachName):
        """Implement in app sub-classes."""
        # xsi.Selection(i).Name =
        # pm.general.rename(eachName, self.renamer.outNames[i])

        return


    def get3DObjectNames(self):
        """Implement in app sub-classes."""
        lNames = []
        # lNames = [x.Name for x in xsi.Selection]
        # lNames = pm.ls(dag=True, sl=True, type="transform")

        return lNames


    def buttonConfigSaveClicked(self):
        """Save name template to disk."""

        try:            
            self.renamer.getNameTemplate()
            dNameTemplate = self.renamer.nameTemplate

            for eachItem in dNameTemplate.keys():
                dNameTemplate[eachItem] = str(self.dConfigInputs["config" + eachItem].text())

            self.renamer.writeNameTemplate()

            print "etRenamer config saved!"

            return

        except Exception, e:
            self.printException(e, sys.exc_info()[2])


    def updateUI(self):
        self.intCurrentFunction = self.comboFunction.currentIndex()

        if self.intCurrentFunction == 0: # Rename
            self.labelSearch.hide()
            self.inputSearch.hide()
            self.labelReplace.hide()
            self.inputReplace.hide()
            self.labelStripMethod.hide()
            self.inputStripMethod.hide()
            self.labelStripCount.hide()
            self.inputStripCount.hide()

            self.labelPrefix.show()
            self.inputPrefix.show()
            self.labelNewName.show()
            self.inputNewName.show()
            self.labelSuffix.show()
            self.inputSuffix.show()
            self.groupTokens.show()

        elif self.intCurrentFunction == 1: # PrefixSuffix
            self.labelSearch.hide()
            self.inputSearch.hide()
            self.labelNewName.hide()
            self.inputNewName.hide()
            self.labelReplace.hide()
            self.inputReplace.hide()
            self.groupTokens.hide()
            self.labelStripMethod.hide()
            self.inputStripMethod.hide()
            self.labelStripCount.hide()
            self.inputStripCount.hide()

            self.labelPrefix.show()
            self.inputPrefix.show()
            self.labelSuffix.show()
            self.inputSuffix.show()

        elif self.intCurrentFunction == 2: # SearchAndReplace
            self.labelPrefix.hide()
            self.inputPrefix.hide()
            self.labelNewName.hide()
            self.inputNewName.hide()
            self.labelSuffix.hide()
            self.inputSuffix.hide()
            self.groupTokens.hide()
            self.labelStripMethod.hide()
            self.inputStripMethod.hide()
            self.labelStripCount.hide()
            self.inputStripCount.hide()

            self.labelSearch.show()
            self.inputSearch.show()
            self.labelReplace.show()
            self.inputReplace.show()
            
        elif self.intCurrentFunction == 3: # Strip
            self.labelPrefix.hide()
            self.inputPrefix.hide()
            self.labelNewName.hide()
            self.inputNewName.hide()
            self.labelSuffix.hide()
            self.inputSuffix.hide()
            self.labelSearch.hide()
            self.inputSearch.hide()
            self.labelReplace.hide()
            self.inputReplace.hide()
            self.groupTokens.hide()

            self.labelStripMethod.show()
            self.inputStripMethod.show()
            self.labelStripCount.show()
            self.inputStripCount.show()
            pass

        self.adjustSize()

        
    def closeEvent(self, e):
        print "etRenamer Closed."

        return


class etRenamerPyQtXSI(etRenamerPyQt):
    """Sub-class of etRenamerPyQt."""
    
    def __init__(self, parent):
        etRenamerPyQt.__init__(self, parent)


    def printException( self, exc, exc_traceback ):
        """Prints exceptions for Softimage."""

        from win32com.client.dynamic import Dispatch as d
        xsi = d("XSI.Application").Application
        log = xsi.LogMessage

        log("Unexpected etRenamer Error Occured! ================================", 2)
        log("Message: " + traceback.format_exception_only(type(exc), exc)[0], 2)
        log("Line: " + str(exc_traceback.tb_lineno), 2)
        log("Full Strack Trace:", 2)
        log(traceback.format_tb( exc_traceback ), 2)


    def rename3DObjects(self, strNewName, i, eachName):
        """Implement in app sub-classes."""
        
        from win32com.client.dynamic import Dispatch as d
        xsi = d("XSI.Application").Application
        xsi.Selection(i).Name = strNewName
        
        return


    def get3DObjectNames(self):
        """Implement in app sub-classes."""
        
        from win32com.client.dynamic import Dispatch as d
        xsi = d("XSI.Application").Application
        lNames = [x.Name for x in xsi.Selection]

        return lNames