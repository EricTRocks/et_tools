"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# etRenamer
import os
import sys
import traceback
import re
import string
import json

DEBUG = False


# ================
# Classes
# ================
class etTemplate(string.Template):
    delimiter = "["
    pattern = r"""
    \[(?:
    (?P<escaped>\[)|
    (?P<named>[_a-z][_a-z0-9]*)\]|
    (?P<braced>[_a-z][_a-z0-9]*)\]|
    (?P<invalid>)
    )
    """


class etRenamer(object):
    """Renamer object."""

    def __init__(self):
        self.initNames = []
        self.prefix = ''
        self.newName = ''
        self.suffix = ''
        self.padding = 4
        self.side = 'M'
        self.objectType = ''
        self.searchFor = ''
        self.replaceWith = ''
        self.nameTemplate = None
        self.outNames = []
        self.strip = None
        self.stripFromBegin = False
        self.token = ''
        self.tokenMap = {}
        self.etRenamerPath = None

        return


    def printException( self, exc, exc_traceback ):
        print 'Unexpected etRenamer Error Occured! ================================'
        print 'Message: ' + traceback.format_exception_only(type(exc), exc)[0]
        print 'Line: ' + str(exc_traceback.tb_lineno)
        print 'Full Strack Trace:'
        print traceback.format_tb( exc_traceback )


    def getNameTemplate(self):
        """Reads the name template from the file."""

        try:
            etRenamerPath = os.path.join([s for s in sys.path if os.path.exists(os.path.join(s, 'etRenamerUtils'))][0], 'etRenamerUtils')
            self.etRenamerPath = os.path.join(etRenamerPath, 'NameTemplate.json')

            with open(self.etRenamerPath, 'r') as templateFile:
                self.nameTemplate = json.load(templateFile)

            return True

        except Exception, e:
            self.printException(e, sys.exc_info()[2])


    def writeNameTemplate(self):
        """Writes name template data to external file."""

        try:
            with open(self.etRenamerPath, 'w') as templateFile:
                json.dump(self.nameTemplate, templateFile, indent=2)

        except Exception, e:
            self.printException(e, sys.exc_info()[2])

        return True


    def rename(self):
        """Rename input names."""

        self.getNameTemplate()
        self.outNames = []

        self.tokenMap = {
                         'prefix': str(self.prefix),
                         'name': str(self.newName),
                         'suffix': str(self.suffix),
                         'side': str(self.side),
                         'type': self.nameTemplate[str(self.objectType)],
                         'padding': ''
                        }

        for i in xrange(len(self.initNames)):

            if self.newName == '':
                raise ValueError('Invalid value for newName')

            if self.padding == 0:
                self.tokenMap['padding'] = ''
            else:
                self.tokenMap['padding'] = str(i + 1).zfill(self.padding)

            tokenSplit = str(self.token).split('_')

            nameAssemble = []
            for eachItem in tokenSplit:
                tempRename = etTemplate(eachItem)
                tokenReplace = tempRename.substitute(self.tokenMap)
                if tokenReplace:
                    nameAssemble.append(tokenReplace)

            newName = '_'.join(nameAssemble)
            self.outNames.append(newName)

        return


    def prefixSuffix(self):
        """Appends the prefix and suffix to each name."""

        self.getNameTemplate()
        self.outNames = []

        for eachName in self.initNames:
            newName = str(self.prefix) + eachName + str(self.suffix)

            self.outNames.append(newName)

        return


    def searchAndReplace(self):
        """Search and replace input names."""

        self.getNameTemplate()
        self.outNames = []

        for eachName in self.initNames:
            newName = eachName.replace(str(self.searchFor), str(self.replaceWith))
            self.outNames.append(newName)

        return


    def stripName(self):
        """Strips input number of characters off the end of each name"""

        self.getNameTemplate()
        self.outNames = []

        for eachName in self.initNames:
            if self.stripFromBegin:
                newName = str(eachName)[self.strip:]
            else:
                newName = str(eachName)[:-self.strip]
            self.outNames.append(newName)

        return