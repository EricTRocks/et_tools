"""
Copyright (c) 2009-2014, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


from win32com.client import constants
from win32com.client import Dispatch

import re
import os
import sys
import traceback
import string

from etRenamerUtils import etRenamer

si = Application
log = si.LogMessage
sel = si.Selection

DEBUG = 0


def XSILoadPlugin( in_reg ):
    in_reg.Author = 'Eric Thivierge'
    in_reg.Name = 'etRenamer_xsi'
    in_reg.Major = 3
    in_reg.Minor = 2

    in_reg.RegisterProperty('etRenamer')
    in_reg.RegisterCommand('etRenamerOpen', 'etRenamerOpen')
    in_reg.RegisterMenu(constants.siMenuMCPEditID, 'etRenamer', False, True)
    #RegistrationInsertionPoint - do not remove this line

    return True


# ================
# Exceptions
# ================
def Naming_Exception(exc, exc_traceback):
    log('Unexpected Naming Error Occured! ================================', 2)
    log('Message: ' + traceback.format_exception_only(type(exc),exc)[0], 2)
    log('Line: ' + str(exc_traceback.tb_lineno), 2)
    log(''.join(['Full Strack Trace:\n'] + [x for x in traceback.format_tb(exc_traceback, 2)]), 2)


# ================
# Commands
# ================
def etRenamer_Init(in_ctxt):
    menu = in_ctxt.Source
    menu.AddCommandItem('etRenamer', 'etRenamerOpen')

    return True


# ================
# Menus
# ================
def etRenamerOpen_Init(in_ctxt):
    cmd = in_ctxt.Source
    cmd.Description = 'Opens the etRenamer'
    cmd.Tooltip = 'Opens the etRenamer'
    cmd.SetFlag(constants.siSupportsKeyAssignment, False)
    cmd.SetFlag(constants.siCannotBeUsedInBatch, True)
    cmd.ReturnValue = False

    return True


def etRenamerOpen_Execute():
    scnRoot = si.ActiveProject3.ActiveScene.Root
    renamerProp = scnRoot.AddProperty('etRenamer', False, 'etRenamer')
    popWindow(renamerProp, 200, 200, 300, 410)

    return True


def etRenamer_Define( in_ctxt ):

    prop = in_ctxt.Source
    prop.AddParameter3('Function', constants.siString, 'Rename', '', '', False, False)
    prop.AddParameter3('Component', constants.siString, '', '', '', False, False)
    prop.AddParameter3('Prefix', constants.siString, '', '', '', False, False)
    prop.AddParameter3('NewName', constants.siString, '', '', '', False, False)
    prop.AddParameter3('Suffix', constants.siString, '', '', '', False, False)
    prop.AddParameter3('Padding', constants.siInt4, 0, 0, 5, False, False)
    prop.AddParameter3('Side', constants.siString, 'M', '', '', False, False)
    prop.AddParameter3('objectType', constants.siString, 'Control', '', '', False, False)
    prop.AddParameter3('SearchFor', constants.siString, '', '', '', False, False)
    prop.AddParameter3('ReplaceWith', constants.siString, '', '', '', False, False)
    prop.AddParameter3('Strip', constants.siInt4, 1, 1, 20, False, False)
    prop.AddParameter3('StripFromBegin', constants.siBool, False, ', ', False, False)

    renamer = etRenamer.etRenamer()
    renamer.getNameTemplate()
    nameTemplate = renamer.nameTemplate

    for eachItem in nameTemplate:
        prop.AddParameter3('config' + eachItem, constants.siString, nameTemplate[eachItem], '', '', False, False)

    return True


def buildNamingUI():

    prop = PPG.Inspected(0)
    renamer = etRenamer.etRenamer()
    renamer.getNameTemplate()

    layout = PPG.PPGLayout
    layout.Clear()

    # Naming Tab
    layout.AddTab('Naming')
    layout.AddRow()
    layout.AddSpacer(10, 10)
    item = layout.AddEnumControl('Function', ['Rename', 'Rename', 'Prefix / Suffix', 'PrefixSuffix', 'Search / Replace', 'SearchAndReplace', 'Strip', 'Strip'])
    item.SetAttribute(constants.siUILabelMinPixels, 75)
    item.SetAttribute(constants.siUILabelPercentage, 15)
    layout.AddSpacer(10, 10)
    layout.EndRow()


    renameFunction = prop.Parameters('Function').Value
    if renameFunction == 'Rename':

        layout.AddGroup('Name')

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('Prefix','Prefix', constants.siControlString)
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('NewName','New Name', constants.siControlString)
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('Suffix','Suffix', constants.siControlString)
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()
        layout.EndGroup()

        layout.AddGroup('Tokens')
        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddEnumControl('Padding', ['Auto', 0, '#', 1, '##', 2, '###', 3, '####', 4, 'None', 5], 'Padding')
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddEnumControl('Side', ['L', 'L', 'M', 'M', 'R', 'R'], 'Side')
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

        layout.AddRow()
        layout.AddSpacer(10, 10)
        objectTypes = sorted([x for x in renamer.nameTemplate.keys() if x != 'Tokens'] * 2)
        item = layout.AddEnumControl('objectType', objectTypes, 'Object Type')
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()
        layout.EndGroup()

    elif renameFunction == 'PrefixSuffix':

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('Prefix', 'Prefix', constants.siControlString)
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('Suffix', 'Suffix', constants.siControlString)
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

    elif renameFunction == 'SearchAndReplace':

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('SearchFor', 'Search For', constants.siControlString)
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('ReplaceWith', 'Replace With', constants.siControlString)
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

    elif renameFunction == 'Strip':

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('StripFromBegin', 'From Beginning')
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem('Strip', 'Strip')
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

    layout.AddSpacer(10, 5)

    item = layout.AddButton('Rename', 'Rename')
    item.SetAttribute(constants.siUICX, 280)
    item.SetAttribute(constants.siUICY, 30)

    layout.AddSpacer(10, 10)

    layout.AddRow()
    layout.AddSpacer(10, 10)
    item = layout.AddStaticText('http://www.ethivierge.com')
    layout.EndRow()

    # Config Tab
    layout.AddTab('Config')

    configParams = {}
    for eachParam in prop.Parameters:
        if eachParam.Name.startswith('config') and eachParam.Name != 'configTokens':
            configParams[eachParam.Name] = eachParam

    layout.AddGroup()

    layout.AddRow()
    layout.AddSpacer(10, 10)
    item = layout.AddItem(prop.Parameters('configTokens').Name, 'Tokens')
    item.SetAttribute(constants.siUILabelMinPixels, 75)
    item.SetAttribute(constants.siUILabelPercentage, 15)
    layout.AddSpacer(10, 10)
    layout.EndRow()

    for eachParam in sorted(configParams):
        layout.AddRow()
        layout.AddSpacer(10, 10)
        item = layout.AddItem(configParams[eachParam].Name, configParams[eachParam].Name[6:])
        item.SetAttribute(constants.siUILabelMinPixels, 75)
        item.SetAttribute(constants.siUILabelPercentage, 15)
        layout.AddSpacer(10, 10)
        layout.EndRow()

    layout.EndGroup()

    layout.AddSpacer(10,5)

    item = layout.AddButton('SaveConfig', 'Save')
    item.SetAttribute(constants.siUICX, 280)
    item.SetAttribute(constants.siUICY, 30)

    layout.AddSpacer(10,10)

    layout.AddRow()
    layout.AddSpacer(10,10)
    item = layout.AddStaticText('http://www.ethivierge.com')
    layout.EndRow()

    PPG.Refresh()

def etRenamer_DefineLayout( in_ctxt ):
    return True


def etRenamer_OnInit():
    buildNamingUI()

    return


def etRenamer_Function_OnChanged():
    buildNamingUI()

    return


def etRenamer_configTokens_OnChanged():
    prop = PPG.Inspected(0)
    param = prop.Parameters('configTokens')

    p = re.compile('[^\w\[\]]')
    param.Value = p.sub('', param.Value).lower()


def etRenamer_Rename_OnClicked():

    try:
        si.BeginUndo('ET_Rename')

        initNames = [x.Name for x in si.Selection]
        prop = PPG.Inspected(0)

        renameFunction = prop.Parameters('Function').Value
        component = prop.Parameters('Component').Value
        prefix = prop.Parameters('Prefix').Value
        newName = prop.Parameters('NewName').Value
        suffix = prop.Parameters('Suffix').Value
        padding = prop.Parameters('Padding').Value
        side = prop.Parameters('Side').Value
        objectType = prop.Parameters('objectType').Value
        searchFor = prop.Parameters('SearchFor').Value
        replaceWith = prop.Parameters('ReplaceWith').Value
        strip = prop.Parameters('Strip').Value
        stripFromBegin = prop.Parameters('StripFromBegin').Value
        token = prop.Parameters('configTokens').Value

        renamer = etRenamer.etRenamer()
        renamer.initNames = initNames
        renamer.component = component
        renamer.prefix = prefix
        renamer.newName = newName
        renamer.suffix = suffix

        if padding == 0:
            renamer.padding = len(str(len(renamer.initNames)))
        elif padding == 5:
            renamer.padding = 0
        else:
            renamer.padding = padding

        renamer.side = side
        renamer.objectType = objectType
        renamer.searchFor = searchFor
        renamer.replaceWith = replaceWith
        renamer.strip = strip
        renamer.strToken = token

        if renameFunction == 'Rename':
            renamer.rename()

            for i, eachObj in enumerate(sel):
                eachObj.Name = renamer.outNames[i]

        elif renameFunction == 'PrefixSuffix':
            renamer.prefixSuffix()

            for i, eachObj in enumerate(sel):
                eachObj.Name = renamer.outNames[i]

        elif renameFunction == 'SearchAndReplace':
            renamer.searchAndReplace()

            for i, eachObj in enumerate(sel):
                eachObj.Name = renamer.outNames[i]

        elif renameFunction == 'Strip':
            if stripFromBegin:
                renamer.stripFromBegin = True

            renamer.stripName()

            for i, eachObj in enumerate(sel):
                eachObj.Name = renamer.outNames[i]

        return

    except Exception, e:
        Naming_Exception(e, sys.exc_info()[2])
        return False

    finally:
        si.EndUndo()


def etRenamer_SaveConfig_OnClicked():

    prop = PPG.Inspected(0)

    renameFunction = prop.Parameters('Function').Value

    renamer = etRenamer.etRenamer()
    renamer.getNameTemplate()
    nameTemplate = renamer.nameTemplate

    for eachItem in nameTemplate:
        nameTemplate[eachItem] = prop.Parameters('config' + eachItem).Value

    renamer.writeNameTemplate()

    return


def etRenamer_OnClosed():
    scnRoot = si.ActiveProject3.ActiveScene.Root

    try:
        si.DeleteObj(scnRoot.Properties('etRenamer'))
    except:
        log('Could not delete etRenamer property.', 8)


# ================
# Utils
# ================
def popWindow(prop, moveX, moveY, resizeX, resizeY):

    renamerView = si.Desktop.ActiveLayout.CreateView('Property Panel', prop.Name)
    renamerView.BeginEdit()
    renamerView.Move(moveX, moveY)
    renamerView.Resize(resizeX, resizeY)
    renamerView.SetAttributeValue('targetcontent', prop.FullName)
    renamerView.EndEdit()

    return True