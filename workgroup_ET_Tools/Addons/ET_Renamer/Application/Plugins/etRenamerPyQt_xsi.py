"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# etRenamerPyQt Softimage Plugin

from win32com.client import constants
from win32com.client import Dispatch

import os
import sys
import traceback

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

DEBUG = 0


def XSILoadPlugin( in_reg ):
    in_reg.Author = 'Eric Thivierge'
    in_reg.Name = 'etRenamerPyQt'
    in_reg.Major = 3
    in_reg.Minor = 2

    try:
        from PyQt4 import QtCore, QtGui
        import sip
    except ImportError:
        log('PyQt not found, etRenamerPyQt will not be loaded.', 4)
        return False

    if xsi.Plugins('QtSoftimage'):
        in_reg.RegisterCommand('etRenamerPyQt', 'etRenamerPyQt')
        in_reg.RegisterMenu(constants.siMenuMCPEditID, 'etRenamerPyQtMenu', False, True)
    #RegistrationInsertionPoint - do not remove this line

    return True


# ======
# Menus
# ======
def etRenamerPyQtMenu_Init(in_ctxt):
    menu = in_ctxt.Source
    menu.AddCommandItem("etRenamerPyQt", "etRenamerPyQt")

    return True


# =========
# Commands
# =========
def etRenamerPyQt_Init( in_ctxt ):
    cmd = in_ctxt.Source
    cmd.Description = "etRenamer PyQt Version"
    cmd.Tooltip = "etRenamer PyQt Version"
    cmd.SetFlag(constants.siCannotBeUsedInBatch, True)
    cmd.ReturnValue = True

    return True


def etRenamerPyQt_Execute():
    from etRenamerUtils import etRenamerPyQt
    from PyQt4 import QtCore, QtGui
    import sip

    if not xsi.Commands('getQtSoftimageAnchor'):
        log('PyQtForSoftimage not found. Unable to load etRenamerPyQt', 4)
        return False

    sianchor = xsi.getQtSoftimageAnchor()
    sianchor = sip.wrapinstance(long(sianchor), QtGui.QWidget)

    etRenamerUI = etRenamerPyQt.etRenamerPyQtXSI(sianchor)
    etRenamerUI.show()

    return True