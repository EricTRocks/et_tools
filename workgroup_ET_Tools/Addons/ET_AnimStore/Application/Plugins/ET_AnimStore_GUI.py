"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# ET_AnimStore_GUI Plugin

from win32com.client import constants as c
from win32com.client import Dispatch as d
import sqlite3 as sq
import os,shutil

xsi = d("XSI.Application").Application
log = xsi.LogMessage
oActiveProject = xsi.ActiveProject2
collSel = xsi.Selection

versionMajor = 2
versionMinor = 3


DEBUG = 0

def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric Thivierge"
    in_reg.Name = "ET_AnimStore_GUI"
    in_reg.Major = versionMajor
    in_reg.Minor = versionMinor

    in_reg.RegisterProperty("ET_AnimStore_GUI")
    in_reg.RegisterProperty("ET_AnimStore_PoseList")
    in_reg.RegisterCommand("ET_AnimStore_CreateGUI","ET_AnimStore_CreateGUI")
    in_reg.RegisterMenu(c.siMenuTbAnimateActionsStoreID,"ET_AnimStore",True,True)
    #RegistrationInsertionPoint - do not remove this line

    return True


# ======
# Menus
# ======
def ET_AnimStore_Init(in_ctxt):
    oMenu = in_ctxt.Source
    oMenu.AddCommandItem("Create DB","ET_AnimStore_CreateDB")
    oMenu.AddCommandItem("Create GUI","ET_AnimStore_CreateGUI")
    return True


# ======================
# ET_AnimStore_CreateGUI
# ======================
def ET_AnimStore_CreateGUI_Init(in_ctxt):
    oCmd = in_ctxt.Source
    oCmd.ScriptingName = "ET_AnimStore_CreateGUI"
    oCmd.ReturnValue = True
    return True


def ET_AnimStore_CreateGUI_Execute():
    import AnimStoreUtils as asu
    
    oScnRoot = xsi.ActiveProject2.ActiveScene.Root

    oETAnimStoreModel = xsi.Dictionary.GetObject("ET_AnimStore_Model",False)
    if oETAnimStoreModel == None:
        oScnRoot = oActiveProject.ActiveScene.Root
        oETAnimStoreModel = oScnRoot.AddModel(None,"ET_AnimStore_Model")
        oETAnimStoreModel.Properties("Visibility").Parameters("viewvis").Value = 0
    
    oET_AnimStoreGUI = oETAnimStoreModel.Properties("ET_AnimStore_GUI")
    if oET_AnimStoreGUI:
        asu.popWindow(oET_AnimStoreGUI, 200, 200, 450, 255,"ET AnimStore v2")
    else:
        oAnimStoreProp = oETAnimStoreModel.AddProperty("ET_AnimStore_GUI")
        asu.popWindow(oAnimStoreProp, 200, 200, 450, 255,"ET AnimStore v2")
        
    return True


# ================
# ET_AnimStore_GUI
# ================
def ET_AnimStore_GUI_Define( in_ctxt ): 
    import AnimStoreUtils as asu
    oCustomProperty = in_ctxt.Source
    strAnimStoreDB = asu.getConfigValue()

    oCustomProperty.AddParameter3("AnimStoreDB",c.siString,strAnimStoreDB,None,None,False,0)
    oCustomProperty.AddParameter3("ProjectID",c.siString,"None",None,None,False,0)
    oCustomProperty.AddParameter3("ModelGroup",c.siString,"None",None,None,False,0)
    oCustomProperty.AddParameter3("ModelTarget",c.siString,"",None,None,False,1)
    oCustomProperty.AddParameter3("ActionType",c.siString,"pose",None,None,False,0)
    oCustomProperty.AddParameter3("ActionSet",c.siString,"None",None,None,False,0)
    oCustomProperty.AddParameter3("GUIInit",c.siInt4,0)
    oCustomProperty.AddParameter3("Logo",c.siString,"",None,None,False,0)
    return True


def ET_BuildAnimStoreGUI():
    import AnimStoreUtils as asu
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    strModelGroup = PPG.ModelGroup.Value
    strActionType = PPG.ActionType.Value
    strActionSet = PPG.ActionSet.Value  
    
    lDBProjects = asu.getProjects(strAnimStoreDB)
    lDBGroups = asu.getGroups(strAnimStoreDB,strProjectID)
    lDBActionSets = asu.getSets(strAnimStoreDB,strActionType,strModelGroup)
    lDBActionTypes = ["Pose","pose","Animation","animation"]
    
    oProp = PPG.Inspected(0)
    
    oLayout = PPG.PPGLayout
    oLayout.Clear() 
    
    if lDBProjects and lDBProjects[0] != "None" and strProjectID != None:
        oLayout.AddTab("Anim Store")
        oLayout.AddGroup("Filters")

        oLayout.AddSpacer(10, 10)
        
        oLayout.AddRow()
        oLayout.AddSpacer(10, 10)

        oItem = oLayout.AddEnumControl("ModelGroup", lDBGroups, "Model Group", c.siControlCombo)
        oItem.SetAttribute("LabelPercentage", 50)
        oItem.SetAttribute("LabelMinPixels", 100)
        oLayout.AddSpacer(10, 10)
        oButton = oLayout.AddButton("NewModelGroup", "New")
        oButton.SetAttribute("CX", 50)
        oLayout.AddSpacer(10, 10)
        if lDBGroups[0] != "None":
            oButton = oLayout.AddButton("DeleteModelGroup", "Delete")
            oButton.SetAttribute("CX", 50)
            oLayout.AddSpacer(10, 10)
        oLayout.EndRow()
        
        oLayout.AddSpacer(10, 10)
        
        
        if strModelGroup != 0 and strModelGroup != "None" and strModelGroup != "0" and int(strModelGroup) in lDBGroups:
        
            oLayout.AddRow()
            oLayout.AddSpacer(10, 10)
            oItem = oLayout.AddEnumControl("ActionType", lDBActionTypes, "Action Type", c.siControlCombo)
            oItem.SetAttribute("LabelPercentage", 50)
            oItem.SetAttribute("LabelMinPixels", 100)
            oLayout.AddSpacer(10, 10)
            oLayout.EndRow()
        
            oLayout.AddSpacer(10, 10)
        
            oLayout.AddRow()
            oLayout.AddSpacer(10, 10)
        
            if lDBActionSets and lDBActionSets[0] != "None":
                oItem = oLayout.AddEnumControl("ActionSet", lDBActionSets, "Action Set", c.siControlCombo)
                oItem.SetAttribute("LabelPercentage", 50)
                oItem.SetAttribute("LabelMinPixels", 100)
                oLayout.AddSpacer(10, 10)
            else:
                oLayout.AddStaticText("Action Set", 100, 20)
            
            oButton = oLayout.AddButton("NewSet", "New")
            oButton.SetAttribute("CX", 50)
            oLayout.AddSpacer(10, 10)
                
            if lDBActionSets and lDBActionSets[0] != "None":
                oButton = oLayout.AddButton("DeleteSet", "Delete")
                oButton.SetAttribute("CX", 50)
                oLayout.AddSpacer(10, 10)
            oLayout.EndRow()
        
            oLayout.AddSpacer(10, 10)

            oLayout.EndGroup()

            if lDBActionSets and lDBActionSets[0] != "None":
                oLayout.AddRow()
                oButton = oLayout.AddButton("Inspect", "Inspect")
                oButton.SetAttribute("CX", 212)
                oButton.SetAttribute("CY", 30)
                oLayout.AddSpacer(6, 10)
                oButton = oLayout.AddButton("NewAction", "New Action")
                oButton.SetAttribute("CX", 212)
                oButton.SetAttribute("CY", 30)
                oLayout.EndRow()
            
            oLayout.AddSpacer(10, 5)
            
            oLayout.AddRow()
            oButton = oLayout.AddButton("Refresh","Refresh")
            oButton.SetAttribute("CX", 430)
            oButton.SetAttribute("CY", 30)
            oLayout.EndRow()
                        
        else:
            oLayout.EndGroup()
        
    oLayout.AddTab("Config")

    oLayout.AddSpacer(10,10)
    
    oLayout.AddGroup("Database Settings")
    
    oLayout.AddSpacer(10,10)
    
    oLayout.AddRow()
    oLayout.AddSpacer(10,10)
    oItem = oLayout.AddItem("AnimStoreDB","AnimStore DB",c.siControlFilePath)
    oItem.SetAttribute("InitialDir",oActiveProject.Path)
    oItem.SetAttribute(c.siUIOpenFile,True)
    oItem.SetAttribute(c.siUIFileMustExist,True)
    oItem.SetAttribute(c.siUIFileFilter,"AnimStore Files (*.sqlite)|*.sqlite")
    oItem.SetAttribute("LabelPercentage",50)
    oItem.SetAttribute("LabelMinPixels",100)
    oLayout.AddSpacer(10,10)
    oButton = oLayout.AddButton("ReReadProject","Re-Read")
    oButton.SetAttribute("CX",50)
    
    oLayout.AddSpacer(10,10)
    
    oLayout.EndRow()
    
    oLayout.AddSpacer(10,10)
    
    if lDBProjects and lDBProjects[0] != "None":
        oLayout.AddRow()
        oLayout.AddSpacer(10,10)
        oItem = oLayout.AddEnumControl("ProjectID",lDBProjects,"Project",c.siControlCombo)
        oLayout.AddSpacer(10,10)
        oLayout.EndRow()

    oLayout.AddSpacer(10,10)
    oLayout.EndGroup()
    
    if strAnimStoreDB != "":
        oLayout.AddGroup("Tools")
        oLayout.AddSpacer(10,10)

    
        oLayout.AddRow()
        oLayout.AddSpacer(10,10)
        oButton = oLayout.AddButton("NewProject","New Project")
        oButton.SetAttribute("CX",126)
        oButton.SetAttribute("CY",30)

        oLayout.AddSpacer(10,10)
        oButton = oLayout.AddButton("RenameProject","Rename Project")
        oButton.SetAttribute("CX",126)
        oButton.SetAttribute("CY",30)

        oLayout.AddSpacer(10,10)
        oButton = oLayout.AddButton("DeleteProject","Delete Project")
        oButton.SetAttribute("CX",126)
        oButton.SetAttribute("CY",30)
        oLayout.AddSpacer(10,10)
        oLayout.EndRow()
        
        oLayout.AddSpacer(10,10)
        oLayout.EndGroup()

    oLayout.AddTab("Info")
    oLayout.AddSpacer(10,10)
    
    oLayout.AddGroup("Addon Info")
    
    oLayout.AddSpacer(10,10)

    oPlugin = xsi.Plugins("ET_AnimStore_GUI")
    strPluginPath = oPlugin.OriginPath
    
    strLogoPath = XSIUtils.BuildPath(strPluginPath, "..", "..", "Data", "Images", "ET_AnimStoreLogo.bmp")
    
    oLayout.AddRow()
    oLayout.AddGroup("",0,30)
    oLogoImage = oLayout.AddItem("Logo","",c.siControlBitmap)
    oLogoImage.SetAttribute("path", strLogoPath)
    oLogoImage.SetAttribute("NoLabel",1)
    oLogoImage.SetAttribute("CX",165)
    oLogoImage.SetAttribute("CY",30)
    oLogoImage.SetAttribute("WidthPercentage",50)
    oLayout.EndGroup()
    oLayout.AddGroup("",False)
    oLayout.EndGroup()
    oLayout.EndRow()
    
    oLayout.AddRow()
    oLayout.AddSpacer(10,5)
    oThanksInfo="""http://www.ethivierge.com | ethivierge@gmail.com

Thanks To:
SpeakeasyFX
Alan Fregtman (www.darkvertex.com)
Janimation (www.janimation.com)
Stephen Blair (xsisupport.wordpress.com)
    """

    oLayout.AddStaticText(oThanksInfo,400,120)

    oLayout.EndRow()
    oLayout.EndGroup()

def ET_AnimStore_GUI_DefineLayout( in_ctxt ):
    return True


def ET_AnimStore_GUI_OnInit():
    refreshPPG()
    return


def ET_AnimStore_GUI_OnClosed():
    return


# === OnChanged === #
def ET_AnimStore_GUI_AnimStoreDB_OnChanged():
    import AnimStoreUtils as asu
    
    if PPG.AnimStoreDB.Value != "":
        asu.updateConfig(str(PPG.AnimStoreDB.Value))
    refreshPPG()
    return


def ET_AnimStore_GUI_ProjectID_OnChanged():
    refreshPPG()
    return


def ET_AnimStore_GUI_ModelGroup_OnChanged():
    refreshPPG()
    return

def ET_AnimStore_GUI_ActionType_OnChanged():
    refreshPPG()
    return
    
def ET_AnimStore_GUI_ActionSet_OnChanged():
    refreshPPG()
    return


# === OnClicked === #
def ET_AnimStore_GUI_Refresh_OnClicked():
    refreshPPG()
    return


def ET_AnimStore_GUI_ReReadProject_OnClicked():
    import AnimStoreUtils as asu
    
    asu.updateConfig(PPG.AnimStoreDB.Value)
    refreshPPG()
    return

def ET_AnimStore_GUI_NewProject_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    
    oProp = XSIFactory.CreateObject("CustomProperty")
    oProp.Name = "New Project"
    oProp.AddParameter3("ProjectName", c.siString, "Project Name")
    oLayout = oProp.PPGLayout
    oProjectName = oLayout.AddItem("ProjectName", "Project Name", "String")
    
    oModal = xsi.InspectObj(oProp, None, "New Project", 4, False)
    
    if oModal == False: # Check to see if the user cancels the Modal PPG
        strProjectName = asu.cleanStr(oProp.ProjectName.Value).upper()
        
        lCheckProjectName = asu.checkStringInput(strProjectName, "New Project Name")
        if not lCheckProjectName:
            return False
                
        try:
            conn = sq.connect(PPG.AnimStoreDB.Value)
            cursor = conn.cursor()
            data = cursor.execute("""INSERT INTO projects (project_name) VALUES ('""" + strProjectName + """')""")
            conn.commit()
            conn.close()
            
            log("Inserted '" + strProjectName + "' into the 'Projects' table.", 8)

            strCleanPath = strAnimStoreDB.replace("AnimStore.sqlite", "")
            log(strCleanPath)
            strNewProjectPath = XSIUtils.BuildPath(strCleanPath,"Projects", strProjectName)
            log(strNewProjectPath)
            
            try:
                os.makedirs(strNewProjectPath)
            except:
                conn = sq.connect(PPG.AnimStoreDB.Value)
                cursor = conn.cursor()
                data = cursor.execute("""DELETE FROM projects WHERE project_name='""" + strProjectName +"""'""")
                conn.commit()
                conn.close()
                log("Deleted '" + strProjectName + "' from 'Projects' table.", 8)
        
        except sq.Error, e:
            if str(e.args[0]) == "column project_name is not unique":
                log("Project '" + strProjectName + "' already exists in the database.", 2)
            else:
                log("Error ocurred: " + str(e.args[0]), 2)
            
        finally:
            refreshPPG()

    else:
        log("User Cancelled", 4)

    return


def ET_AnimStore_GUI_RenameProject_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    
    if not strProjectID:
        log("Invalid project selected.", 4)
        return False

    oProp = XSIFactory.CreateObject("CustomProperty")
    oProp.Name = "New Name"
    oProp.AddParameter3("NewName", c.siString, "New Name")
    oLayout = oProp.PPGLayout
    oNewName = oLayout.AddItem("NewName", "New Name", "String")
    
    oModal = xsi.InspectObj(oProp, None, "New Name", 4, False)
    
    if oModal == False: # Check to see if the user cancels the Modal PPG
        strNewName = asu.cleanStr(oProp.NewName.Value).upper()
        dActionInfo = asu.getActionInfo(strProjectID, None, None, strAnimStoreDB)
        
        lCheckNewName = asu.checkStringInput(strNewName, "New Project Name")
        if not lCheckNewName:
            return False
        
        try:
            conn = sq.connect(PPG.AnimStoreDB.Value)
            cursor = conn.cursor()
                
            data = cursor.execute("""UPDATE projects SET project_name='""" + strNewName + """' WHERE rowid='""" + strProjectID + """'""")
            conn.commit()
            conn.close()
            
            strAnimStoreDBPath = strAnimStoreDB.replace("AnimStore.sqlite", "")
            
            os.rename(XSIUtils.BuildPath(strAnimStoreDBPath, "Projects", dActionInfo["Project"]), XSIUtils.BuildPath(strAnimStoreDBPath, "Projects", strNewName))
            
            log("Updated project '" + dActionInfo["Project"] + "' to " + strNewName + ".", 8)
        
        except sq.Error, e:
            log("Error ocurred: " + str(e.args[0]), 2)
            
        finally:
            refreshPPG()

    else:
        log("User Cancelled", 4)

    return


def ET_AnimStore_GUI_DeleteProject_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    
    if not strProjectID:
        log("Invalid project selected.", 4)
        return False

    msgBox = XSIUIToolkit.MsgBox("Are you sure?", c.siMsgYesNo, "ET_AnimStore Delete Project")
    if msgBox == 6: # Check to see if the user cancels the Modal PPG
        dActionInfo = asu.getActionInfo(strProjectID, None, None, strAnimStoreDB)
    
        try:
            conn = sq.connect(PPG.AnimStoreDB.Value)
            cursor = conn.cursor()
            
            data = cursor.execute("""DELETE FROM projects WHERE project_name='""" + dActionInfo["Project"] + """'""")
            conn.commit()
            conn.close()
            
            log("Deleted project: '" + dActionInfo["Project"] + "' from 'Projects' table.", 8)
            
            try:
                strCleanPath = strAnimStoreDB.replace("AnimStore.sqlite", "")
                strProjectPath = XSIUtils.BuildPath(strCleanPath, "Projects", dActionInfo["Project"])
                shutil.rmtree(strProjectPath)
            except:
                log("Could not delete '" + strProjectPath + "'")

        except sq.Error, e:
            log("Error ocurred: " + str(e.args[0]), 2)
            
        finally:
            PPG.ProjectID.Value = ""
            refreshPPG()

    else:
        log("User Cancelled", 4)

    return  


def ET_AnimStore_GUI_NewModelGroup_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    strGroupID = PPG.ModelGroup.Value
    strSetID = PPG.ActionSet.Value

    oProp = XSIFactory.CreateObject("CustomProperty")
    oProp.Name = "New Group"
    oProp.AddParameter3("GroupName", c.siString, "Group Name")
    oLayout = oProp.PPGLayout
    oGroupName = oLayout.AddItem("GroupName", "Group Name", "String")
    
    oModal = xsi.InspectObj(oProp, None, "New Group", 4, False)
    
    if oModal == False: # Check to see if the user cancels the Modal PPG
        strGroupName = asu.cleanStr(oProp.GroupName.Value).lower()
        dActionInfo = asu.getActionInfo(strProjectID, None, None, strAnimStoreDB)
        
        lCheckGroupName = asu.checkStringInput(strGroupName, "New Group Name")
        if not lCheckGroupName:
            return False
        
        try:
            conn = sq.connect(strAnimStoreDB)
            cursor = conn.cursor()
            
            dataProject = cursor.execute("""SELECT rowid FROM projects WHERE rowid='""" + strProjectID + """' LIMIT 1""")
            intProjectID = [x[0] for x in dataProject][0]
            
            data = cursor.execute("""INSERT INTO groups (group_name,project_id) VALUES ('""" + strGroupName +"""', """ + str(intProjectID) + """)""")
            conn.commit()
            conn.close()
            
            log("Inserted '" + strGroupName + "' into the 'Groups' table.", 8)
            
            strCleanPath = strAnimStoreDB.replace("AnimStore.sqlite", "")
            strNewGroupPath = XSIUtils.BuildPath(strCleanPath, "Projects", dActionInfo["Project"], strGroupName)
            
            try:
                os.makedirs(strNewGroupPath)
                if DEBUG:
                    log(strNewGroupPath)
            except:
                conn = sq.connect(PPG.AnimStoreDB.Value)
                cursor = conn.cursor()
                data = cursor.execute("""DELETE FROM groups WHERE group_name='""" + strGroupName +"""'""")
                conn.commit()
                conn.close()
                log("Deleted '" + strGroupName + "' from 'Groups' table.", 8)
        
        except sq.Error, e:
                log("Error ocurred: " + str(e.args[0]), 2)
        finally:
            refreshPPG()

    else:
        log("User Cancelled", 2)

    return
    
    
def ET_AnimStore_GUI_DeleteModelGroup_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    strGroupID = PPG.ModelGroup.Value
    
    dActionInfo = asu.getActionInfo(strProjectID, strGroupID, None, strAnimStoreDB)
    
    msgBox = XSIUIToolkit.MsgBox("Are you sure you want to delete the '" + dActionInfo["Group"] + "' group?", c.siMsgYesNo, "Are you sure?")
    if msgBox == 6: # Check to see if the user cancels the Modal PPG
        dActionInfo = asu.getActionInfo(strProjectID, strGroupID, None, strAnimStoreDB)
        
        try:
            conn = sq.connect(PPG.AnimStoreDB.Value)
            cursor = conn.cursor()
                        
            data = cursor.execute("""DELETE FROM groups WHERE rowid='""" + strGroupID +"""' AND project_id = """ + strProjectID + """""")
            conn.commit()
            conn.close()
            
            log("Deleted '" + dActionInfo["Group"] + "' from the 'Groups' table.", 8)
            
            try:
                strCleanPath = strAnimStoreDB.replace("AnimStore.sqlite", "")
                strGroupPath = XSIUtils.BuildPath(strCleanPath,"Projects", dActionInfo["Project"], dActionInfo["Group"])
                shutil.rmtree(strGroupPath)
            except:
                log("Could not delete '" + strGroupPath + "'")
            
        except sq.Error, e:
            log("Error ocurred: " + str(e.args[0]), 2)
        finally:
            refreshPPG()
    else:
        log("User Cancelled!", 2)
        
    return


def ET_AnimStore_GUI_NewSet_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    strModelGroup = PPG.ModelGroup.Value
    strActionType = PPG.ActionType.Value
    
    oProp = XSIFactory.CreateObject("CustomProperty")
    oProp.Name = "New Set"
    oProp.AddParameter3("NewSetName", c.siString, "Set Name")
    oLayout = oProp.PPGLayout
    oNewSetName = oLayout.AddItem("NewSetName", "Set Name", "String")
    
    oModal = xsi.InspectObj(oProp, None, "New Set", 4, False)
    
    if oModal == False: # Check to see if the user cancels the Modal PPG
        strSetName = asu.cleanStr(oProp.NewSetName.Value).lower()
        
        lCheckSetName = asu.checkStringInput(strSetName, "New Set Name")
        if not lCheckSetName:
            return False
        
        try:
            try:
                dActionInfo = asu.getActionInfo(strProjectID, strModelGroup, None, strAnimStoreDB)
                
                conn = sq.connect(PPG.AnimStoreDB.Value)
                cursor = conn.cursor()
                checkdata = cursor.execute("""SELECT set_name FROM sets WHERE set_name='""" + strSetName + """' AND action_type='""" + strActionType + """' AND group_id=""" + strModelGroup + """""")
                                
                lRows = [x for x in checkdata]
                if len(lRows):
                    log(strSetName + " is already in the database.", 2)
                    conn.close()
                    return
                
                data = cursor.execute("""INSERT INTO sets (set_name,group_id,action_type) VALUES ('""" + strSetName + """', '""" + strModelGroup + """', '""" + strActionType + """')""")
                
                conn.commit()
                conn.close()
                
                log("Inserted '" + strSetName + "' into the 'Sets' table.", 8)
                
                strCleanPath = strAnimStoreDB.replace("AnimStore.sqlite","")
                strNewSetPath = XSIUtils.BuildPath(strCleanPath, "Projects", dActionInfo["Project"], dActionInfo["Group"], strActionType, strSetName)
                os.makedirs(strNewSetPath)              
                
            except Exception, strerror:
                log(strerror, 2)                
        
        except sq.Error, e:
            if str(e.args[0]) == "column set_name is not unique":
                log("Project '" + strProjectID + "' already exists in the database.", 2)
            else:
                log("Error ocurred: " + str(e.args[0]), 2)
        finally:
            refreshPPG()

    else:
        log("User Cancelled", 2)

    return


def ET_AnimStore_GUI_DeleteSet_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    strGroupID = PPG.ModelGroup.Value
    strActionType = PPG.ActionType.Value
    strSetID = PPG.ActionSet.Value
    
    dActionInfo = asu.getActionInfo(strProjectID,strGroupID,strSetID,strAnimStoreDB)
        
    msgBox = XSIUIToolkit.MsgBox("Are you sure you want to delete the '" + dActionInfo["Set"] + "' set?", c.siMsgYesNo, "Are you sure?")
    if msgBox == 6: # Check to see if the user cancels the Modal PPG
        
        try:
            strCleanPath = strAnimStoreDB.replace("AnimStore.sqlite","")
            strSetPath = XSIUtils.BuildPath(strCleanPath, "Projects", dActionInfo["Project"], dActionInfo["Group"], strActionType, dActionInfo["Set"])
                
            conn = sq.connect(PPG.AnimStoreDB.Value)
            cursor = conn.cursor()
            
            data = cursor.execute("""DELETE FROM sets WHERE rowid='""" + strSetID +"""' AND group_id = """ + strGroupID + """""")
            conn.commit()
            conn.close()
            
            log("Deleted '" + dActionInfo["Set"] + "' from the 'Sets' table.",8)
            
            try:
                shutil.rmtree(strSetPath)
            except:
                log("Could not delete '" + strSetPath + "'")
        
        except sq.Error, e:
            log("Error ocurred: " + str(e.args[0]), 2)
        finally:
            refreshPPG()
    else:
        log("User Cancelled!", 2)

    return


def ET_AnimStore_GUI_Inspect_OnClicked():
    import AnimStoreUtils as asu
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    strGroupID = PPG.ModelGroup.Value
    strActionType = PPG.ActionType.Value
    strSetID = PPG.ActionSet.Value
    
    oETAnimStoreModel = PPG.Inspected(0).Model
    oProp = oETAnimStoreModel.AddProperty("ET_AnimStore_PoseList")
    
    oProp.Parameters("strAnimStoreDB").Value = strAnimStoreDB
    oProp.Parameters("strProjectID").Value = strProjectID
    oProp.Parameters("strGroupID").Value = strGroupID
    oProp.Parameters("strActionType").Value = strActionType
    oProp.Parameters("strSetID").Value = strSetID
    
    asu.popWindow(oProp, 115, 68, 420, 550, "ET AnimStore PoseList")
    
    return
    
    
def ET_AnimStore_GUI_NewAction_OnClicked( ):

    if not collSel.Count:
        log("Nothing selected", 2)
        return

    # Get plug-in path
    oPlugin = xsi.Plugins("ET_AnimStore_GUI")
    strPluginPath = oPlugin.OriginPath
    
    strAnimStoreDB = PPG.AnimStoreDB.Value
    strProjectID = PPG.ProjectID.Value
    strGroupID = PPG.ModelGroup.Value
    strActionType = PPG.ActionType.Value
    strSetID = PPG.ActionSet.Value
    
    conn = sq.connect(PPG.AnimStoreDB.Value)
    cursor = conn.cursor()
    
    data = cursor.execute("""SELECT set_name FROM sets WHERE group_id='""" + strGroupID + """' AND rowid='""" + strSetID + """'""")
    lData = [x for x in data]
    
    conn.commit()
    conn.close()
    
    if not len(lData):
        log("Invalid set selection. Pease verify your settings on the ET_AnimStore_GUI", 2)
        return False
    
    oETAnimStoreModel = PPG.Inspected(0).Model
    
    oExportActionProp = xsi.Dictionary.GetObject("ET_AnimStore_GUI.ET_AnimStore_ExportAction", False)
    if oExportActionProp == None:
        oExportActionProp = oETAnimStoreModel.AddProperty("ET_AnimStore_ExportAction")
    
    preSel = d("XSI.Collection")
    preSel.SetAsText(collSel.GetAsText())
    
    oETPoseCam = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_PoseCam", False)
    if oETPoseCam == None:
        oETPoseCamModel = xsi.ImportModel(XSIUtils.BuildPath(strPluginPath, "..", "..", "Data", "ET_PoseCam_Model.emdl"))
        lETPoseCamItems = oETPoseCamModel(1).FindChildren("", "", "", False)
        
        for eachItem in lETPoseCamItems:
            oETAnimStoreModel.AddChild(eachItem)
            
        xsi.DeleteObj(oETPoseCamModel(1))
        collSel.SetAsText(preSel.GetAsText())
    
    strViewPath = XSIUtils.BuildPath(strPluginPath, "..", "Views", "ET_AnimStore_PoseCam.xsivw")
    oETAnimStoreModel = Application.Dictionary.GetObject("ET_AnimStore_Model")
    oCam = oETAnimStoreModel.FindChild("",c.siCameraPrimType)
    
    camView = xsi.Desktop.ActiveLayout.CreateViewFromDefinitionFile(strViewPath, "Pose Cam")
    
    for eachView in camView.Views:
        if eachView.Name == "object_view":
            oObjView = eachView 
    
    oObjView.SetAttributeValue("camera",oCam.FullName)
    oObjView.SetAttributeValue("displaymode","shaded")
    oObjView.SetAttributeValue("targetcontent",collSel(0).Model.FullName + ".*")
    oObjView.SetAttributeValue("lockstatus","true")
    
    camView.BeginEdit()
    camView.Move(120,80)
    camView.Resize(380,640)
    camView.EndEdit()
    
    return


#== On Tab Events ==#
def ET_AnimStore_GUI_Config_OnTab():
    PPG.GUIInit = 1

def ET_AnimStore_GUI_AnimStore_OnTab():
    #log(PPG.GUIInit.Value)
    if PPG.GUIInit.Value == 0:
        PPG.CurrentTab = "Config"
    return
    

# ===============================
# ET_AnimStore_PoseList Property
# ===============================
def ET_AnimStore_PoseList_Define( in_ctxt ):
    oProp = in_ctxt.Source

    oProp.AddParameter2("strAnimStoreDB", c.siString, "", "", "", "", "", c.siClassifUnknown, c.siPersistable + c.siNotInspectable)
    oProp.AddParameter2("strProjectID", c.siString, "", "", "", "", "", c.siClassifUnknown, c.siPersistable + c.siNotInspectable)
    oProp.AddParameter2("strGroupID", c.siString, "", "", "", "", "", c.siClassifUnknown, c.siPersistable + c.siNotInspectable)
    oProp.AddParameter2("strActionType", c.siString, "", "", "", "", "", c.siClassifUnknown, c.siPersistable + c.siNotInspectable)
    oProp.AddParameter2("strSetID", c.siString, "", "", "", "", "", c.siClassifUnknown, c.siPersistable + c.siNotInspectable)
    oProp.AddParameter2("Spacer",c.siString)

    return True

    
def ET_AnimStore_PoseList_DefineLayout( in_ctxt ):
    return
    

def makePoseListLayout():
    import AnimStoreUtils as asu
    
    oProp = PPG.Inspected(0)
    oModel = oProp.Model
    oGUIProp = oModel.Properties("ET_AnimStore_GUI")
    
    strAnimStoreDB = oGUIProp.Parameters("AnimStoreDB").Value
    strProjectID = oGUIProp.Parameters("ProjectID").Value
    strGroupID = oGUIProp.Parameters("ModelGroup").Value
    strActionType = oGUIProp.Parameters("ActionType").Value
    strSetID = oGUIProp.Parameters("ActionSet").Value
    oSpacer = oProp.Parameters("Spacer").Value
        
    # Clear old params
    # =================
    collParams = oProp.Parameters
    
    for eachParam in collParams:
        strParamName = eachParam.Name
        strNameSplit = strParamName.split("_", 1)
        
        if len(strNameSplit) > 1 and strNameSplit[1] == "Syn":
            oProp.RemoveParameter(eachParam)
    
        if strParamName[:6] == "Spacer":
            oProp.RemoveParameter(eachParam)
    
    # Add New Params
    # ===============
    oProp.AddParameter3("Spacer", c.siString)
    
    if DEBUG:
        from time import clock
        start_time = clock()
    
    dFoundPoses = asu.getPoseList(strProjectID, strGroupID, strActionType, strSetID, strAnimStoreDB)
    if dFoundPoses:
        for eachPose in sorted(dFoundPoses.keys()):
            myPath = XSIUtils.BuildPath(strAnimStoreDB.replace("\AnimStore.sqlite",""), dFoundPoses[eachPose][1] + ".htm")
            oProp.AddParameter3(dFoundPoses[eachPose][0] + "_Syn", c.siString,myPath)
        
    if DEBUG:
        timeTaken = clock() - start_time
        log(timeTaken)
    
    # Create layout
    # ==============
    oLayout = oProp.PPGLayout
    oLayout.Clear()

    oPlugin = xsi.Plugins("ET_AnimStore_GUI")
    strPluginPath = oPlugin.OriginPath
    strSpacerPath = XSIUtils.BuildPath(strPluginPath, "..", "..", "Data", "Images", "Spacer.bmp")

    oLayout.AddRow()
    oLayout.AddSpacer(5, 10)

    oButton = oLayout.AddButton("Refresh")
    oButton.SetAttribute("CX", 252)
    oButton.SetAttribute("CY", 30)

    oLayout.AddSpacer(5, 10)
    
    oButton = oLayout.AddButton("Clean")
    oButton.SetAttribute("CX", 120)
    oButton.SetAttribute("CY", 30)
    
    oLayout.AddSpacer(5, 10)
    oLayout.EndRow()
    
    if len(dFoundPoses) < 4:
        oLayout.AddSpacer(10, 5)
    
    if len(dFoundPoses) < 1:
        oLayout.AddSpacer(10, 10)
        oLayout.AddRow()
        oLayout.AddSpacer(10, 10)
        oLayout.AddStaticText("No poses found. Please ensure you've set the correct group, action type, and action set.", 380, 150)
        oLayout.AddSpacer(10, 10)
        oLayout.EndRow()
    
    else:
    
        iRow = 1
        iIter = 1
    
        if dFoundPoses:
            for eachPose in sorted(dFoundPoses):
                            
                if iRow == 1: oLayout.AddRow()
                
                oItem = oLayout.AddItem(dFoundPoses[eachPose][0] + "_Syn", "", c.siControlSynoptic)
                oItem.SetAttribute("NoLabel", 1)
                oItem.SetAttribute("AlignLeft", 1)
                oItem.SetAttribute("CX", 100)
                oItem.SetAttribute("CY", 100)
                
                if iRow == 4:
                    oLayout.EndRow()
                    iRow = 1
                elif iRow != 4 and iIter == len(dFoundPoses):
                    iSpacerCount = 4 - iRow
                    
                    for each in range(iSpacerCount):
                        oSpacer = oLayout.AddItem("Spacer", "", c.siControlBitmap)
                        oSpacer.SetAttribute(c.siUIFilePath, strSpacerPath)
                        oSpacer.SetAttribute(c.siUINoLabel, True)
                    
                    oLayout.EndRow()
                else:
                    iRow += 1
                
                iIter += 1


def ET_AnimStore_PoseList_OnInit():
    makePoseListLayout()
    PPG.Refresh()
    

def ET_AnimStore_PoseList_OnClosed():
    oProp = PPG.Inspected(0)
    
    try:
        Application.DeleteObj(oProp)
    except:
        log("Could not delete Pose list")

        
def ET_AnimStore_PoseList_Refresh_OnClicked():
    makePoseListLayout()
    PPG.Refresh()
    
    
def ET_AnimStore_PoseList_Help_OnClicked():
    strHelpPath = XSIUtils.BuildPath(xsi.Plugins("ET_AnimStore_GUI").OriginPath, "..", "..", "netview_ET_AnimStore_v02.htm")
    myView = xsi.Desktop.ActiveLayout.CreateView("NetView", "ET_AnimStore Help")
    myView.SetAttributeValue("url", strHelpPath)

    return
    

def ET_AnimStore_PoseList_Clean_OnClicked():
    import AnimStoreUtils as asu
    import time
    
    oProp = PPG.Inspected(0)
    oModel = oProp.Model
    oGUIProp = oModel.Properties("ET_AnimStore_GUI")
    
    strAnimStoreDB = oGUIProp.Parameters("AnimStoreDB").Value
    strProjectID = oGUIProp.Parameters("ProjectID").Value
    strGroupID = oGUIProp.Parameters("ModelGroup").Value
    strActionType = oGUIProp.Parameters("ActionType").Value
    strSetID = oGUIProp.Parameters("ActionSet").Value
    oSpacer = oProp.Parameters("Spacer").Value
    
    try:
        # Progress Bar
        oProgBar = XSIUIToolkit.ProgressBar
        oProgBar.Caption = "Deleting Invalid Poses:"
        oProgBar.CancelEnabled = False
        oProgBar.Visible = True
    
        dFoundPoses = asu.getPoseList(strProjectID, strGroupID, strActionType, strSetID, strAnimStoreDB)
        if dFoundPoses:
            for eachPose in sorted(dFoundPoses.keys()):
                oProgBar.Value = 0
                
                if strActionType == "pose":
                    lValidFiles = ["htm","bmp","eani"]
                elif strActionType == "animation":
                    lValidFiles = ["htm","bmp","eani","avi"]
                    
                strPosePath = XSIUtils.BuildPath(strAnimStoreDB.replace("\AnimStore.sqlite",""), dFoundPoses[eachPose][1])
                lValids = [strPosePath + "." + x for x in lValidFiles if os.path.exists(strPosePath + "." + x)]
                if len(lValids) != len(lValidFiles):
                
                    oProgBar.Caption = "Deleting Invalid Poses: " + dFoundPoses[eachPose][0]
                    
                    conn = sq.connect(strAnimStoreDB)
                    cursor = conn.cursor()                  
                    intBadPoseRowID = eachPose[3]
                    dataSet = cursor.execute("DELETE FROM actions WHERE rowid=?", [intBadPoseRowID])
                    conn.commit()
                    conn.close()
                    
                    # Delete Files
                    for eachOrphanFile in lValids:
                        if os.path.isfile(eachOrphanFile) and os.path.exists(eachOrphanFile):
                            os.remove(eachOrphanFile)
                    
                    oProgBar.Value = 100                    
                    time.sleep(0.1)
        
        oProgBar.Visible = False

    except Exception, e:
        exc_traceback = sys.exc_info()[2]
        
        strError = """
ET_AnimStore Clean Pose List Exception
===================================="""
        
        strError += "\n" + traceback.format_exception_only(type(e),e)[0]    
        strError += "Line Number: " + str(exc_traceback.tb_lineno)
        
        log(strError,2)
        XSIUIToolkit.MsgBox(strError,c.siMsgOkOnly, "ET_AnimStore Error!")
        
        return False
        
    makePoseListLayout()
    PPG.Refresh()


# =================
# Helper Functions
# =================
def refreshPPG():
    oLayout = PPG.PPGLayout
    oLayout.Clear()
    ET_BuildAnimStoreGUI()
    PPG.Refresh()
    return
    

def passCheck(strProjectID, strPassword):
    conn = sq.connect(PPG.AnimStoreDB.Value)
    cursor = conn.cursor()
    passdata = cursor.execute("""SELECT * FROM projects WHERE rowid='""" + strProjectID + """' AND project_password='""" + strPassword + """' LIMIT 1""")
    lValidProjects = [x for x in passdata]
    
    if not len(lValidProjects):
        log("Invalid Password.",2)
        conn.close()
        return False
    else:
        return True