"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# ET_AnimStore_SynClicks

from win32com.client import constants as c
from win32com.client import Dispatch as d
import sqlite3 as sq
import os,shutil

xsi = d( "XSI.Application" ).Application
collSel = xsi.Selection
log = xsi.LogMessage

oProject = xsi.ActiveProject2
oScene = oProject.ActiveScene
oScnRoot = oProject.ActiveScene.Root

versionMajor = 2
versionMinor = 3

DEBUG = 0

oPoseList = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStoreGUI.ET_PoseList", 0)
strPosePath = ""


def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric Thivierge"
    in_reg.Name = "ET_AnimStore_SynClicks"
    in_reg.Major = versionMajor
    in_reg.Minor = versionMinor

    in_reg.RegisterCommand("ET_AnimStore_LMBSynClick", "ET_AnimStore_LMBSynClick")
    in_reg.RegisterCommand("ET_AnimStore_MMBSynClick", "ET_AnimStore_MMBSynClick")
    in_reg.RegisterCommand("ET_AnimStore_RMBSynClick", "ET_AnimStore_RMBSynClick")

    in_reg.RegisterCommand("ET_AnimStore_CtrlLMBSynClick", "ET_AnimStore_CtrlLMBSynClick")
    in_reg.RegisterCommand("ET_AnimStore_CtrlMMBSynClick", "ET_AnimStore_CtrlMMBSynClick")
    in_reg.RegisterCommand("ET_AnimStore_CtrlRMBSynClick", "ET_AnimStore_CtrlRMBSynClick")

    in_reg.RegisterCommand("ET_AnimStore_AltLMBSynClick", "ET_AnimStore_AltLMBSynClick")
    in_reg.RegisterCommand("ET_AnimStore_AltMMBSynClick", "ET_AnimStore_AltMMBSynClick")
    in_reg.RegisterCommand("ET_AnimStore_AltRMBSynClick", "ET_AnimStore_AltRMBSynClick")

    in_reg.RegisterCommand("ET_AnimStore_KeyActionControls", "ET_AnimStore_KeyActionControls")
    #RegistrationInsertionPoint - do not remove this line

    return True

def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    Application.LogMessage(str(strPluginName) + str(" has been unloaded."), c.siVerbose)
    return True


def checkSel():
    collSel = xsi.Selection
    dSel = {}

    if collSel.Count == 0:
        log("Nothing selected. Please select part of a character.", 2)
        return False

    dSel["Valid"] = True

    if collSel(0).Type =="#model":
        oModel = collSel(0)
    else:
        oModel = collSel(0).Model

    dSel["Model"] = oModel

    return dSel


#==============
# Normal Clicks
#==============
def ET_AnimStore_LMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True


def ET_AnimStore_LMBSynClick_Execute(strActionPath,strActionName,strIsAnim,intActionID):
    oProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDBPath = str(oProp.Parameters("AnimStoreDB").Value).replace("AnimStore.sqlite", "")
    strAction = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName + ".eani")

    oPlayControl = xsi.ActiveProject.Properties.Find("PlayControl")
    Fc = oPlayControl.Parameters("Current").Value

    checkSelection = checkSel()
    if not checkSelection:
        log("Nothing selected. Please select a controller for the character.", 2)
        return False

    oAction = xsi.ImportAction(checkSelection["Model"], strAction)
    sclActionLen = oAction.Parameters("FrameEnd").Value - oAction.Parameters("FrameStart").Value
    xsi.ApplyAction(oAction,"", True, Fc, Fc + sclActionLen, 1)
    xsi.DeleteObj(oAction)
    return True


def ET_AnimStore_MMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True

def ET_AnimStore_MMBSynClick_Execute(strActionPath, strActionName, strIsAnim, intActionID):
    if DEBUG:
        log(strActionPath)
        log(strActionName)
        log(strIsAnim)
        log(intActionID)

    import AnimStoreUtils as asu
    reload(asu)

    oETAnimStoreProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDB = str(oETAnimStoreProp.Parameters("AnimStoreDB").Value)
    strDBPath = str(oETAnimStoreProp.Parameters("AnimStoreDB").Value).replace("\AnimStore.sqlite", "")
    strProjectID = str(oETAnimStoreProp.Parameters("ProjectID").Value)
    strAction = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName)

    msgBox = XSIUIToolkit.MsgBox("Are you sure you want to delete the pose / animation?", c.siMsgYesNo, "Are you sure?")
    if msgBox == 6: # Check to see if the user cancels the Modal PPG

        try:
            # Delete from DB
            conn = sq.connect(strDB)
            cursor = conn.cursor()

            data = cursor.execute("""DELETE FROM actions WHERE rowid='""" + intActionID + """'""")
            conn.commit()
            conn.close()

            if DEBUG: log("Deleted Pose", 8)

            # Delete Files
            lFiles = ["eani","htm","bmp","avi"]
            for each in lFiles:
                strFilePath = XSIUtils.BuildPath(strDBPath, strActionPath, strActionName + "." + each)

                if DEBUG:
                    log(strFilePath)

                if  os.path.isfile(strFilePath) and os.path.exists(strFilePath):
                    os.remove(strFilePath)

            return True
        except Exception, strerror:
            log(strerror, 2)
            return False
    else:
        log("User Cancelled", 2)


def ET_AnimStore_RMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True

def ET_AnimStore_RMBSynClick_Execute(strActionPath,strActionName,strIsAnim,intActionID):
    oProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDBPath = str(oProp.Parameters("AnimStoreDB").Value).replace("AnimStore.sqlite", "")
    strAction = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName + ".eani")

    oPlayControl = xsi.ActiveProject.Properties.Find("PlayControl")
    Fc = oPlayControl.Parameters("Current").Value

    checkSelection = checkSel()
    if not checkSelection:
        log("Nothing selected. Please select a controller for the character.", 2)
        return False

    try:
        strActionPath = XSIUtils.BuildPath(strPosePath,strActionName + ".eani")
        oAction = xsi.ImportAction(checkSelection["Model"], strAction)
        sclActionLen = oAction.Parameters("FrameEnd").Value - oAction.Parameters("FrameStart").Value

        if strIsAnim == "0": # If not an anim clip
            xsi.ApplyAction(oAction,"", True, Fc, Fc + sclActionLen, 1)
            xsi.ET_AnimStore_KeyActionControls(oAction)

        elif strIsAnim == "1": # If is an anim clip
            xsi.ApplyAction(oAction,"", True, Fc, Fc + sclActionLen, 0)

    except Exception, strerror:
        log(strerror, 2)
        return False
    finally:
        xsi.DeleteObj(oAction)


#==============
# Ctrl + Clicks
#==============

def ET_AnimStore_CtrlLMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True


def ET_AnimStore_CtrlLMBSynClick_Execute(strActionPath,strActionName,strIsAnim,intActionID):
    oProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDBPath = str(oProp.Parameters("AnimStoreDB").Value).replace("AnimStore.sqlite", "")
    strAction = XSIUtils.BuildPath(strDBPath, strActionPath, strActionName + ".eani")

    oPlayControl = xsi.ActiveProject.Properties.Find("PlayControl")
    Fc = oPlayControl.Parameters("Current").Value

    checkSelection = checkSel()
    if not checkSelection:
        log("Nothing selected. Please select a controller for the character.", 2)
        return False

    try:
        oAction = xsi.ImportAction(checkSelection["Model"], strAction)
        sclActionLen = oAction.Parameters("FrameEnd").Value - oAction.Parameters("FrameStart").Value

        colSourceItems = oAction.SourceItems
        for eachItem in colSourceItems:
            oSourceTgt = str(eachItem.Name).split(".")[0]

            lSelected = [eachObj.Name for eachObj in collSel]
            if oSourceTgt not in lSelected:
                eachItem.Active = 0

        if strIsAnim == "0":
            xsi.ApplyAction(oAction,None)
        elif strIsAnim == "1":
            xsi.ApplyAction(oAction, "", True, Fc, Fc + sclActionLen, 1)
            #xsi.ApplyAction(oAction,None)

        return True
    except Exception, strerror:
        log(strerror, 2)
        return False
    finally:
        xsi.DeleteObj(oAction)
        pass


def ET_AnimStore_CtrlMMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True


def ET_AnimStore_CtrlMMBSynClick_Execute(strActionPath, strActionName, strIsAnim, intActionID):
    # Pose
    if strIsAnim == "0":
        log("Function not defined for Poses.", 2)
        return True
    # Anim
    elif strIsAnim == "1":
        log("Function not defined for Animations.", 2)
        return True


def ET_AnimStore_CtrlRMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True


def ET_AnimStore_CtrlRMBSynClick_Execute(strActionPath, strActionName, strIsAnim, intActionID):
    oProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDBPath = str(oProp.Parameters("AnimStoreDB").Value).replace("AnimStore.sqlite", "")
    strAction = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName + ".eani")

    checkSelection = checkSel()
    if not checkSelection:
        log("Nothing selected. Please select a controller for the character.", 2)
        return False

    try:
        oAction = xsi.ImportAction(checkSelection["Model"],strAction)

        colSourceItems = oAction.SourceItems
        for eachItem in colSourceItems:
            oSourceTgt = str(eachItem.Name).split(".")[0]

            lSelected = [eachObj.Name for eachObj in collSel]
            if oSourceTgt not in lSelected:
                eachItem.Active = 0

        if strIsAnim == "0":
            xsi.ApplyAction(oAction)
            xsi.ET_AnimStore_KeyActionControls(oAction)
        elif strIsAnim == "1":
            xsi.ApplyAction(oAction,"", True, Fc, Fc + sclActionLen, 0)

        return True
    except Exception, strerror:
        log(strerror, 2)
        return False
    finally:
        xsi.DeleteObj(oAction)


#==============
# Alt + Clicks
#==============

def ET_AnimStore_AltLMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True

def ET_AnimStore_AltLMBSynClick_Execute(strActionPath,strActionName,strIsAnim,intActionID):
    oProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDBPath = str(oProp.Parameters("AnimStoreDB").Value).replace("AnimStore.sqlite","")
    strAction = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName + ".eani")

    checkSelection = checkSel()
    if not checkSelection:
        log("Nothing selected. Please select a controller for the character.", 2)
        return False

    try:
        oModel = checkSelection["Model"]

        if not oModel.HasMixer():
            oAddMixer = oModel.AddMixer()

        oAction = xsi.ImportAction(oModel,strAction)
        oTrack = xsi.AddTrack(oModel, None, 0, strActionName + "_Track")
        oClip = xsi.AddClip(oModel, oAction, None, oTrack, None, strActionName + "_Clip")

        oViews = xsi.Desktop.ActiveLayout.Views.Find("Animation Mixer") # See if anim mixer is open
        if not oViews:
            popWindow("Animation Mixer", 150, 150, 800, 500)

        return True
    except Exception, strerror:
        log(strerror,2)
        return False

def ET_AnimStore_AltMMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True


def ET_AnimStore_AltMMBSynClick_Execute(strActionPath,strActionName,strIsAnim,intActionID):
    oProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDBPath = str(oProp.Parameters("AnimStoreDB").Value).replace("AnimStore.sqlite", "")
    strAction = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName + ".eani")

    checkSelection = checkSel()
    if not checkSelection:
        log("Nothing selected. Please select a controller for the character.", 2)
        return False

    oAction = xsi.ImportAction(checkSelection["Model"], strAction)
    intStartFrame = oAction.Parameters("FrameStart").Value
    intEndFrame = oAction.Parameters("FrameEnd").Value

    XSIUIToolkit.MsgBox("Start Frame: " + str(intStartFrame) + "\n" + "End Frame: " + str(intEndFrame), c.siMsgOkOnly, oAction.Name + " Info")


def ET_AnimStore_AltRMBSynClick_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("strActionPath", c.siArgumentInput)
    oArgs.Add("strActionName", c.siArgumentInput)
    oArgs.Add("strIsAnim", c.siArgumentInput)
    oArgs.Add("intActionID", c.siArgumentInput)
    return True

def ET_AnimStore_AltRMBSynClick_Execute(strActionPath, strActionName, strIsAnim, intActionID):
    oProp = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_GUI", False)
    strDBPath = str(oProp.Parameters("AnimStoreDB").Value).replace("AnimStore.sqlite", "")
    strAction = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName + ".eani")
    strMoviePath = XSIUtils.BuildPath(strDBPath,strActionPath,strActionName + ".avi")

    # Pose
    if strIsAnim == "0":
        log("Function not defined for Poses.", 2)
        return True
    # Anim \ Open Flipbook and play movie file.
    elif strIsAnim == "1":
        xsi.Flipbook("-i " + strMoviePath + " -m")
        return True


def ET_AnimStore_KeyActionControls_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siSupportsKeyAssignment, False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.SetFlag(c.siNoLogging, True)
    oCmd.ReturnValue = True

    oArgs = oCmd.Arguments
    oArgs.Add("InAction")
    return True

def ET_AnimStore_KeyActionControls_Execute( InAction ):
    oAction = InAction
    oModel = oAction.Model
    collItems = oAction.SourceItems
    oString = ""

    for eachItem in collItems:
        oItemName = str(eachItem).split(".")
        oString = oString + str(oModel.Name) + "." + str(eachItem) + ","
    xsi.SaveKeyOnKeyable(oString)
    return True


#=================
# Helper functions
#=================

def addClip(strActionName,strIsAnim,inCurFrame = False):

    oPlayControl = xsi.ActiveProject.Properties.Find("PlayControl")
    Fc = oPlayControl.Parameters("Current").Value

    collSel = xsi.Selection

    if collSel(0).Type == "#model":
        oModel = collSel(0)
    else:
        oModel = collSel(0).Model

    strPosePath = strPosePath
    strActionPath = XSIUtils.BuildPath(strPosePath, strActionName + ".eani")

    if os.path.isfile(strActionPath):
        oAction = xsi.ImportAction(oModel, strActionPath)
    else:
        log("Cannot find specified file.", 2)
        return False

    if not oModel.HasMixer():
        oAddMixer = oModel.AddMixer()

    if inCurFrame:
        intAddClipTime = Fc
    else:
        intAddClipTime = 1

    strCleanActionName = oAction.Name.split("_")[2] # Clean extra info off action name
    oTrack = xsi.AddTrack(oModel, None, 0, strCleanActionName + "_Track")
    oClip = xsi.AddClip(oModel, oAction, None, oTrack, intAddClipTime, strCleanActionName + "_Clip")

    oViews = xsi.Desktop.ActiveLayout.Views.Find("Animation Mixer") # See if anim mixer is open

    if not oViews:
        popWindow("Animation Mixer", 150, 150, 800, 500)

def popWindow(inType, moveX, moveY, resizeX, resizeY):

    myView = xsi.Desktop.ActiveLayout.CreateView(inType, inType)
    myView.BeginEdit()
    myView.Move(moveX, moveY)
    myView.Resize(resizeX, resizeY)
    myView.EndEdit()

    return True


def passCheck(strDB, strProjectID, strPassword):
    conn = sq.connect(strDB)
    cursor = conn.cursor()
    passdata = cursor.execute("""SELECT * FROM projects WHERE rowid='""" + strProjectID + """' AND project_password='""" + strPassword + """' LIMIT 1""")
    lValidProjects = [x for x in passdata]

    if not len(lValidProjects):
        log("Invalid Password.", 2)
        conn.close()
        return False
    else:
        return True