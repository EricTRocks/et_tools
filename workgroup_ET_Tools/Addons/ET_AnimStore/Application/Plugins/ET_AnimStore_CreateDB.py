"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# ET_AnimStoreCreateDB

from win32com.client import constants as c
from win32com.client import Dispatch as d

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

versionMajor = 2
versionMinor = 3

DEBUG = 0


def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric Thivierge"
    in_reg.Name = "ET_AnimStore_CreateDB"
    in_reg.Major = versionMajor
    in_reg.Minor = versionMinor

    in_reg.RegisterCommand("ET_AnimStore_CreateDB", "ET_AnimStore_CreateDB")
    #RegistrationInsertionPoint - do not remove this line

    return True

def ET_AnimStore_CreateDB_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    return True

def ET_AnimStore_CreateDB_Execute(  ):

    import AnimStoreUtils as asu
    import os,shutil
    
    oActiveProject = xsi.ActiveProject2
    oPlugin = xsi.Plugins("ET_AnimStore_GUI")
    strPluginPath = oPlugin.OriginPath
    
    # Create Modal Property and Parameters
    oProp = XSIFactory.CreateObject("CustomProperty")
    oProp.Name = "Create New ET_AnimStore Database"
    strNewDBPath = oProp.AddParameter3("NewDBPath", c.siString)

    oLayout = oProp.PPGLayout
    oLayout.AddGroup("")
    oLayout.AddSpacer(10, 10)
    oLayout.AddRow()
    oLayout.AddSpacer(10, 10)
    oDBPath = oLayout.AddItem("NewDBPath","New Database Path", c.siControlFolder)
    oDBPath.SetAttribute(c.siUILabelMinPixels, 125)
    oDBPath.SetAttribute(c.siUIInitialDir, oActiveProject.Path)
    oLayout.AddSpacer(10, 10)
    oLayout.EndRow()
    oLayout.AddSpacer(10, 10)
    oLayout.EndGroup()
    oLayout.AddSpacer(10, 10)

    oLayout.AddGroup()
    oLayout.AddSpacer(10, 10)
    oLayout.AddRow()
    oLayout.AddSpacer(10, 10)
    oMessage = oLayout.AddStaticText("NOTE:\nThe AnimStore directory will be created under the specified folder above.", 400, 30)
    oLayout.AddSpacer(10,10)
    oLayout.EndRow()
    oLayout.AddSpacer(10,10)
    oLayout.EndGroup()


    oModal = xsi.InspectObj(oProp, None,"Create New ET_AnimStore Database", 4, False)
        
    if oModal == False: # Check to see if the user cancels the Modal PPG
        strDBPath = oProp.Parameters("NewDBPath").Value
        strDBDir = XSIUtils.BuildPath(strDBPath,"AnimStore")
        strDefaultDBFile = XSIUtils.BuildPath(strPluginPath, "..", "..", "Data", "AnimStore.sqlite")
        strNewDBFile = XSIUtils.BuildPath(strDBPath,"AnimStore", "AnimStore.sqlite")
        
        if not os.path.isdir(oProp.Parameters("NewDBPath").Value):
            log("Not a valid directory!", 2)
            return
        
        try:
            if os.path.isfile(strNewDBFile):
                log("AnimStore.sqlite file already exists!", 2)
                return False
            
            os.mkdir(strDBDir)
            shutil.copyfile(strDefaultDBFile, strNewDBFile)
            
            reload(asu)
            asu.updateConfig(strNewDBFile)
            
            xsi.ET_AnimStore_CreateGUI()
            
            return True
            
        except (IOError,OSError), (errno, strerror):
            log("Error creating AnimStore Database", 2)
            log(str(errno) + ": " + strerror, 2)
            return False

    else:
        log("User Cancelled", 2)
        
        return False
