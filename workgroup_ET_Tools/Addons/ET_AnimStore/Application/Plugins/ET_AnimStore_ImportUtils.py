"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# ET_AnimStoreImportUtils

from win32com.client import constants as c
from win32com.client import Dispatch as d
import sys

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

versionMajor = 2
versionMinor = 3

DEBUG = 0

def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric Thivierge"
    in_reg.Name = "ET_AnimStore"
    in_reg.Major = versionMajor
    in_reg.Minor = versionMinor

    in_reg.RegisterEvent("ET_AnimStore_UtilImport", c.siOnStartup)
    #RegistrationInsertionPoint - do not remove this line

    return True


def ET_AnimStore_UtilImport_OnEvent( in_ctxt ):

    collPlugins = xsi.Plugins
    oPluginPath = collPlugins("ET_AnimStore").OriginPath    
    oPluginPath.replace("ET_AnimStore.py", "")
    oModulePath = XSIUtils.BuildPath(oPluginPath, "..", "..", "Modules")
    
    if oModulePath not in sys.path :
        sys.path.append( oModulePath )

    return True

