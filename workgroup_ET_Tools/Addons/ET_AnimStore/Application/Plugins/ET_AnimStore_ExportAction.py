"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# ET_AnimStore_ExportAction Plugin

from win32com.client import constants as c
from win32com.client.dynamic import Dispatch as d
import sqlite3 as sq
import os, shutil
import sys
import traceback

xsi = Application
log = xsi.LogMessage
oScnRoot = xsi.ActiveProject2.ActiveScene.Root
collSel = xsi.Selection

versionMajor = 2
versionMinor = 3

DEBUG = 0

def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric Thivierge"
    in_reg.Name = "ET_AnimStore_ExportAction"
    in_reg.Major = versionMajor
    in_reg.Minor = versionMinor

    in_reg.RegisterProperty("ET_AnimStore_ExportAction")
    #RegistrationInsertionPoint - do not remove this line

    return True


def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    Application.LogMessage(str(strPluginName) + str(" has been unloaded."),c.siVerbose)
    return True


def ET_AnimStore_ExportAction_Define( in_ctxt ):
    oCustomProperty = in_ctxt.Source
    oCustomProperty.AddParameter2("ActionName", c.siString, "", None, None, None, None, c.siClassifUnknown, c.siPersistable)
    oCustomProperty.AddParameter2("StartFrame", c.siInt4, 101, 0, 10000, 0, 1000, c.siClassifUnknown, c.siPersistable)
    oCustomProperty.AddParameter2("EndFrame", c.siInt4, 201, 0, 10000, 0, 1000, c.siClassifUnknown, c.siPersistable)
    return True

# ========
# Classes
# ========
class captureManager(object):
    
    def __init__(self):
        self.dCaptureSettings = {}
        self.save()


    def save(self):
        """Saves current capture settings."""
        
        oCaptureOptions = d(xsi.Dictionary.GetObject("ViewportCapture"))
        collNestedOptions = oCaptureOptions.NestedObjects
        
        for eachNested in collNestedOptions:
            self.dCaptureSettings[eachNested.Name] = eachNested.Value
            
        return True
        
        
    def load(self):
        """Loads capture settings from internal data."""
        
        assert self.dCaptureSettings, "Capture settings have not been saved."
        
        oCaptureOptions = d(xsi.Dictionary.GetObject("ViewportCapture"))
        collNestedOptions = oCaptureOptions.NestedObjects
        
        for eachNested in collNestedOptions:
            oLoadValue = self.dCaptureSettings[eachNested.Name]
            if not oLoadValue:
                continue
                
            eachNested.Value = self.dCaptureSettings[eachNested.Name]
            
        return True


# ==========
# Functions
# ==========
def printException( exc, exc_traceback ):
    xsi.LogMessage("Unexpected ET_AnimStore Error Occured! ================================",2)
    xsi.LogMessage("Message: " + traceback.format_exception_only(type(exc), exc)[0], 2)
    xsi.LogMessage("Line: " + str(exc_traceback.tb_lineno), 2)
    xsi.LogMessage("Full Strack Trace:", 2)
    xsi.LogMessage(traceback.format_tb( exc_traceback ), 2)
    xsi.LogMessage("Softimage Version: " + xsi.Version(), 2)


def buildLayout():
    oProp = PPG.Inspected(0)
    oModel = oProp.Model
    oGUIProp = oModel.Properties("ET_AnimStore_GUI")
    strAnimStoreDB = oGUIProp.Parameters("AnimStoreDB").Value
    strProjectID = oGUIProp.Parameters("ProjectID").Value
    strGroupID = oGUIProp.Parameters("ModelGroup").Value
    strActionType = oGUIProp.Parameters("ActionType").Value
    strSetID = oGUIProp.Parameters("ActionSet").Value
    
    oLayout = PPG.PPGLayout
    oLayout.Clear()
    
    oLayout.AddRow()
    oButton = oLayout.AddButton("Refresh")
    oButton.SetAttribute("CX", 365)
    oButton.SetAttribute("CY", 30)
    oLayout.EndRow()
    
    oLayout.AddSpacer(10, 10)
    
    oLayout.AddGroup("Export Settings")
    oLayout.AddSpacer(10, 10)
    
    oLayout.AddRow()
    oLayout.AddSpacer(10, 10)
    oLayout.AddItem("IsAnimation","Animation", c.siControlBoolean)
    oLayout.AddItem("ActionName","Name", c.siControlString)
    oLayout.AddSpacer(10, 10)
    oLayout.EndRow()
    
    oLayout.AddSpacer(10, 10)

    if strActionType == "animation":
        oLayout.AddRow()
        oLayout.AddSpacer(10, 10)
        oLayout.AddItem("StartFrame","Start Frame", c.siControlNumber)
        oLayout.AddSpacer(10, 10)
        oLayout.EndRow()
        
        oLayout.AddSpacer(10, 10)
        
        oLayout.AddRow()
        oLayout.AddSpacer(10, 10)
        oLayout.AddItem("EndFrame","End Frame", c.siControlNumber)
        oLayout.AddSpacer(10, 10)
        oLayout.EndRow()
        
        oLayout.AddSpacer(10, 10)

    oLayout.AddRow()
    oLayout.AddSpacer(10, 10)
    oLayout.AddButton("StoreAction","Store Action")
    oLayout.AddSpacer(10, 10)
    oLayout.EndRow()

    oLayout.AddSpacer(10, 10)
    oLayout.EndGroup()
    

def ET_AnimStore_ExportAction_DefineLayout( in_ctxt ):
    return True


def ET_AnimStore_ExportAction_OnInit( ):
    buildLayout()
    PPG.Refresh()
    return


def ET_AnimStore_ExportAction_OnClosed( ):
    xsi.DeleteObj(PPG.Inspected(0))
    return


def ET_AnimStore_ExportAction_Refresh_OnClicked():
    buildLayout()
    PPG.Refresh()
    
    
def ET_AnimStore_ExportAction_StoreAction_OnClicked( ):
    import AnimStoreUtils as asu
    
    oProp = PPG.Inspected(0)
    oModel = oProp.Model
    oGUIProp = oModel.Properties("ET_AnimStore_GUI")
    
    strActionName = PPG.ActionName.Value
    intStartFrame = PPG.StartFrame.Value
    intEndFrame = PPG.EndFrame.Value
    strAnimStoreDB = oGUIProp.Parameters("AnimStoreDB").Value
    strProjectID = oGUIProp.Parameters("ProjectID").Value
    strGroupID = oGUIProp.Parameters("ModelGroup").Value
    strActionType = oGUIProp.Parameters("ActionType").Value
    strSetID = oGUIProp.Parameters("ActionSet").Value
    
    if DEBUG:
        log("Action Name: " + strActionName)
        log("Start Frame: " + str(intStartFrame))
        log("End Frame: " + str(intEndFrame))
        log("Project ID: " + strProjectID)
        log("Group ID: " + strGroupID)
        log("Set ID: " + strSetID)
        log("Action Type: " + strActionType)
        log("DB Path: " + strAnimStoreDB)
        
    dActionInfo = asu.getActionInfo(strProjectID, strGroupID, strSetID, strAnimStoreDB)
    strCleanPath = strAnimStoreDB.replace("AnimStore.sqlite", "")
    strExportPath = XSIUtils.BuildPath(strCleanPath, "Projects", dActionInfo["Project"], dActionInfo["Group"], strActionType, dActionInfo["Set"])
    
    if strActionType == "pose":
        intDimension = 100
    else:
        intDimension = 400

    # Export
    # ==================    
    try:
        bExportOK = 0
        intLastRowID = -10000
        oPoseText = xsi.Dictionary.GetObject("ET_AnimStore_Model.PoseName_Text",0)
        oPoseMesh = xsi.Dictionary.GetObject("ET_AnimStore_Model.PoseName_Mesh",0)
        collInitSel = d("XSI.Collection")
        collInitSel.SetAsText(collSel.GetAsText())
        
        lCheckPoseName = asu.checkStringInput(strActionName, "Pose Name")
        if not lCheckPoseName:
            raise Exception("Pose Name not valid.")
            
        if len(collSel) < 1:
            raise Exception("Selection is empty. Please select controls to store.")     
                
        oPC = xsi.ActiveProject2.Properties("Play Control")
        intFC = oPC.Parameters("Current").Value
        
        # insert into DB
        conn = sq.connect(strAnimStoreDB)
        cursor = conn.cursor()
        actionQuery = cursor.execute("SELECT actions.action_name,actions.set_id FROM actions INNER JOIN sets ON actions.set_id=sets.rowid WHERE action_name= ? AND actions.set_id= ? LIMIT 1", [strActionName, strSetID])
        lFoundActions = [x for x in actionQuery]
        if len(lFoundActions):
            raise sq.IntegrityError("Pose already in DB.")
        
        data = cursor.execute("INSERT INTO actions (action_name,set_id,action_type) VALUES (?,?,?)", [strActionName, strSetID, strActionType])
        conn.commit()
        conn.close()
        
        intLastRowID = cursor.lastrowid
        
        oPoseText.Text = "_RTF_{\\rtf1\\ansi\\ansicpg1252\\deff0{\\fonttbl{\\f0\\fnil\\fprq4\\fcharset0 Arial;}}\r\n\\viewkind4\\uc1\\pard\\qc\\lang3082\\b\\f0\\fs20 " + strActionName + "\\par\r\n}\r\n"
        if len(strActionName) > 9:
            oPoseText.fitsize.Value = 0.25
        else:
            oPoseText.fitsize.Value = 0.35
        
        if strActionType == "pose":
            oPoseMesh.Properties("Visibility").Parameters("viewvis").Value = 1
            exportAction(strActionName, strExportPath) # Make Action and export
            buildSynoptic(strExportPath, strActionName, intLastRowID) # Build Synoptic and export
            oIcon = ViewportCapture(strActionType, intDimension, strExportPath, strActionName, intFC, intFC) # Save Icon
            
            if oIcon == False:
                raise Exception("Viewport Capture Failed!")
                
        elif strActionType == "animation":
            oPoseMesh.Properties("Visibility").Parameters("viewvis").Value = 1
            exportAction(strActionName, strExportPath, 1, intStartFrame, intEndFrame) # Make Action and export
            buildSynoptic(strExportPath, strActionName, intLastRowID, bIsAnim=1) # Build Synoptic and export
            oIcon = ViewportCapture("pose", 100, strExportPath, strActionName, intFC, intFC) # Save Icon
            collSel.SetAsText(collInitSel.GetAsText())
            oVid = ViewportCapture(strActionType, intDimension, strExportPath, strActionName, intStartFrame, intEndFrame) # Save Preview Movie
            
            if oIcon == False or oVid == False:
                raise Exception("Viewport Capture Failed!")
    
        bExportOK = 1
    
    except Exception, e:
        printException(e, sys.exc_info()[2])
        
        return False

    finally:
        
        if not bExportOK:
            if intLastRowID != -10000:
                conn = sq.connect(strAnimStoreDB)
                cursor = conn.cursor()
                actionQuery = cursor.execute("DELETE FROM actions WHERE rowid=?", [intLastRowID])
            strActionPath = XSIUtils.BuildPath(strExportPath,strActionName + ".eani")
            strSynopticPath = XSIUtils.BuildPath(strExportPath,strActionName + ".htm")
            strIconPath = XSIUtils.BuildPath(strExportPath,strActionName + ".bmp")
            strVidPath = XSIUtils.BuildPath(strExportPath,strActionName + ".avi")
            if os.path.isfile(strActionPath): os.remove(strActionPath)
            if os.path.isfile(strSynopticPath): os.remove(strSynopticPath)
            if os.path.isfile(strIconPath): os.remove(strIconPath)
        
        oPoseMesh.Properties("Visibility").Parameters("viewvis").Value = 0
        
#==================
# Helper Functions
#==================
def ViewportCapture(bIsAnim, intDimension, strPath, strFileName, intAnimStart=0, intAnimEnd=100):
    """Capture viewport based on whether it is for icon or preview sequence."""

    oPlayControl = xsi.ActiveProject.Properties.Find("PlayControl")
    intFC = oPlayControl.Parameters("Current").Value
    oViewportCapture = xsi.Dictionary.GetObject("ViewportCapture")
    oCaptureManager = captureManager()
    strPluginPath = xsi.Plugins("ET_AnimStore_GUI").OriginPath
    
    dictViewport = {"A":1, "B":2, "C":3, "D":4}
    oViews = xsi.Desktop.Activelayout.Views
    oViewMan = oViews.Find("View Manager")
    strFocusedViewport = oViewMan.GetAttributeValue("focusedviewport")
    strViewportView = oViewMan.GetAttributeValue("viewport:" + strFocusedViewport)
    
    if strViewportView == "Viewer":
        oActiveCam = oViewMan.GetAttributeValue("activecamera:" + strFocusedViewport)
        
    if bIsAnim == "pose":
        xsi.LoadPreset(XSIUtils.BuildPath(strPluginPath, "..", "..", "Data", "IconCapture.Preset"), "ViewportCapture")
        strFilePath = XSIUtils.BuildPath(str(strPath), str(strFileName) + ".bmp")
        xsi.Dictionary.GetObject("ViewportCapture.Start").Value = intFC
        xsi.Dictionary.GetObject("ViewportCapture.End").Value = intFC
        xsi.Dictionary.GetObject("ViewportCapture.OpenGLAntiAliasing").Value = 4
        xsi.Dictionary.GetObject("ViewportCapture.ImageWidth").Value = intDimension
        xsi.Dictionary.GetObject("ViewportCapture.ImageHeight").Value = intDimension
        xsi.Dictionary.GetObject("ViewportCapture.Filename").Value = strFilePath
        xsi.Dictionary.GetObject("ViewportCapture.Padding").Value = "(fn)(ext)"
        xsi.Dictionary.GetObject("ViewportCapture.LaunchFlipbook").Value = 0

    elif bIsAnim == "animation":
        if not XSIUtils.Is64BitOS():
            xsi.LoadPreset(XSIUtils.BuildPath(strPluginPath, "..", "..", "Data", "MovieCapture_32bit.Preset"), "ViewportCapture")
        else:
            xsi.LoadPreset(XSIUtils.BuildPath(strPluginPath, "..", "..", "Data", "MovieCapture_64bit.Preset"), "ViewportCapture")
        strFilePath = XSIUtils.BuildPath(str(strPath),str(strFileName) + ".avi")
        xsi.Dictionary.GetObject("ViewportCapture.Start").Value = intAnimStart
        xsi.Dictionary.GetObject("ViewportCapture.End").Value = intAnimEnd
        xsi.Dictionary.GetObject("ViewportCapture.OpenGLAntiAliasing").Value = 4
        xsi.Dictionary.GetObject("ViewportCapture.ImageWidth").Value = 400
        xsi.Dictionary.GetObject("ViewportCapture.ImageHeight").Value = 400
        xsi.Dictionary.GetObject("ViewportCapture.Filename").Value = strFilePath
        xsi.Dictionary.GetObject("ViewportCapture.Padding").Value = "(fn)(ext)"
        xsi.Dictionary.GetObject("ViewportCapture.LaunchFlipbook").Value = 0

    try:
        XSIUtils.EnsureFolderExists(strFilePath, 1)
        oCam = xsi.Dictionary.GetObject("ET_AnimStore_Model.ET_AnimStore_PoseCam", False)
        oCam.Properties("Camera Visibility").Parameters("objcurves").Value = False
        oCam.Properties("Camera Visibility").Parameters("objctrlchnjnts").Value = False
        
        oModel = collSel(0).Model       
        oChildren = oModel.FindChildren("", "", "", True)
        collSel.SetAsText(oChildren.GetAsText())
        
        oETPoseCam = xsi.Dictionary.GetObject("ET_AnimStore_Model.PoseName_Mesh", False)
        collSel.Add(oETPoseCam, c.siSelectDefault)
        xsi.IsolateSelected(None, -1)
        collSel.Clear()
                
        oViewMan.SetAttributeValue("activecamera:" + strFocusedViewport, "ET_AnimStore_Model.ET_AnimStore_PoseCam") # Set Pose Camera
        xsi.CaptureViewport(dictViewport[strFocusedViewport], 0)
        
        if strViewportView == "Viewer":
            oViewMan.SetAttributeValue("activecamera:" + strFocusedViewport, oActiveCam)
        else:
            oViewMan.SetAttributeValue("viewport:" + strFocusedViewport, strViewportView)
        
        oCaptureManager.load()
        
    except Exception, e:
        printException(e, sys.exc_info()[2])
        
        return False

    finally:
        oCam.Properties("Camera Visibility").Parameters("objcurves").Value = True
        oCam.Properties("Camera Visibility").Parameters("objctrlchnjnts").Value = True
        xsi.IsolateAll("", 1)


def exportAction(strActionName,strExportPath,bIsAnim=0,intAnimStartFrame=1,intAnimEndFrame=100):
    
    oModel = collSel(0).Model
    oColl = d( "XSI.Collection" )
    oColl.AddItems(collSel)
    lKeyables = oColl.FindObjectsByMarkingAndCapabilities("", 2048)
    lKeyables = [x for x in lKeyables if x.ValueType in [2, 3, 4, 5, 22]]

    oAction = oModel.AddActionSource(strActionName)
    oAction.Parameters("FrameStart").Value = intAnimStartFrame
    oAction.Parameters("FrameEnd").Value = intAnimEndFrame
    if not bIsAnim:
        for oItem in lKeyables:
            strFullName = oItem.FullName
            strRelName = strFullName.partition(".")
            strRelName = strRelName[2]
            
            oType = str(oItem.Source).rpartition(".")
            if oType[2] in ["FCurve","None"]:
                oSrcItem = oAction.AddSourceItem(strRelName,oItem.Source)
                oSrcItem.SetAsStatic(oItem.GetValue2())
    elif bIsAnim:
        for oItem in lKeyables:
            strFullName = oItem.FullName
            strRelName = strFullName.partition(".")
            strRelName = strRelName[2]
            
            oType = str(oItem.Source).rpartition(".")
            if oType[2] in ["FCurve"]:
                oSrcItem = oAction.AddSourceItem(strRelName, oItem.Source)
                oSrcItem.Source = oItem.Source
            elif oType[2] in ["None"]:
                oSrcItem = oAction.AddSourceItem(strRelName, oItem.Source)
                oSrcItem.SetAsStatic(oItem.GetValue2())

    try:
        xsi.ExportAction(oAction, strExportPath + "\\" + strActionName + ".eani")
        xsi.DeleteObj(oAction)
        return True     
    
    except Exception, e:
        printException(e, sys.exc_info()[2])
        
        return False

    finally:
        xsi.DeleteObj(oAction)


def buildSynoptic(strExportPath, strActionName, intLastRowID, bIsAnim=0):
    import textwrap
    
    try:
        strPath = XSIUtils.BuildPath(strExportPath, strActionName + ".htm")
        f = open(strPath, 'w')
        
        try:
            strHTML = """
<html>
<body version=\"2\">

<script language=\"Python\">
xsi = Application
oProject = xsi.ActiveProject2

def PoseClick(in_obj,in_mousebutton,in_keymodifier):
    strActionPath = \"""" "\\\\" + "\\\\".join(strExportPath.rsplit("\\", 5)[-5:]) + """\"
    strActionName = \"""" + strActionName + """\"
    strIsAnim = \"""" + str(bIsAnim) + """\"
    intActionID = \"""" + str(intLastRowID) + """\"
        
    if in_mousebutton == 0 and in_keymodifier == 0:
        xsi.ET_AnimStore_LMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)
    if in_mousebutton == 1 and in_keymodifier == 0:
        xsi.ET_AnimStore_MMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)
    if in_mousebutton == 2 and in_keymodifier == 0:
        xsi.ET_AnimStore_RMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)

    if in_mousebutton == 0 and in_keymodifier == 2:
        xsi.ET_AnimStore_CtrlLMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)
    if in_mousebutton == 1 and in_keymodifier == 2:
        xsi.ET_AnimStore_CtrlMMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)
    if in_mousebutton == 2 and in_keymodifier == 2:
        xsi.ET_AnimStore_CtrlRMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)

    if in_mousebutton == 0 and in_keymodifier == 4:
        xsi.ET_AnimStore_AltLMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)
    if in_mousebutton == 1 and in_keymodifier == 4:
        xsi.ET_AnimStore_AltMMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)
    if in_mousebutton == 2 and in_keymodifier == 4:
        xsi.ET_AnimStore_AltRMBSynClick(strActionPath,strActionName,strIsAnim,intActionID)

</script>

<map name=\"SynopticMap\">
<area shape=\"rect\" coords=\"0,0,100,100\" title=\"""" + strActionName + """\" NOHREF onClick=\"PoseClick()\">
</map>

<img src=\"""" + strActionName + """.bmp\" usemap=\"#SynopticMap\" />
</body>
</html>
        """
        
            strHTML = textwrap.dedent(strHTML)
            f.write(strHTML)        
        
            return True
        
        finally:
            f.close()
            
    except IOError, (errno, strerror):
        log("I/O error: " + strerror)
        return False
        
