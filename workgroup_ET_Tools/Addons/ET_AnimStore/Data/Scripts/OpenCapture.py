"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

ET_Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ET_Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ET_Tools.  If not, see <http://www.gnu.org/licenses/>.
"""

from win32com.client import constants as c
from win32com.client import Dispatch as d

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection
oSceneRoot = xsi.ActiveProject2.ActiveScene.Root

myView = xsi.Desktop.ActiveLayout.CreateViewFromDefinitionFile("C:\\Users\\speakeasyfx\\Autodesk\\Softimage_2011_SP1\\Addons\\ET_AnimStore_v02\\Application\\views\\ET_AnimStore_PoseCam.xsivw","Pose Cam")
myView.BeginEdit()
myView.Move(120,80)
myView.Resize(350, 600)

myView.EndEdit()
