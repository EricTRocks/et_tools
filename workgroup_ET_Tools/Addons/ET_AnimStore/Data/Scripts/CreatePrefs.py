"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

ET_Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ET_Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ET_Tools.  If not, see <http://www.gnu.org/licenses/>.
"""

from win32com.client import constants as c
from win32com.client import Dispatch as d

xsi = Application
log = xsi.LogMessage
oSceneRoot = xsi.ActiveProject2.ActiveScene.Root
collSel = xsi.Selection

oPrefs = xsi.Preferences

collPrefCats = oPrefs.Categories

try:
	oLang = oPrefs.GetPreferenceValue("ET_AnimStore.export_dir")
	log(oLang)
except:
	strUserPath = xsi.InstallationPath(c.siUserPath)
	oProp = oSceneRoot.AddCustomProperty("ET_AnimStore")
	oProp.AddParameter2("export_dir",c.siString,strUserPath,None,None,None,None,c.siClassifUnknown,4,"Export Directory")

	xsi.InstallCustomPreferences(oProp,"ET_AnimStore")