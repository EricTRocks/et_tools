"""
Copyright (c) 2009-2013, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

You should have received a copy of the GNU General Public License
along with ET_Tools.  If not, see <http://www.gnu.org/licenses/>.
"""

from win32com.client import constants as c
from win32com.client import Dispatch as d
import sqlite3 as sq
import os,re,ConfigParser
import itertools as it
from xml.etree import ElementTree as ET

xsi = d("XSI.Application").Application
log = xsi.LogMessage
collSel = xsi.Selection
oActiveProject = xsi.ActiveProject2

XSIUIToolkit = d("XSI.UIToolkit")
XSIUtils = d("XSI.Utils")

versionMajor = 2
versionMinor = 25

DEBUG = 0

# Alan Fregtman's property panel function
def popWindow(prop, moveX, moveY, resizeX, resizeY,strPanelTitle="Property Panel"):

	myView = xsi.Desktop.ActiveLayout.CreateView("Property Panel",strPanelTitle)
	myView.BeginEdit()
	myView.Move(moveX, moveY)
	myView.Resize(resizeX, resizeY)
	myView.SetAttributeValue("targetcontent", prop.FullName)
	myView.EndEdit()
	
	return True

	
def updateConfig(strLastDir):

	strUserPath = xsi.GetInstallationPath2(c.siUserPath)
	strConfigPath = XSIUtils.BuildPath(strUserPath,"Data","Preferences","ET_AnimStore.xml")
			
	oFile = open(strConfigPath,"w")
	oFile.write(strLastDir)
	oFile.close()	

	
def getConfigValue():
	strUserPath = xsi.GetInstallationPath2(c.siUserPath)
	strConfigPath = XSIUtils.BuildPath(strUserPath,"Data","Preferences","ET_AnimStore.xml")
	
	if os.path.isfile(strConfigPath):
		oFile = open(strConfigPath,"r")
		strLastDir = oFile.readline()
		oFile.close()	
		return strLastDir
	else:
		return ""

		
def cleanStr(string):
	strClean = re.sub(r'[^\w]','',string)
	return strClean
	

# DB Queries
def getProjects(strDBPath):
	if not os.path.isfile(strDBPath):
		#log("",2)
		return
		
	try:
		conn = sq.connect(strDBPath)
		cursor = conn.cursor()
		data = cursor.execute("""SELECT project_name,rowid FROM projects ORDER BY project_name""")
		lData = [x for x in data]
		conn.close()
		
		lProjects = list(it.chain.from_iterable(lData))
				
		if not lProjects:
			lProjects = ["None",0]
		
		return lProjects
	
	except sq.Error, e:
		log("Error ocurred: " + str(e.args[0]) + " in " + strDBPath,2)
		
		
def getGroups(strDBPath,project_id):
	if not os.path.isfile(strDBPath):
		#log(strDBPath + " doesn't exists!",2)
		return
		
	try:
		conn = sq.connect(strDBPath)
		cursor = conn.cursor()
		data = cursor.execute("""SELECT groups.group_name,groups.rowid FROM groups INNER JOIN projects ON groups.project_id=projects.rowid WHERE projects.rowid='""" + str(project_id) + """' ORDER BY group_name""")
		lData = [x for x in data]
		conn.close()
		
		lGroups = list(it.chain.from_iterable(lData))
		
		if not lGroups:
			lGroups = ["None",0]
		
		return lGroups
	
	except sq.Error, e:
		log("Error ocurred: " + str(e.args[0]) + " in " + strDBPath,2)
		

def getSets(strDBPath,strActionType,group_id):
	if not os.path.isfile(strDBPath):
		#log(strDBPath + " doesn't exists!",2)
		return
		
	try:
		conn = sq.connect(strDBPath)
		cursor = conn.cursor()
		data = cursor.execute("""SELECT sets.set_name,sets.rowid FROM sets INNER JOIN groups ON sets.group_id=groups.rowid WHERE groups.rowid='""" + group_id + """' AND sets.action_type='""" + strActionType + """' ORDER BY set_name""")
		lData = [x for x in data]
		conn.close()
		
		lSets = list(it.chain.from_iterable(lData))
			
		if not lSets:
			lSets = ["None",0]
			
		return lSets
	
	except sq.Error, e:
		log("Error ocurred: " + str(e.args[0]) + " in " + strDBPath,2)

def getActionInfo(strProjectID,strGroupID,strSetID,strDBPath):

	conn = sq.connect(strDBPath)
	cursor = conn.cursor()
	
	dActionInfo = {}
	
	if strProjectID:
		dataProject = cursor.execute("""SELECT project_name FROM projects where rowid='""" + strProjectID + """' LIMIT 1""")
		lProjects = [x for x in dataProject]
				
		if len(lProjects):
			strProject = [x for x in lProjects][0][0]
			dActionInfo["Project"] = strProject
		else:
			dActionInfo["Project"] = ""
	
	if strGroupID:
		dataGroup = cursor.execute("""SELECT group_name FROM groups where rowid='""" + strGroupID + """' LIMIT 1""")
		lGroups = [x for x in dataGroup]
		
		if len(lGroups):
			strGroup = [x for x in lGroups][0][0]
			dActionInfo["Group"] = strGroup
		else:
			dActionInfo["Group"] = ""
	
	if strSetID:
		dataSet = cursor.execute("""SELECT set_name FROM sets where rowid='""" + strSetID + """' LIMIT 1""")
		lSets = [x for x in dataSet]
				
		if len(lSets):
			strSet = [x for x in lSets][0][0]
			dActionInfo["Set"] = strSet
		else:
			dActionInfo["Set"] = ""
	
	conn.close()

	return dActionInfo
	
def checkStringInput(strInput,strTarget,intLen=14):

	bType = type(strTarget) is str
	
	if not bType:
		log("'strTarget' input on checkStringInput is not a string!",2)
		return False

	lErrors = []
	if not len(strInput): lErrors.append(strTarget + ": This string cannot be empty!")
	if len(strInput) > intLen: lErrors.append(strTarget + ": This string is too long. Limit: " + str(intLen))
	if strInput[:1].isdigit(): lErrors.append(strTarget + ": This string cannot start with a digit!")
	if not strInput.isalnum(): lErrors.append(strTarget + ": This string cannot contain symbols or spaces!")

	if len(lErrors) > 0:
	
		log("Invalid value for " + strTarget + ". See log for details.",2)
		log("\n" + "\n".join([x for x in lErrors]))
		XSIUIToolkit.MsgBox(strTarget + ": Invalid String! See log for details.")
	
		return False
	
	return True
	
	
def getPoseList(strProjectID,strGroupID,strActionType,strSetID,strDBPath):
	
	dActionInfo = getActionInfo(strProjectID,strGroupID,strSetID,strDBPath)
	
	conn = sq.connect(strDBPath)
	cursor = conn.cursor()
	
	actionQuery = cursor.execute("SELECT actions.action_name,actions.action_type,actions.set_id FROM actions INNER JOIN sets ON actions.set_id=sets.rowid WHERE actions.action_type= ? AND actions.set_id= ? ORDER BY actions.action_name",[strActionType,strSetID])
	lFoundActions = [x[0] for x in actionQuery]
	
	actionQuery2 = conn.execute("SELECT actions.action_name,actions.action_type,actions.set_id,actions.rowid FROM actions INNER JOIN sets ON actions.set_id=sets.rowid WHERE actions.action_type= ? AND actions.set_id= ? ORDER BY actions.action_name",[strActionType,strSetID])
	lFoundActions2 = [x for x in actionQuery2]
	
	conn.commit()
	conn.close()
		
	"""
	dActions = {}
	for each in sorted(lFoundActions):
		strPath = "Projects\\" + "\\".join([dActionInfo["Project"],dActionInfo["Group"],strActionType,dActionInfo["Set"]])
		dActions[each] = [each,strPath + "\\" + each]
	"""
	
	dActions = {}
	for each in sorted(lFoundActions2):
		strPath = "Projects\\" + "\\".join([dActionInfo["Project"],dActionInfo["Group"],strActionType,dActionInfo["Set"]])
		dActions[each] = [each[0],strPath + "\\" + each[0],each[3]]
	
	return dActions