"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


from win32com.client import constants as c
from win32com.client.dynamic import Dispatch as d
import os
import shutil
import getpass
import sys
import traceback
import sqlite3
import datetime

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

DEBUG = 0


# ========
# Plug-in
# ========
def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric"
    in_reg.Name = "ET_ProjectTools"
    in_reg.Major = 1
    in_reg.Minor = 0

    in_reg.RegisterProperty("ET_ProjectTools_ProjMngrGUI")
    in_reg.RegisterCommand("ET_ProjectTools_OpenProjMngr", "ET_ProjectTools_OpenProjMngr")
    in_reg.RegisterMenu(c.siMenuMainFileProjectID, "ET_ProjectTools_OpenProjMngr_Menu", False, False)
    #RegistrationInsertionPoint - do not remove this line

    return True


def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    Application.LogMessage(str(strPluginName) + str(" has been unloaded."), c.siVerbose)
    return True


# ===========
# Exceptions
# ===========
def printException( exc, exc_traceback ):
    log("Unexpected ET_ProjectTools Error Occured! ================================", 2)
    log("Message: " + traceback.format_exception_only(type(exc), exc)[0], 2)
    log("Line: " + str(exc_traceback.tb_lineno), 2)
    log("Full Strack Trace:", 2)
    log(traceback.format_tb( exc_traceback ), 2)
    log("Softimage Version: " + xsi.Version(), 2)
    log("Unexpected ET_ProjectTools Error Occured! ================================", 2)


# ========
# Classes
# ========
class ETProjectManager(object):
    """Manages projects contained in projects.sqlite databases."""

    def __init__(self):

        self.prefPath = os.path.join(XSIUtils.Environment.Item("XSI_USERHOME"), "Data", "default.xsiprojects")
        self.projects = self.getProjects()
        self.user = getpass.getuser()


    def addProject(self, projectName, projectPath):
        """Adds a project to the database."""

        assert type(projectName) in [str, unicode], "projectName argument is not a string."
        assert type(projectPath) in [str, unicode], "projectPath argument is not a string."

        try:
            if not self.isValidProject(projectPath):
                raise Exception("projectPath argument is an invalid Softimage project path.")
                return False

            with open(self.prefPath, "a") as prefFile:
                prefFile.write(projectPath + ", " + projectName)

            self.projects = self.getProjects()
            
            print "Added project: %s" % projectName

        except Exception, e:
            printException(e, sys.exc_info()[2])
            return None


    def removeProject(self, projectName):
        """Removes a project from the project manager."""

        assert type(projectName) in [str, unicode], "projectName argument is not an integer."

        try:
            self.projects = self.getProjects()

            if projectName in self.projects.keys():
                del self.projects[projectName]

            with open(self.prefPath, "w") as prefFile:
                projectsData = """//\n//.xsiprojects resource file.\n//\n"""
                
                for key, value in self.projects.iteritems():
                    projectsData += value + ", " + key + "\n"
                    
                prefFile.write(projectsData)

            self.projects = self.getProjects()

            print "Removed project: %s" % projectName

        except Exception, e:
            printException(e, sys.exc_info()[2])
            return None


    def getProjects(self):
        """Returns a dictionary of projects within the database."""

        try:
            with open(self.prefPath, "r") as prefFile:
                prefData = prefFile.readlines()
            
            projects = {}
            for eachLine in prefData[3:]:
                projectInfo = eachLine.split(",")
                projects[projectInfo[1].strip()] = projectInfo[0]

            return projects

        except Exception, e:
            printException(e, sys.exc_info()[2])
            return None


    def getProjPath(self, projectName):
        """Gets the path of the specified project."""

        try:
            if projectName not in self.projects.keys():
                log(projectName + " was not found in project list.", 4)
                return None

            return self.projects[projectName]

        except Exception, e:
            printException(e, sys.exc_info()[2])
            return None


    def getFiles(self, projectName):
        """Gets a list of files in the specified project."""

        assert type(projectName) in [str, unicode] and projectName, "projectName argument is not a valid string."
        if projectName not in self.projects.keys():
            log(projectName + " was not found in project list.", 4)
            return False

        files = []
        projectPath = os.path.join(self.projects[projectName], "Scenes")
        for eachFile in [x for x in os.listdir(os.path.join(projectPath)) if x.endswith(".scn")]:
            files.append(eachFile[:-4])
            files.append(eachFile[:-4])

        if len(files) < 1:
            files = ""

        return files


    def isValidProject(self, projectPath):
        """Returns true if the strPath argument returns a valid Softimage Project."""

        assert type(projectPath) in [str, unicode], "projectPath argument is not a string."

        if os.path.exists(projectPath) and os.path.exists(os.path.join(projectPath, "system", "dsprojectinfo")):
            return True
        else:
            return False


# ======
# Menus
# ======
def ET_ProjectTools_OpenProjMngr_Menu_Init( in_ctxt ):
    oMenu = in_ctxt.Source
    oMenu.AddCommandItem("ET Project Manager", "ET_ProjectTools_OpenProjMngr")
    return True


# =========
# Commands
# =========
def ET_ProjectTools_OpenProjMngr_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = "Opens ET Project Manager"
    oCmd.Tooltip = "Opens ET Project Manager"
    oCmd.SetFlag(c.siCannotBeUsedInBatch, True)
    oCmd.ReturnValue = True

    return True

def ET_ProjectTools_OpenProjMngr_Execute(  ):

    try:
        scnRoot = xsi.ActiveProject2.ActiveScene.Root

        if scnRoot.LocalProperties("ET_ProjMngrGUI"):
            projectMngrProp = scnRoot.LocalProperties("ET_ProjMngrGUI")
        else:
            projectMngrProp = scnRoot.AddProperty("ET_ProjectTools_ProjMngrGUI", False, "ET_ProjMngrGUI")
            
        projMngrView = xsi.Desktop.ActiveLayout.CreateView("Property Panel", "ET_ProjectManager")
        projMngrView.SetAttributeValue("targetcontent", projectMngrProp.FullName)
        projMngrView.BeginEdit()
        projMngrView.Resize(600, 275)
        projMngrView.EndEdit()

        return True

    except Exception, e:
        printException(e, sys.exc_info()[2])

# ===========
# Properties
# ===========
def ET_ProjectTools_ProjMngrGUI_Define( in_ctxt ):
    customProperty = in_ctxt.Source
    customProperty.AddParameter2("projects", c.siString, "", None, None, None, None, c.siClassifUnknown, c.siPersistable)
    customProperty.AddParameter2("files", c.siString, "", None, None, None, None, c.siClassifUnknown, c.siPersistable)
    return True


def buildUI():
    projMngrProperty = PPG.Inspected(0)
    layout = PPG.PPGLayout
    layout.Clear() 
    
    projectMngr = ETProjectManager()

    projectList = []
    projects = projectMngr.projects
    for eachItem in projects.keys():
        projectList.extend([eachItem for x in range(2)])
    
    fileList = ""
    
    if projMngrProperty.Parameters("projects").Value != "":
        fileList = sorted(projectMngr.getFiles(projMngrProperty.Parameters("projects").Value))

    layout.AddRow()

    item = layout.AddGroup("", False)
    
    layout.AddRow()
    layout.AddGroup("", True)
    item = layout.AddButton("NewProject", "New Project")
    item.SetAttribute(c.siUICX, 120)
    item = layout.AddButton("SetActiveProject", "Set As Active")
    item.SetAttribute(c.siUICX, 120)
    item = layout.AddButton("AddProject", "Add Project")
    item.SetAttribute(c.siUICX, 120)
    item = layout.AddButton("RemoveProject", "Remove Project")
    item.SetAttribute(c.siUICX, 120)
    item = layout.AddButton("DeleteProject", "Delete Project")
    item.SetAttribute(c.siUICX, 120)
    layout.EndGroup()

    layout.AddGroup("Projects", True, 60)
    itemProjectList = layout.AddEnumControl("projects", sorted(projectList), "Projects", c.siControlListBox)
    itemProjectList.SetAttribute(c.siUINoLabel, True)
    itemProjectList.SetAttribute(c.siUICY, 200)
    layout.EndGroup()
    layout.EndRow()

    layout.EndGroup()
    

    item = layout.AddGroup("", False)
    layout.AddRow()
    layout.AddGroup("Scenes", True, 60)
    itemFileList = layout.AddEnumControl("files", fileList, "Files", c.siControlListBox)
    itemFileList.SetAttribute(c.siUINoLabel, True)
    itemFileList.SetAttribute(c.siUICY, 200)
    layout.EndGroup()

    layout.AddGroup("", True)
    item = layout.AddButton("NewFile", "New Scene")
    item.SetAttribute(c.siUICX, 120)
    item = layout.AddButton("OpenFile", "Open Scene")
    item.SetAttribute(c.siUICX, 120)
    item = layout.AddButton("DeleteFile", "Delete Scene")
    item.SetAttribute(c.siUICX, 120)
    layout.EndGroup()
    layout.EndRow()

    layout.EndGroup()

    layout.EndRow()

    PPG.Refresh()

    return True


def ET_ProjectTools_ProjMngrGUI_DefineLayout( in_ctxt ):    
    return True


def ET_ProjectTools_ProjMngrGUI_OnInit( ):
    buildUI()

    return


def ET_ProjectTools_ProjMngrGUI_OnClosed( ):
    return


def ET_ProjectTools_ProjMngrGUI_projects_OnChanged():
    buildUI()

    projectMngrProp = PPG.Inspected(0)
    projectMngrProp.Parameters("files").Value = ""

    return


# ==================
# Project Callbacks
# ==================
def ET_ProjectTools_ProjMngrGUI_NewProject_OnClicked():
    try:
        scnRoot = xsi.ActiveProject2.ActiveScene.Root
        newProjectProperty = scnRoot.AddProperty("CustomProperty", False, "New Project")
        newProjectProperty.AddParameter3("location", c.siString, "", "", "", False, False)
        newProjectProperty.AddParameter3("projectName", c.siString, "New_Project", "", "", False, False)

        newProjectLayout = newProjectProperty.PPGLayout
        newProjectLayout.AddString("projectName", False)
        newProjectLayout.AddItem("location", "Location", c.siControlFolder)

        modal = xsi.InspectObj(newProjectProperty, "", "New Project", c.siModal, False)

        if modal:
            log("User Cancelled.", 4)
            return False

        newProjectPath = None
        projectPath = newProjectProperty.Parameters("location").Value
        projectName = newProjectProperty.Parameters("projectName").Value

        if not os.path.exists(projectPath):
            log("Project Path does not exist!", 4)
            return False

        newProjectPath = os.path.join(projectPath, projectName)
        if os.path.exists(newProjectPath):
            log("Project already exists!", 4)
            return False

        xsi.CreateProject(newProjectPath)

        projectManager = ETProjectManager()

        if projectManager.isValidProject(newProjectPath):
            projectManager.addProject(projectName, newProjectPath)

        buildUI()

        return True

    except Exception, e:
        printException(e, sys.exc_info()[2])

        if newProjectPath and os.path.exists(newProjectPath):
            shutil.rmtree(newProjectPath)

        return False

    finally:
        xsi.DeleteObj(newProjectProperty)


def ET_ProjectTools_ProjMngrGUI_AddProject_OnClicked():
    try:
        scnRoot = xsi.ActiveProject2.ActiveScene.Root
        addProjectProperty = scnRoot.AddProperty("CustomProperty", False, "Add Project")
        addProjectProperty.AddParameter3("strLocation", c.siString, "", "", "", False, False)

        addProjectLayout = addProjectProperty.PPGLayout
        addProjectLayout.AddItem("strLocation", "Location", c.siControlFolder)

        modal = xsi.InspectObj(addProjectProperty, "", "Add Project", c.siModal, False)

        if modal:
            log("User Cancelled.", 4)
            return False

        projectPath = addProjectProperty.Parameters("strLocation").Value

        if not os.path.exists(projectPath):
            log("Project Path does not exist!", 4)
            return False

        projectManager = ETProjectManager()
        if projectPath in projectManager.projects.keys():
            log("Project already in database.", 4)
            return False

        if projectManager.isValidProject(projectPath):
            projectManager.addProject(projectPath.rsplit("\\", 1)[1], projectPath)

        buildUI()

        return True

    except Exception, e:
        printException(e, sys.exc_info()[2])

        return False

    finally:
        xsi.DeleteObj(addProjectProperty)


def ET_ProjectTools_ProjMngrGUI_SetActiveProject_OnClicked():
    try:
        projectManagerProperty = PPG.Inspected(0)
        projectManager = ETProjectManager()

        projectPath = projectManager.getProjPath(projectManagerProperty.Parameters("projects").Value)
        xsi.ActiveProject = projectPath

    except Exception, e:
        printException(e, sys.exc_info()[2])

        return False


def ET_ProjectTools_ProjMngrGUI_RemoveProject_OnClicked():
    try:
        projectManagerProperty = PPG.Inspected(0)
        projectManager = ETProjectManager()

        projectManager.removeProject(projectManagerProperty.Parameters("projects").Value)
        projectManagerProperty.Parameters("projects").Value = ""
        buildUI()

        return True

    except Exception, e:
        printException(e, sys.exc_info()[2])

        return False

def ET_ProjectTools_ProjMngrGUI_DeleteProject_OnClicked():
    try:
        projectManagerProperty = PPG.Inspected(0)
        projectManager = ETProjectManager()

        messageBox = XSIUIToolkit.MsgBox("Delete project: %s?" % projectManagerProperty.Parameters("projects").Value, c.siMsgYesNo)

        if messageBox == 6:
            shutil.rmtree(projectManager.projects[projectManagerProperty.Parameters("projects").Value])
            projectManager.removeProject(projectManagerProperty.Parameters("projects").Value)
            projectManagerProperty.Parameters("projects").Value = ""
            buildUI()
        else:
            log("User cancelled.", 4)

        return True

    except Exception, e:
        printException(e, sys.exc_info()[2])

        return False


# ===============
# File Callbacks
# ===============
def ET_ProjectTools_ProjMngrGUI_NewFile_OnClicked():
    projectManagerProperty = PPG.Inspected(0)
    projectManager = ETProjectManager()
    projectPath = projectManager.getProjPath(projectManagerProperty.Parameters("projects").Value)

    projectMngrView = xsi.Desktop.ActiveLayout.Views("ET_ProjectManager")
    projectMngrView.State = c.siClosed

    xsi.NewScene(projectPath, True)


def ET_ProjectTools_ProjMngrGUI_OpenFile_OnClicked():
    projectManagerProperty = PPG.Inspected(0)
    projectManager = ETProjectManager()
    projectPath = projectManager.getProjPath(projectManagerProperty.Parameters("projects").Value)
    fileName = projectManagerProperty.Parameters("files").Value

    filePath = os.path.join(projectPath, "Scenes", fileName + ".scn")
    
    projectMngrView = xsi.Desktop.ActiveLayout.Views("ET_ProjectManager")
    projectMngrView.State = c.siClosed

    xsi.OpenScene(filePath, True)
    xsi.ET_ProjectTools_OpenProjMngr()


def ET_ProjectTools_ProjMngrGUI_DeleteFile_OnClicked():
    try:
        projectManagerProperty = PPG.Inspected(0)
        projectManager = ETProjectManager()
        projectPath = projectManager.getProjPath(projectManagerProperty.Parameters("projects").Value)
        fileName = projectManagerProperty.Parameters("files").Value

        filePath = os.path.join(projectPath, "Scenes", fileName + ".scn")

        messageBox = XSIUIToolkit.MsgBox("Delete the %s?" % fileName, c.siMsgYesNo)

        if messageBox == 6:
            os.remove(filePath)
            buildUI()
        else:
            log("User cancelled.", 4)

        return True

    except Exception, e:
        printException(e, sys.exc_info()[2])
        return False

    finally:
        projectManagerProperty.Parameters("files").Value = ""