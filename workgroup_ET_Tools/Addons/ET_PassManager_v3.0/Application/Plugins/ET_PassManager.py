"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


from win32com.client import constants as c
from win32com.client import Dispatch as d
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import tostring
import os
import base64
import sys
import traceback

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

DEBUG = 0


# ===============================
# Print ET_PassManager Exception
# ===============================
def printException( exc, exc_traceback ):
	xsi.LogMessage("Unexpected ET_PassManager Error Occured! ================================", 2)
	xsi.LogMessage("Message: " + traceback.format_exception_only(type(exc), exc)[0], 2)
	xsi.LogMessage("Line: " + str(exc_traceback.tb_lineno), 2)
	xsi.LogMessage("Full Strack Trace:", 2)
	xsi.LogMessage(traceback.format_tb( exc_traceback ), 2)
	xsi.LogMessage("Softimage Version: " + xsi.Version(), 2)


def XSILoadPlugin( in_reg ):
	in_reg.Author = "Eric Thivierge"
	in_reg.Name = "ET_PassManager_v3.0"
	in_reg.Major = 3
	in_reg.Minor = 21

	in_reg.RegisterProperty("ET_PassManager_GUI")
	in_reg.RegisterMenu(c.siMenuTbRenderPassEditID, "ET_PassManager", True, True)
	in_reg.RegisterCommand("ET_PassManager_Import", "ET_PassManager_Import")
	in_reg.RegisterCommand("ET_PassManager_Export", "ET_PassManager_Export")
	#RegistrationInsertionPoint - do not remove this line

	return True


# =================
# PassManager Menu
# =================
def ET_PassManager_Init(in_ctxt):
	oMenu = in_ctxt.Source
	oMenu.AddCallbackItem("Create GUI", "ET_PassManager_CreateGUI")
	
	oMenu.AddCallbackItem("Import *.sipass", "ET_PassManager_ImportAll")
	oMenu.AddCallbackItem("Export *.sipass", "ET_PassManager_ExportAll")
	
	oMenu.AddSeparatorItem()
	
	oMenu.AddCommandItem("Import *.asslight", "ET_AssLight_Import")
	oMenu.AddCommandItem("Export *.asslight", "ET_AssLight_Export")

	oMenu.AddSeparatorItem()

	oMenu.AddCommandItem("New Global Partition", "ET_NewGlobalPartition")
	oMenu.AddCommandItem("Copy Obj Partitions", "ET_CopyObjPartitions")

	
def ET_PassManager_CreateGUI(in_ctxt):
	oScnRoot = xsi.ActiveProject2.ActiveScene.Root
	
	oGUI = oScnRoot.Properties("ET_PassManager_GUI")
	if oGUI == None:
		oGUI = oScnRoot.AddProperty("ET_PassManager_GUI")
		
	xsi.InspectObj(oGUI)


def ET_PassManager_ImportAll(in_ctxt):

	oFB = XSIUIToolkit.FileBrowser
	oFB.DialogTitle = "Select sipass file"
	oFB.InitialDirectory = xsi.ActiveProject2.Path + "\\Render_Pictures"
	oFB.Filter = "SI Pass Files (*.sipass)|*.sipass||"
	oFB.ShowOpen()

	if oFB.FileName == "":
		return False
		
	xsi.ET_PassManager_Import(oFB.FilePathName)


def ET_PassManager_ExportAll(in_ctxt):
	oFB = XSIUIToolkit.FileBrowser
	oFB.InitialDirectory = xsi.ActiveProject2.Path + "\\Render_Pictures"
	oFB.Filter = "SI Pass Files (*.sipass)|*.sipass||"
	oFB.ShowSave()
	
	if oFB.FileName == "":
		log("Output filename not specified. Command aborted.", 2)
		return False
	
	strPassList = ";".join(sorted([x.Name for x in xsi.ActiveProject2.ActiveScene.Passes]))
	xsi.ET_PassManager_Export(strPassList, True, True, True, True, True, True, True, True, False, oFB.FilePathName)

	
# ================
# PassManager GUI
# ================
def ET_PassManager_GUI_Define( in_ctxt ):
	oProp = in_ctxt.Source
	oProp.AddParameter2("Logo", c.siString, "", None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("PassList", c.siString, "", None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludeScnRndrOpts", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludeRendererSettings", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludePassOptions", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludeLocalRendererOpts", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludePartitions", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludePartitionMembers", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludeOverrides", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("IncludeMaterials", c.siBool, True, None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("LogDebug", c.siBool, False ,None ,None ,None ,None , c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("ExportPath", c.siString,"", None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("ImportPath", c.siString,"", None, None, None, None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("SkipExistingPasses", c.siBool, False ,None ,None ,None , None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("UseExistingMats", c.siBool, False ,None ,None ,None , None, c.siClassifUnknown, c.siPersistable)
	oProp.AddParameter2("LogDebugImport", c.siBool, False ,None ,None ,None , None, c.siClassifUnknown, c.siPersistable)
	
	return True


def ET_PassManager_GUI_DefineLayout( in_ctxt ):
	return True


def ET_PassManager_GUI_BuildLayout():
	
	strPluginPath = xsi.Plugins("ET_PassManager_v3.0").OriginPath
	strLogoPath = XSIUtils.BuildPath(strPluginPath, "..", "..", "Help", "images", "ET_PassManger3.0_Logo.bmp")

	oLayout = PPG.PPGLayout
	oLayout.Clear()
	
	oLayout.AddTab("Export")

	oLogo = oLayout.AddItem("Logo", "", c.siControlBitmap)
	oLogo.SetAttribute(c.siUIFilePath, strLogoPath)
	oLogo.SetAttribute(c.siUINoLabel, True)
	
	oLayout.AddGroup("Pass Settings")
	oLayout.AddSpacer(10,10)
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	lPasses = sorted([x.Name for x in xsi.ActiveProject2.ActiveScene.Passes]*2)
	oItem = oLayout.AddEnumControl("PassList", lPasses, "Pass List", c.siControlListBox)
	oItem.SetAttribute(c.siUIMultiSelectionListBox, True)
	oItem.SetAttribute(c.siUILabelMinPixels, 65)
	oItem.SetAttribute(c.siUILabelPercentage, 25)
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	
	oLayout.AddSpacer(10,10)
	
	oLayout.AddRow()
	oLayout.AddSpacer(75,10)
	oItem = oLayout.AddButton("RefreshPassList", "Refresh Pass List")
	oItem.SetAttribute("CX",215)
	oItem.SetAttribute("CY",35)
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddSpacer(10,10)
	oLayout.EndGroup()
	
	oLayout.AddGroup("Renderers")
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludeScnRndrOpts", "Scene Render Options")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludeRendererSettings", "Renderer Settings")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.EndGroup()
	
	oLayout.AddGroup("Passes")	
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludePassOptions", "Pass Options")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludeLocalRendererOpts", "Local Renderer Options")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.EndGroup()
	
	oLayout.AddGroup("Partitions")
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludePartitions", "Partitions")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludePartitionMembers", "Partition Members")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludeOverrides", "Overrides")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("IncludeMaterials", "Materials")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	
	oLayout.EndGroup()
	
	oLayout.AddGroup("Misc")
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("LogDebug", "Log Debug Info")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.EndGroup()
	
	oLayout.AddGroup("Export Settings")
	oLayout.AddSpacer(10,10)
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oItem = oLayout.AddItem("ExportPath", "Export *.sipass", c.siControlFilePath)
	oItem.SetAttribute(c.siUILabelMinPixels, 100)
	oItem.SetAttribute(c.siUIOpenFile, False)
	oItem.SetAttribute(c.siUIFileMustExist, False)
	oItem.SetAttribute(c.siUIInitialDir, xsi.ActiveProject2.Path + "\\Render_Pictures")
	oItem.SetAttribute(c.siUIFileFilter, "*.sipass files (*.sipass)|*.sipass|")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oItem = oLayout.AddButton("Export")
	oItem.SetAttribute("CX",280)
	oItem.SetAttribute("CY",35)
	oLayout.EndRow()
	oLayout.AddSpacer(10,10)
	oLayout.EndGroup()
	
	oLayout.AddTab("Import")
	
	oLogo = oLayout.AddItem("Logo", "", c.siControlBitmap)
	oLogo.SetAttribute(c.siUIFilePath, strLogoPath)
	oLogo.SetAttribute(c.siUINoLabel, True)
	
	oLayout.AddGroup("Import Settings")
	oLayout.AddSpacer(10,10)
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oItem = oLayout.AddItem("ImportPath", "Import *.sipass", c.siControlFilePath)
	oItem.SetAttribute(c.siUIOpenFile, True)
	oItem.SetAttribute(c.siUIFileMustExist, True)
	oItem.SetAttribute(c.siUILabelMinPixels, 100)
	oItem.SetAttribute(c.siUIInitialDir, xsi.ActiveProject2.Path + "\\Render_Pictures")
	oItem.SetAttribute(c.siUIFileFilter, "*.sipass files (*.sipass)|*.sipass|")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddSpacer(10,10)
	
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("SkipExistingPasses","Skip Existing Passes")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddSpacer(10,10)
	
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("UseExistingMats","Use existing materials")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	
	oLayout.AddSpacer(10,10)
	
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddItem("LogDebugImport","Log Debug Info")
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddSpacer(10,10)
	oLayout.AddRow()
	oLayout.AddSpacer(10,10)
	oItem = oLayout.AddButton("Import")
	oItem.SetAttribute("CX",280)
	oItem.SetAttribute("CY",35)
	oLayout.AddSpacer(10,10)
	oLayout.EndRow()
	oLayout.AddSpacer(10,10)
	oLayout.EndGroup()

	
def ET_PassManager_GUI_OnInit( ):
	ET_PassManager_GUI_BuildLayout()
	PPG.Refresh()

	
def ET_PassManager_GUI_OnClosed( ):
	Application.LogMessage("ET_PassManager_GUI_OnClosed called", c.siVerbose)

	
def ET_PassManager_GUI_PassList_OnChanged( ):
	strPassList = PPG.PassList.Value
	log(strPassList)

	
def ET_PassManager_GUI_RefreshPassList_OnClicked( ):
	ET_PassManager_GUI_BuildLayout()
	PPG.Refresh()

	
def ET_PassManager_GUI_Export_OnClicked( ):
	ET_PassManager_GUI_BuildLayout()
	PPG.Refresh()
	
	# Check that selected passes are still valid and strip out invalid passes.
	collPasses = xsi.ActiveProject.ActiveScene.Passes
	lPasses = [str(x.Name) for x in collPasses]
	lValidPasses = [str(x) for x in PPG.PassList.Value.split(";") if x in lPasses]
	PPG.PassList.Value = ";".join(sorted(lValidPasses))
	
	strPassList = str(PPG.PassList.Value)
	bIncludeScnRndrOpts = PPG.IncludeScnRndrOpts.Value
	bIncludeRendererSettings = PPG.IncludeRendererSettings.Value
	bIncludePassOptions = PPG.IncludePassOptions.Value
	bIncludeLocalRendererOpts = PPG.IncludeLocalRendererOpts.Value
	bIncludePartitions = PPG.IncludePartitions.Value
	bIncludePartitionMembers = PPG.IncludePartitionMembers.Value
	bIncludeOverrides = PPG.IncludeOverrides.Value
	bIncludeMaterials = PPG.IncludeMaterials.Value
	bLogDebug = PPG.LogDebug.Value
	strExportPath = str(PPG.ExportPath.Value)
	
	xsi.ET_PassManager_Export(strPassList, bIncludeScnRndrOpts, bIncludeRendererSettings, bIncludePassOptions, bIncludeLocalRendererOpts, bIncludePartitions, bIncludePartitionMembers, bIncludeOverrides, bIncludeMaterials, bLogDebug, strExportPath)


def ET_PassManager_GUI_IncludePartitions_OnChanged():
	if PPG.IncludePartitions.Value == 0:
		PPG.IncludePartitionMembers.Value = 0
		PPG.IncludeOverrides.Value = 0
		PPG.IncludeMaterials.Value = 0
	else:
		PPG.IncludePartitionMembers.Value = 1
		PPG.IncludeOverrides.Value = 1
		PPG.IncludeMaterials.Value = 1


def ET_PassManager_GUI_IncludePartitionMembers_OnChanged():
	if PPG.IncludePartitions.Value == 0:
		PPG.IncludePartitionMembers.Value = 0
		log("Partition Objects can only be enabled if exporting Partitions.", 4)


def ET_PassManager_GUI_IncludeOverrides_OnChanged():
	if PPG.IncludePartitions.Value == 0:
		PPG.IncludeOverrides.Value = 0
		log("Partition overrides can only be enabled if exporting Partitions.", 4)


def ET_PassManager_GUI_IncludeMaterials_OnChanged():
	if PPG.IncludePartitions.Value == 0:
		PPG.IncludeMaterials.Value = 0
		log("Partition materials can only be enabled if exporting Partitions.", 4)


def ET_PassManager_GUI_Import_OnClicked():
	strImportPath = str(PPG.ImportPath.Value)
	bSkipExistingPasses = PPG.SkipExistingPasses.Value
	bUseExistingMat = PPG.UseExistingMats.Value
	bLogDebugImport = PPG.LogDebugImport.Value
	xsi.ET_PassManager_Import(strImportPath, bSkipExistingPasses, bUseExistingMat, bLogDebugImport)


# ===========================
# PassManager Import Command
# ===========================
def ET_PassManager_Import_Init(in_ctxt):
	oCmd = in_ctxt.Source
	oCmd.Arguments.Add("ImportPath", c.siArgumentInput, "", c.siString)
	oCmd.Arguments.Add("SkipExistingPasses", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("UseExistingMats", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("LogDebugImport", c.siArgumentInput, False, c.siBool)
	
	return True


def ET_PassManager_Import_Execute(*args):
	
	strImportPath = args[0]
	bSkipExistingPasses = args[1]
	bUseExistingMats = args[2]
	bLogDebugImport = args[3]
	
	try:
		bMsgLog = xsi.Preferences.GetPreferenceValue("scripting.msglog")
		xsi.Preferences.SetPreferenceValue("scripting.cmdlog", False)
		xsi.Preferences.SetPreferenceValue("scripting.msglog", False)
		xsi.BeginUndo()
		
		# Parse the XML file, change path
		tree = ET.parse(strImportPath)
		oDocRoot = tree.getroot()

		# Scene level objects
		oActiveScene = xsi.ActiveProject2.ActiveScene
		oScnRoot = oActiveScene.Root
		collMatLibs = oActiveScene.MaterialLibraries
		collPasses = oActiveScene.Passes
		oPassCon = oActiveScene.PassContainer

		oInitPass = oActiveScene.ActivePass

		# ==== Import Material Library ==== #
		oMatLib = None
		lMatLibs = oDocRoot.getiterator("matlib")
		if len(lMatLibs) > 0:
		
			strMatLibPath = XSIUtils.BuildPath(XSIUtils.Environment("TEMP"),os.path.basename(strImportPath).split(".")[0] + "_MatLib.xsi")
			fMatLib = open(strMatLibPath,"w")
			
			strMatLibEncoded = oDocRoot.getiterator("matlib")[0].get("value")
			decodeFile = base64.b64decode(strMatLibEncoded)
			fMatLib.write(decodeFile)
			fMatLib.close()
			
			oMatLib = xsi.SIImportMatLib(strMatLibPath)("Value")
					
			xsi.Preferences.SetPreferenceValue("scripting.msglog", True)
			if bLogDebugImport:
				log("Imported MatLib: " + oMatLib.FullName)
		
		xsi.Preferences.SetPreferenceValue("scripting.msglog", True)
		

		# ==== Import Scene Render Options ==== #
		lScnRndrOpts = oDocRoot.getiterator("scnrndropts")
		if len(lScnRndrOpts) > 0:
			oRndrOpts = xsi.Dictionary.GetObject("Passes.RenderOptions")
			collRndrParams = oRndrOpts.Parameters
			
			if bLogDebugImport:
				log("")
				log("Importing Scene Render Parameters")
			
			lRndrOpts = lScnRndrOpts[0].getiterator("option")
			for eachOption in lRndrOpts:
				strOptionName = eachOption.get("name")
				strOptionValue = eachOption.get("value")
				
				if bLogDebugImport:
					log("\t" + strOptionName + ": " + strOptionValue)
				
				if strOptionName == "ResolvedOutputDir":
					continue
				
				collRndrParams(strOptionName).Value = strOptionValue
			
			if bLogDebugImport:
				log("")
				log("Importing Custom Channels")
			
			lChannels = lScnRndrOpts[0].getiterator("channel")
			for eachChan in lChannels:
				strChanName = eachChan.get("name")
				strChanType = eachChan.get("type")
				
				if bLogDebugImport:
					log("\t" + strChanName + ": " + strChanType)
				
				collChannels = oRndrOpts.RenderChannels
				if collChannels(strChanName) == None:
					oRndrOpts.CreateRenderChannel(strChanName,strChanType)
		

		# ==== Import Renderer Settings ==== #
		lRendererSettings = oDocRoot.getiterator("renderer")
		if len(lRendererSettings) > 0:
			
			for eachProp in oPassCon.Properties:
				if eachProp.Type != "SceneRenderProperty": # Don't include the Scene Render Options, already saved out
					
					if bLogDebugImport:
						log("")
						log("Importing Renderer Settings")
						
					for eachRenderer in lRendererSettings:
						strRendererType = eachRenderer.get("type")
						strRendererValue = eachRenderer.get("value")
						
						if bLogDebugImport:
							log("\t" + strRendererType)
												
						if eachProp.Type == strRendererType:
							
							strRendererPath = XSIUtils.BuildPath(XSIUtils.Environment("TEMP"),os.path.basename(strImportPath).split(".")[0] + "_" + strRendererType + "_Renderer.preset")
							fRendererPreset = open(strRendererPath,"wb")
													
							decodeFile = base64.b64decode(strRendererValue)
							fRendererPreset.write(decodeFile)
							fRendererPreset.close()
				
							xsi.LoadPreset(strRendererPath,eachProp)
		

		# ==== Import Image Clips ==== #
		lImageClips = oDocRoot.getiterator("imageclip")
		if lImageClips:
			for eachClip in lImageClips:
				strImageClipName = eachClip.get("name")
				strImageClipSrc = eachClip.get("src")

				collImgClips = xsi.ActiveProject2.ActiveScene.ImageClips
				if collImgClips(strImageClipName):
					continue

				oImgSource = xsi.SIAddImageSource(strImageClipSrc, strImageClipName)
				oImgClip = xsi.SIAddImageClip(oImgSource("Source"), oImgSource("Source").Name)
		

		# ==== Import Passes ==== #
		lPasses = oDocRoot.getiterator("pass")
		
		# Pass Progress Bar
		oProgBar = XSIUIToolkit.ProgressBar
		oProgBar.Maximum = 100.00
		oProgBar.Step = 100.00 / len(lPasses)
		oProgBar.Caption = "Importing Passes"
		oProgBar.CancelEnabled = True
		oProgBar.Visible = True
		

		if bLogDebugImport:
			log("")
			log("Importing Passes")
		
		for eachPass in lPasses:
			strPassName = eachPass.get("name")
			
			if bLogDebugImport:
				log("")
				log("\tPass: " + strPassName)
				log("\t===========================================")
			
			if collPasses(strPassName) != None and bSkipExistingPasses:
				continue
			elif collPasses(strPassName) != None and not bSkipExistingPasses:
				oPass = collPasses(strPassName)
				xsi.SetCurrentPass(oPass)
			else:
				oPass = oPassCon.AddPass("Pass", strPassName)
			
			oProgBar.StatusText = "Pass: " + strPassName

			if bLogDebugImport:
				log("")
				log("\t\tImporting Pass Parameters")
			
			# ==== Import Pass Parameters ==== #
			lPassParams = eachPass.getiterator("passparam")
			for eachParam in lPassParams:
				strParamName = eachParam.get("name")
				strParamValue = eachParam.get("value")
				
				if bLogDebugImport:
					log("\t\t\t" + strParamName + ": " + strParamValue)
				
				if strParamName == "Camera":
					oCam = xsi.Dictionary.GetObject(strParamValue,False)
					if oCam == None:
						log("\t\tCamera not found. Skipped!", 4)
						continue

				oParam = oPass.Parameters(strParamName).Value = strParamValue				
			

			if bLogDebugImport:
				log("")
				log("\t\tImporting Frame Buffers")
			
			# ==== Import Frame Buffers ==== #
			lFrameBufs = eachPass.getiterator("framebuffer")
			for eachFrameBuf in lFrameBufs:
				strBufName = eachFrameBuf.get("name")
				strBufEnabled = eachFrameBuf.get("enabled")
				strBufFileName = eachFrameBuf.get("filename")
				strBufFormat = eachFrameBuf.get("format")
				strBufDataType = eachFrameBuf.get("datatype")
				strBufBitDepth = eachFrameBuf.get("bitdepth")
				
				if bLogDebugImport:
					log("\t\t\t" + strBufName + ", " + strBufEnabled + ", " + strBufFileName + ", " + strBufFormat + ", " + strBufDataType + ", " + strBufBitDepth)
				
				if strBufName != "Main":
					oFrameBuffer = oPass.CreateFrameBuffer(strBufName)
					oScnBuf = xsi.Dictionary.GetObject("Passes." + str(oPass.Name) + "." + strBufName, 0)
					
					if oScnBuf:
						oScnBuf.Parameters("Enabled").Value = strBufEnabled
						oScnBuf.Parameters("FileName").Value = strBufFileName
						oScnBuf.Parameters("Format").Value = strBufFormat
						oScnBuf.Parameters("DataType").Value = strBufDataType
						oScnBuf.Parameters("BitDepth").Value = strBufBitDepth
					else:
						log("Framebuffer: " + str(strBufName) + " was not found. Skipped.")
				
				else:
					oMainBuf = oPass.FrameBuffers("Main")
					oMainBuf.Parameters("Enabled").Value = strBufEnabled
					oMainBuf.Parameters("FileName").Value = strBufFileName
					oMainBuf.Parameters("Format").Value = strBufFormat
					oMainBuf.Parameters("DataType").Value = strBufDataType
					oMainBuf.Parameters("BitDepth").Value = strBufBitDepth				
			

			if bLogDebugImport:
				log("")
				log("\t\tImporting Local Renderer Settings")
			
			# ==== Import Local Renderer Settings ==== #
			lPassRendererSettings = eachPass.getiterator("passrenderer")
			for eachRenderer in lPassRendererSettings:
				strRendererType = eachRenderer.get("type")
				strRendererValue = eachRenderer.get("value")
				
				if bLogDebugImport:
					log("\t\t\t" + strRendererType)
				
				lRendererProps = [x for x in oPass.Properties] # if x.Type in ["mentalray","Arnold_Render_Options","HardwareRenderer"]]
				for eachRendererProp in lRendererProps:
					
					if strRendererType == eachRendererProp.Type:
						oLocalProp = xsi.MakeLocal(eachRendererProp, c.siDefaultPropagation)
						
						strRendererPath = XSIUtils.BuildPath(XSIUtils.Environment("TEMP"), os.path.basename(strImportPath).split(".")[0] + "_" + strRendererType + "_LocalRenderer.preset")
						fRendererPreset = open(strRendererPath, "wb")
						
						decodeFile = base64.b64decode(strRendererValue)
						fRendererPreset.write(decodeFile)
						fRendererPreset.close()
			
						xsi.LoadPreset(strRendererPath, oLocalProp)


			if bLogDebugImport:
				log("")
				log("\t\t\tImporting Pass Shaders")

			# ==== Import Pass Shaders ==== #
			lShaderFiles = eachPass.getiterator("shaderfile")

			for eachShaderFile in lShaderFiles:
				strShaderFileValue = eachShaderFile.get("value")

				strShaderFilePath = XSIUtils.BuildPath(XSIUtils.Environment("TEMP"),os.path.basename(strImportPath).split(".")[0] + "_" + strPassName + ".xsishaders")
				fShaderFile = open(strShaderFilePath, "wb")
				
				decodeFile = base64.b64decode(strShaderFileValue)
				fShaderFile.write(decodeFile)
				fShaderFile.close()

				collShaders = xsi.ImportShaderTree(oPass.FullName, strShaderFilePath)
				

				# ==== Get Pass Shader Connections ==== #
				lShaderCnx = eachPass.getiterator("shadercnx")
				for eachCnx in lShaderCnx:
					strShaderName = eachCnx.get("name")
					strShaderTgt = eachCnx.get("tgt")

					lShaders = [x for x in collShaders if x.Name.startswith(strShaderName)]
					if lShaders:
						oShader = lShaders[0]
						oOutParam = oShader.Parameters("out")

						oShaderTgt = xsi.Dictionary.GetObject(oPass.FullName + "." + strShaderTgt,False)
						if not oShaderTgt:
							lShaderTgtSplit = strShaderTgt.split(".")
							if len(lShaderTgtSplit) < 2:
								oShaderTgt = xsi.SIAddArrayElement( oPass.FullName + ".EnvironmentShaderStack")
							else:
								oShaderTgt = xsi.SIAddArrayElement( oPass.FullName + "." + lShaderTgtSplit[0])

						xsi.SIConnectShaderToCnxPoint(oOutParam.FullName, oShaderTgt.FullName)


			if bLogDebugImport:
				log("")
				log("\t\tImporting Partitions")
			
			# ==== Import Partitions ==== #
			lPartitions = eachPass.getiterator("partition")
			[xsi.DeleteObj(x) for x in oPass.Partitions if x.Name not in [i.get("name") for i in lPartitions]]

			for eachPartition in lPartitions:
				strPartName = eachPartition.get("name")
				strPartType = eachPartition.get("type")
				strPartViewVis = eachPartition.get("viewvis")
				strPartRendVis = eachPartition.get("rendvis")
				
				if bLogDebugImport:
					log("\t\t\tName: " + strPartName)
					log("\t\t\tType: " + strPartType)
					log("\t\t\tView Visibility: " + strPartViewVis)
					log("\t\t\tRender Visibility: " + strPartRendVis)
				
				if strPartName == "Background_Objects_Partition" or strPartName == "Background_Lights_Partition":
					oPartition = xsi.Dictionary.GetObject("Passes." + strPassName + "." + strPartName, False)
				else:
					oPartition = oPass.Partitions(strPartName)
					if not oPartition:
						oPartition = oPass.CreatePartition(strPartName, strPartType)

				oPartition.Parameters("viewvis").Value = strPartViewVis
				oPartition.Parameters("rendvis").Value = strPartRendVis
				
				if not oPartition.Background:
					oPartition.RemoveAllMembers()

				# ==== Apply materials to partitions ==== #
				lMats = eachPartition.getiterator("material")
				for eachMat in lMats:
					strMatName = eachMat.get("name")
					strMatLib = eachMat.get("library")
					
					if bLogDebugImport:
						log("\t\t\t\tPartition Material: " + strMatName + " from: " + strMatLib)
					
					if bUseExistingMats:
						log("\t\t\t\tUSING EXISTING MATS!")

						oLib = collMatLibs(strMatLib)
						if oLib != None:
							oMat = oLib.Items(strMatName)
						else:
							oMat = None
					else:
						log("\t\t\t\tNOT USING EXISTING MATS!")
						oMat = xsi.Dictionary.GetObject(str(oMatLib.FullName) + "." + strMatName, False)
						log(oMat)
					
					if oMat != None:
						oPartition.SetMaterial(oMat)
					else:
						if bLogDebugImport:
							log("\t\t\t\tMaterial not found, skipped!")
				
				# ==== Import members of partitions ==== #
				lMembers = eachPartition.getiterator("member")
				for eachMember in lMembers:
					strMemberName = eachMember.get("name")
					oMember = xsi.Dictionary.GetObject(strMemberName,False)
					
					if bLogDebugImport:
						log("\t\t\t\tMember: " + strMemberName)
					
					if oMember and oMember.Type != "null":
						oPartition.AddMember(oMember)
				
				# ==== Import overrides ==== #
				lOverrides = eachPartition.getiterator("override")
				for eachOverride in lOverrides:
					strOverrideName = eachOverride.get("name")

					oOverride = oPartition.GetLocalPropertyFromName(strOverrideName)
					if not oOverride:
						oOverride = oPartition.AddProperty("Override", 0, strOverrideName)
					
					if bLogDebugImport:
						log("\t\t\t\tOverride: " + strOverrideName)
					
					lEntries = eachOverride.getiterator("entry")
					for eachEntry in lEntries:
						strEntryName = eachEntry.text
						strEntryValue = eachEntry.get("value")
						strEntrySource = eachEntry.get("source")
						strEntryParameter = eachEntry.get("parameter")
						collPartMembers = oPartition.Members
						
						if bLogDebugImport:
							log("\t\t\t\t\tEntry Name: " + strEntryName)
							log("\t\t\t\t\tEntry Value: " + strEntryValue)
							log("\t\t\t\t\tEntry Source: " + strEntrySource)
							log("\t\t\t\t\tEntry Parameter: " + strEntryParameter)
						
						if collPartMembers.Count > 0:

							oEntry = d(oOverride).ParameterEntries(strEntryName)
							if oEntry:
								continue

							oFoundParam = xsi.Dictionary.GetObject(strEntryParameter,False)
							
							log(strEntryName)
							if oFoundParam and oOverride:
								xsi.SIAddEntryToOverride(oOverride,oFoundParam,2)
								oEntry = d(oOverride).ParameterEntries(strEntryName)
								oEntry.Value = strEntryValue
							
			oProgBar.Increment()
		
		if oMatLib and oMatLib.Items.Count < 1:
			xsi.SetCurrentMaterialLibrary(collMatLibs(0))
			xsi.DeleteObj(oMatLib)

		oProgBar.Visible = False
		XSIUIToolkit.MsgBox("Import Successful!",c.siMsgOkOnly,"ET PassManager Import")
		
	except Exception, e:
		printException(e, sys.exc_info()[2])
		
		return False
		
	finally:
		xsi.SetCurrentPass(oInitPass)
		xsi.Preferences.SetPreferenceValue("scripting.msglog", bMsgLog)
		xsi.EndUndo()


# ===========================
# PassManager Export Command
# ===========================
def ET_PassManager_Export_Init(in_ctxt):
	oCmd = in_ctxt.Source
	
	oCmd.Arguments.Add("PassList", c.siArgumentInput, "", c.siString)
	oCmd.Arguments.Add("IncludeScnRndrOpts", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("IncludeRendererSettings", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("IncludePassOptions", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("IncludeLocalRendererOpts", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("IncludePartitions", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("IncludePartitionMembers", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("IncludeOverrides", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("IncludeMaterials", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("LogDebug", c.siArgumentInput, False, c.siBool)
	oCmd.Arguments.Add("ExportPath", c.siArgumentInput, "", c.siString)
	
	return True


def ET_PassManager_Export_Execute(*args):

	strPassList = args[0]
	bIncludeScnRndrOpts = args[1]
	bIncludeRendererSettings = args[2]
	bIncludePassOptions = args[3]
	bIncludeLocalRendererOpts = args[4]
	bIncludePartitions = args[5]
	bIncludePartitionMembers = args[6]
	bIncludeOverrides = args[7]
	bIncludeMaterials = args[8]
	bLogDebug = args[9]
	strExportPath = args[10]

	oScene = xsi.ActiveProject.ActiveScene
	
	try:
		bMsgLog = xsi.Preferences.GetPreferenceValue("scripting.msglog")
		xsi.Preferences.SetPreferenceValue("scripting.cmdlog",False)
		xsi.Preferences.SetPreferenceValue("scripting.msglog", True)
		xsi.BeginUndo()
		
		# Change active viewport to explorer and maximize, improves performance when switching passes
		oLayout = xsi.Desktop.ActiveLayout
		oVM = oLayout.Views.Filter("View Manager")(0)
		strActiveViewport = oVM.GetAttributeValue("focusedviewport")
		strActiveViewportType = oVM.GetAttributeValue("viewport:" + strActiveViewport)
		oVM.SetAttributeValue("viewport:" + strActiveViewport,"Explorer")
		oVM.SetAttributeValue("layout","maximize:" + strActiveViewport)
		
		assert strExportPath != "", "Export Path is invalid!"
		
		oInitPass = xsi.GetCurrentPass()
		
		wRoot = ET.Element("root")
		
		# ==== Scene Render Options ==== #
		if bIncludeScnRndrOpts:
		
			oProgBar = XSIUIToolkit.ProgressBar
			oProgBar.Maximum = 100
			oProgBar.Step = 50
			oProgBar.Caption = "Exporting Scene Render Options"
			oProgBar.CancelEnabled = True
			oProgBar.Visible = True
			
			while oProgBar.Value < 100:
				wScnRndrOpts = ET.SubElement(wRoot, "scnrndropts")
				oRndrOpts = xsi.Dictionary.GetObject("Passes.RenderOptions")
				collRndrParams = oRndrOpts.Parameters
				
				for eachParam in collRndrParams:
					strParamName = eachParam.Name
					strParamValue = str(eachParam.Value)
					if strParamName not in ["Type","User"]:
						wOption = ET.SubElement(wScnRndrOpts, "option")
						wOption.set("name", strParamName)
						wOption.set("value", strParamValue)
				
				if oProgBar.CancelPressed:
					raise Exception("Cancelled ET_PassManager Export")
				
				oProgBar.Increment()
				
				collRndrChannels = oRndrOpts.RenderChannels
				lDefaultChannels = ["Main","Depth","Motion","Normal","Object Labels","Pixel Coverage","Pixel Time","Raster Motion","Ambient","Diffuse","Specular","Irradiance","Reflection","Refraction"]
				lCustomChannels = [x for x in collRndrChannels if x.Name not in lDefaultChannels]
				
				for eachChannel in lCustomChannels:
					strChannelName = eachChannel.Name
					strChannelType = str(eachChannel.ChannelType)
					wOption = ET.SubElement(wScnRndrOpts, "channel")
					wOption.set("name", strChannelName)
					wOption.set("type", strChannelType)
				
				if oProgBar.CancelPressed:
					raise Exception("Cancelled ET_PassManager Export")
					
				oProgBar.Increment()
				
			oProgBar.Visible = True
		
		# ==== Include Renderer Settings ==== #
		if bIncludeRendererSettings:
			oPassCon = xsi.ActiveProject2.ActiveScene.PassContainer
			for eachProp in oPassCon.Properties:
				if eachProp.Type != "SceneRenderProperty": # Don't include the Scene Render Options, already saved out
					wRenderer = ET.SubElement(wRoot, "renderer")
					wRenderer.set("type",eachProp.Type)
					
					if bLogDebug == True:
						log("Renderer Settings: " + eachProp.FullName)
						
					strRendererPreset = XSIUtils.BuildPath(XSIUtils.Environment("TEMP"),eachProp.Type + ".Preset")
					xsi.SavePreset(eachProp.FullName,strRendererPreset)
					openFile = open(strRendererPreset,"rb")
					readFile = openFile.read()
					encodeFile = base64.b64encode(readFile)
					wRenderer.set("value", encodeFile)
		
		lPassList = str(strPassList).split(";")
		collPasses = xsi.ActiveProject2.ActiveScene.Passes
		
		if lPassList[0] == '':
			raise Exception("No Passes selected")
		
		# ==== Image Clips ==== #
		wImageClips = ET.SubElement(wRoot, "imageclips")

		collClips = xsi.ActiveProject2.ActiveScene.ImageClips
		for eachClip in collClips:
			if eachClip.Name == "noIcon_pic":
				continue

			wImageClip = ET.SubElement(wImageClips, "imageclip")
			wImageClip.set("name",eachClip.Name)
			wImageClip.set("src",eachClip.GetFileName())

		# ==== Include Materials ==== #
		if bIncludeMaterials:
			oMatLib = xsi.CreateLibrary(os.path.basename(strExportPath).split(".")[0] + "_MatLib", 1, "")("Value")
			lMatLibList = []
		
		# Pass Progress Bar
		oProgBar = XSIUIToolkit.ProgressBar
		oProgBar.Maximum = 100.00
		oProgBar.Step = 100.00 / collPasses.Count
		oProgBar.Caption = "Exporting Passes"
		oProgBar.CancelEnabled = True
		oProgBar.Visible = True
		
		# ==== Passes ==== #
		for eachPass in collPasses:
		
			if eachPass.Name not in lPassList:
				continue
			
			strPassName = eachPass.Name
			xsi.SetCurrentPass("Passes." + strPassName)
			oProgBar.Caption = "Exporting: " + strPassName
						
			if bLogDebug == True:
				log("Pass: " + strPassName)
				
			# ==== Write Pass to XML ==== #
			wPass = ET.SubElement(wRoot, "pass")
			wPass.set("name",strPassName)
			
			if bLogDebug == True:
				log("pass shaders") # =================================== <

			# ==== Pass Shaders ==== #
			wPassShaders = ET.SubElement(wPass, "shaders")

			collShaders = eachPass.GetAllShaders()
			collClips = eachPass.AllImageClips

			if collShaders.Count > 1:

				if collClips.Count > 1:
					strPassShaders = xsi.ExportShaderTree(",".join([x.FullName for x in collShaders]) + "," + ",".join([x.FullName for x in collClips]), os.path.basename(strExportPath).split(".")[0] + "_" + eachPass.Name + ".xsishaders", "", "", True, "")
				else:
					strPassShaders = xsi.ExportShaderTree(",".join([x.FullName for x in collShaders]) + "," + "", os.path.basename(strExportPath).split(".")[0] + "_" + eachPass.Name + ".xsishaders", "", "", True, "")

				openFile = open(strPassShaders,"rb")
				readFile = openFile.read()
				encodeFile = base64.b64encode(readFile)
				wShaders = ET.SubElement(wPassShaders, "shaderfile")
				wShaders.set("value", encodeFile)

				if bLogDebug == True:
					log("shader connections") # =================================== <

				# ==== Shader Connections ==== #
				for eachShader in eachPass.GetAllShaders():
					collCnxTgt = eachShader.GetShaderParameterTargets("Out")
					for eachCnxTgt in collCnxTgt:
						if collCnxTgt.Count and "Item" in eachCnxTgt.FullName:
							strShaderOutCnx = ".".join(eachCnxTgt.FullName.split(".")[2:])

							wPassShader = ET.SubElement(wPassShaders, "shadercnx")
							wPassShader.set("name",eachShader.Name)
							wPassShader.set("tgt",strShaderOutCnx)

			if bLogDebug == True:
				log("pass options") # =================================== <

			if bIncludePassOptions:
				lFactoryChans = set(["Main","Depth","Motion","Normal","ObjectLabels","Pixel Coverage","Raster Motion","Diffuse","Specular","Irradiance","Reflection","Refraction","Ambient"])
				collFrameBuffers = eachPass.FrameBuffers
				for eachBuf in [x for x in collFrameBuffers]: # if x.Name in lFactoryChans
					strBufName = eachBuf.Name
					strFileNameValue = str(eachBuf.Parameters("Filename").Value)
					strEnabledValue = str(eachBuf.Parameters("Enabled").Value)
					strFormatValue = str(eachBuf.Parameters("Format").Value)
					strDataTypeValue = str(eachBuf.Parameters("DataType").Value)
					strBitDepthValue = str(eachBuf.Parameters("BitDepth").Value)
					
					if bLogDebug == True:
						log("\tFramebuffer: " + strBufName)
						log("\t\tFileName: " + strFileNameValue)
						log("\t\tEnabled: " + strEnabledValue)
						log("\t\tFormat: " + strFormatValue)
						log("\t\tData Type: " + strDataTypeValue)
						log("\t\tBit Depth: " + strBitDepthValue)
					
					# ==== Write Frame Buffer to XML ==== #
					wFrameBuf = ET.SubElement(wPass, "framebuffer")
					wFrameBuf.set("name", strBufName)
					wFrameBuf.set("filename", strFileNameValue)
					wFrameBuf.set("enabled", strEnabledValue)
					wFrameBuf.set("format", strFormatValue)
					wFrameBuf.set("datatype", strDataTypeValue)
					wFrameBuf.set("bitdepth", strBitDepthValue)
				
				lPassParams = ["FrameRangeSource","FrameSkipRendered","FrameRangeSource","FrameStart","FrameEnd","FrameStep","FrameSet","Camera","ImageFormatOverride","ImageFormatPreset","ImageWidth","ImageHeight","ImagePixelRatio","ImageLockAspectRatio","ImageAspectRatio","MotionBlur","MotionBlurOverride","MotionBlurShutterSpeed","MotionBlurShutterType","MotionBlurShutterOffset","MotionBlurDeformation","FieldOverride","FieldEnable","FieldInterleave","FieldOrder","CropWindowEnabled","SelectionTracking","CropWindowWidth","CropWindowHeight","CropWindowOffsetX","CropWindowOffsetY","UseDisplayGammaCorrection","SelectionOnly"]
				for eachParam in lPassParams:
					# ==== Write pass parameters to XML ==== #
					oParam = eachPass.Parameters(eachParam)
					wPassParam = ET.SubElement(wPass, "passparam")
					wPassParam.set("name",oParam.Name)
					wPassParam.set("value",str(oParam.Value))

				# Pass Shaders
				collShaders = eachPass.GetAllShaders()
				os.path.basename(strExportPath).split(".")[0] + "_MatLib"
			
			if not bIncludeLocalRendererOpts:
				continue
				
			lLocalRendererProps = [x for x in eachPass.LocalProperties if x.Type in ["mentalray","Arnold_Render_Options","HardwareRenderer"]]
			for eachRendererProp in lLocalRendererProps:
				
				# ==== Write Pass Renderer to XML ==== #
				wPassRenderer = ET.SubElement(wPass, "passrenderer")
				wPassRenderer.set("type",eachRendererProp.Type)
				
				if bLogDebug == True:
					log("\tPass Renderer: " + eachRendererProp.FullName)
					
				strRendererPreset = XSIUtils.BuildPath(XSIUtils.Environment("TEMP"),eachRendererProp.Type + ".Preset")
				xsi.SavePreset(eachRendererProp.FullName,strRendererPreset)
				openFile = open(strRendererPreset,"rb")
				readFile = openFile.read()
				encodeFile = base64.b64encode(readFile)
				wPassRenderer.set("value", encodeFile)
			
			if not bIncludePartitions:
				continue
			
			# ==== Partitions ==== #
			for eachPartition in eachPass.Partitions:
				strPartitionName = eachPartition.Name
				strPatitionType = str(eachPartition.PartitionType)
				strPartitionViewVis = str(eachPartition.Parameters("viewvis").Value)
				strPartitionRendVis = str(eachPartition.Parameters("rendvis").Value)
				
				if bLogDebug == True:
					log("\tPartition: " + strPartitionName)
					log("\t\tType: " + strPatitionType)
					log("\t\tView Visibility: " + strPartitionViewVis)
					log("\t\tRender Visibility: " + strPartitionRendVis)
				
				# Write Partition to XML
				wPart = ET.SubElement(wPass, "partition")
				wPart.set("name", strPartitionName)
				wPart.set("type", strPatitionType)
				wPart.set("viewvis", strPartitionViewVis)
				wPart.set("rendvis", strPartitionRendVis)
				
				# ==== Members ==== #
				if bIncludePartitionMembers:
					for eachMember in eachPartition.Members:
						strMemberName = eachMember.FullName
						
						if bLogDebug == True:
							log("\t\tPartition Member: " + strMemberName)
						
						# Write Objects to XML
						wMem = ET.SubElement(wPart, "member")
						wMem.set("name", strMemberName)
				
				# ==== Overrides ==== #
				if bIncludeOverrides and eachPartition.Members.Count > 0:
					collOverrides = eachPartition.LocalProperties.Filter(c.siOverrideType)
					for eachOverride in [x for x in collOverrides if not x.Name.startswith("VisibilityOverride")]:
						strOverrideName = eachOverride.Name
						
						if bLogDebug == True:
							log("\t\tOverride: " + eachOverride.Name)
							
						# Write Override to XML
						wOver = ET.SubElement(wPart, "override")
						wOver.set("name", strOverrideName)
						
						# ==== Override Entries ==== #
						for eachEntry in eachOverride.ParameterEntries:
							strEntryValue = str(eachEntry.Value)
							strEntryName = eachEntry.ScriptName
							strEntrySource = str(eachEntry.Source)
							strOverriddenObject = eachEntry.OverridenObject.FullName
							
							if bLogDebug == True:
								log("\t\t\tEntry: " + strEntryName)
								log("\t\t\tEntry Value: " + strEntryValue)
								log("\t\t\tEntry Source: " + strEntrySource)
								log("\t\t\tOverridden Object: " + strOverriddenObject)
								
							# Write Entry to XML
							wEntry = ET.SubElement(wOver, "entry")
							wEntry.set("parameter", strOverriddenObject)
							wEntry.set("value", strEntryValue)
							wEntry.set("source", strEntrySource)
							wEntry.text = strEntryName
				
				# ==== Materials ==== #
				if bIncludeMaterials:
					oMat = eachPartition.LocalProperties.Filter(c.siMaterialType)
					if oMat(0) != None:
						if bLogDebug == True:
							log("\t\tMaterial: " + oMat(0).Name)
							log("\t\tMaterial: " + oMat(0).FullName)
						
						if len(lMatLibList) > 0:
							for oItem in lMatLibList:
								if bLogDebug == True:
									log("\t\t\tCurrent Material in loop: " + oItem,8)
									log("\t\t\tPartMat: " + oMat(0).Name,8)
								
								if oMat(0).Name != oItem:
									xsi.CopyPaste(oMat(0).FullName, "", oMatLib, 2)
						else:
							lMatLibList.append(oMat(0).Name)
							oCopiedMat = xsi.CopyPaste(oMat(0).FullName, "", oMatLib, 2)
						
						oGetMat = xsi.Dictionary.GetObject(oMat(0).FullName,False) # Have to get object since oMat(0) didn't return correctly
						
						# Write Material to XML
						wMat = ET.SubElement(wPart, "material")
						wMat.set("library", oGetMat.Library.Name)
						wMat.set("name", oMat(0).Name)
			
			if oProgBar.CancelPressed:
				raise Exception("Canceled during pass export.")
			
			oProgBar.Increment()
			
		oProgBar.Visible = False
		
		log("""
=============================
Softimage MatLib Export Info
=============================""")
		
		# Export MatLib and encode into .sipass file
		if bIncludeMaterials:
			strMatLibPath = XSIUtils.BuildPath(XSIUtils.Environment("TEMP"),os.path.basename(strExportPath).split(".")[0] + "_MatLib.xsi")
			xsi.ExportMaterialLibrary(strMatLibPath, oMatLib)
			fMatLibFile = open(strMatLibPath,"r")
			readFile = fMatLibFile.read()
			encodeFile = base64.b64encode(readFile)
			fMatLibFile.close()
			wMatLib = ET.SubElement(wRoot,"matlib")
			wMatLib.set("value", encodeFile)
			
			# Delete Matlib
			collMatLibs = oScene.MaterialLibraries
			xsi.SetCurrentMaterialLibrary(collMatLibs(0))
			xsi.DeleteObj(oMatLib)
		
		log("""
=============================""")
		
		siPass = ET.ElementTree(wRoot)
		siPass.write(strExportPath)		
		cleanWriteXML(strExportPath)
		xsi.SetCurrentPass(oInitPass)
		
		XSIUIToolkit.MsgBox("Export Successful!",c.siMsgOkOnly,"ET PassManager")
		
	except Exception, e:
		printException(e, sys.exc_info()[2])
		
		return False
		
	finally:
		# Reset viewport
		oVM.SetAttributeValue("viewport:" + strActiveViewport,strActiveViewportType)
		oVM.SetAttributeValue("layout","default")
		
		xsi.Preferences.SetPreferenceValue("scripting.msglog", bMsgLog)
		xsi.EndUndo()

		
# ================
# Helper Functions
# ================
def getLights():
	oScnRoot = xsi.ActiveProject2.ActiveScene.Root
	collLights = oScnRoot.FindChildren("","light")

	dLights = {}
	for each in collLights:
		oGrp = each.ActivePrimitive.NestedObjects("Associated Models")
		collMembers = oGrp.NestedObjects("Members").NestedObjects
		
		dLights[each.FullName] = [x.FullName for x in collMembers]
		
	return dLights


def indent(elem, level=0):
	i = "\n" + level*"  "
	if len(elem):
		if not elem.text or not elem.text.strip():
			elem.text = i + "  "
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
		for elem in elem:
			indent(elem, level+1)
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
	else:
		if level and (not elem.tail or not elem.tail.strip()):
			elem.tail = i

			
def cleanxml(xml):
	elem = ET.fromstring(xml)
	indent(elem)
	strIndented = ET.tostring(elem)
	return strIndented

	
def cleanWriteXML(strFilePath):
	fOpen = open(strFilePath,"r")
	fRead = fOpen.read()
 
	fWrite = open(strFilePath,"w")
	strPrettyXML = cleanxml(fRead)
	fWrite.write(strPrettyXML)
	fWrite.close()
 
