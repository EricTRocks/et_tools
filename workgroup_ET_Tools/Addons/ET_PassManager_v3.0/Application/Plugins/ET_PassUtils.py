"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


from win32com.client import constants as c
from win32com.client import Dispatch as d
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import tostring
import os
import base64
import sys
import traceback

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

DEBUG = 0


# ===============================
# Print ET_PassManager Exception
# ===============================
def printException( exc, exc_traceback ):
    xsi.LogMessage( "Unexpected ET_PassManager Error Occured! ================================" )
    xsi.LogMessage( "Message: " + traceback.format_exception_only(type(exc),exc)[0]  )
    xsi.LogMessage( "Line: " + str(exc_traceback.tb_lineno) )
    xsi.LogMessage( "Full Strack Trace:" )
    xsi.LogMessage( traceback.format_tb( exc_traceback ) )
    xsi.LogMessage( "Softimage Version: " + xsi.Version() )


def XSILoadPlugin( in_reg ):
    in_reg.Author = "Eric Thivierge"
    in_reg.Name = "ET_PassUtils"
    in_reg.Major = 3
    in_reg.Minor = 21

    in_reg.RegisterCommand("ET_AssLight_Import","ET_AssLight_Import")
    in_reg.RegisterCommand("ET_AssLight_Export","ET_AssLight_Export")
    in_reg.RegisterCommand("ET_NewGlobalPartition","ET_NewGlobalPartition")
    in_reg.RegisterCommand("ET_CopyObjPartitions","ET_ObjCopyPartitions")

    #RegistrationInsertionPoint - do not remove this line

    return True


def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    Application.LogMessage(str(strPluginName) + str(" has been unloaded."),c.siVerbose)
    
    return True


def ET_AssLight_Import_Init(in_ctxt):
    oCmd = in_ctxt.Source
    oCmd.Description = "Imports .asslight files."
    oCmd.Tooltip = "Imports .asslight files."
    oCmd.SetFlag(c.siSupportsKeyAssignment,False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch,True)
    oCmd.ReturnValue = False
    
    return True


def ET_AssLight_Import_Execute():
    oFB = XSIUIToolkit.FileBrowser
    oFB.DialogTitle = "Select .asslight file"
    oFB.InitialDirectory = xsi.ActiveProject2.Path + "\\Render_Pictures"
    oFB.Filter = "AssLight Files (*.asslight)|*.asslight||"
    oFB.ShowOpen()

    if oFB.FileName == "":
        log("File not specified. Command aborted.", 2)
        return False

    try:
        tree = ET.parse(oFB.FilePathName)
        oDocRoot = tree.getroot()
        lLights = oDocRoot.getiterator('lights')[0].getiterator('light')
        
        for each in lLights:
            strLightName = each.get('name')
            oLight = xsi.Dictionary.GetObject(strLightName, False)
            
            if not oLight:
                log('Could not find ' + strLightName,16)
                continue
            
            lMemberItems = each.getiterator('member')
            
            if len(lMemberItems) < 1:
                continue
                
            strMemberList = ""
            for eachItem in lMemberItems:
                strMemberName = eachItem.get('name')
                oMember = xsi.Dictionary.GetObject(strMemberName, False)
                
                if not oMember:
                    log("Couldnot find: " + strMemberName,16)
                    continue
                    
                strMemberList += strMemberName + ","
            
            collMembers = d("XSI.Collection")
            collMembers.SetAsText(strMemberList)
                    
            oGrp = xsi.Dictionary.GetObject(strLightName + ".Associated Models")
            oGrp.RemoveAllMembers()
            oGrp.AddMember(collMembers)
    
        XSIUIToolkit.MsgBox("Import Successful!",c.siMsgOkOnly,"ET AssLight Import")
    
    except Exception, e:
        printException(e, sys.exc_info()[2])
        
        return False



def ET_AssLight_Export_Init(in_ctxt):
    oCmd = in_ctxt.Source
    oCmd.Description = "Exports .asslight files."
    oCmd.Tooltip = "Exports .asslight files."
    oCmd.SetFlag(c.siSupportsKeyAssignment,False)
    oCmd.SetFlag(c.siCannotBeUsedInBatch,True)
    oCmd.ReturnValue = False

    return True


def ET_AssLight_Export_Execute():
    oFB = XSIUIToolkit.FileBrowser
    oFB.InitialDirectory = xsi.ActiveProject2.Path + "\\Render_Pictures"
    oFB.Filter = "AssLight Files (*.asslight)|*.asslight||"
    oFB.ShowSave()
    
    if oFB.FileName == "":
        log("Output filename not specified. Command aborted.", 2)
        return False

    try:
        wRoot = ET.Element("root")
        wLights = ET.SubElement(wRoot, "lights")

        dLights = getLights()
        for eachLight in dLights.keys():
            wLight = ET.SubElement(wLights, "light")
            wLight.set('name', eachLight)
            
            for eachMember in dLights[eachLight]:
                wMember = ET.SubElement(wLight, "member")
                wMember.set('name', eachMember)
    
        asslight = ET.ElementTree(wRoot)
        asslight.write(oFB.FilePathName)
        cleanWriteXML(oFB.FilePathName)

        XSIUIToolkit.MsgBox("Export Successful!",c.siMsgOkOnly,"ET AssLight Export")
        
        return True

    except Exception, e:
        printException(e, sys.exc_info()[2])
        
        return False


# ================
# Partition Utils
# ================
def ET_CopyObjPartitions_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = "Copies partitions from picked source object"
    oCmd.Tooltip = "Copies partitions from picked source object"
    oCmd.ReturnValue = False

    return True


def ET_CopyObjPartitions_Execute():

    oSrcBuffer = xsi.PickElement("geometry", 'Pick Source Object', 'Pick Source Object')
    oTgtBuffer = xsi.PickElement("geometry", 'Pick Target Object', 'Pick Target Object')
    
    oTgt = oTgtBuffer('PickedElement')
    oSrc = oSrcBuffer('PickedElement')

    lPart = []
    collPasses = xsi.ActiveProject.ActiveScene.Passes
    for eachPass in collPasses:
        
        collPartitions = eachPass.Partitions        
        for eachPart in collPartitions:
                    
            if eachPart.IsMember(oSrc):                         
                lPart.append(eachPart.FullName)

    for eachItem in lPart:
        oPartition = xsi.Dictionary.GetObject(eachItem,False)
        oPartition.AddMember(oTgt)
    
    return True


def ET_NewGlobalPartition_Init(in_ctxt):
    oCmd = in_ctxt.Source
    oCmd.Description = "Adds selected objects to a partition for all passes"
    oCmd.ReturnValue = False
    
    return True


def ET_NewGlobalPartition_Execute():

    try:
        oScnRoot = xsi.ActiveProject2.ActiveScene.Root
        oProp = oScnRoot.AddProperty("CustomProperty", 0, "ET_GlobalPartition")
        oParam = oProp.AddParameter("NewPartitionName", 8)
        xsi.InspectObj(oProp,"","Add Global Partition",c.siModal,False)

        if oParam.Value == "":
            raise Exception("No name given, command aborted!")
            return False

        collMeshes = d("XSI.Collection")
        collLights = d("XSI.Collection")

        for eachObj in collSel:
            if eachObj.Type == "polymsh" or eachObj.Type == "surfmsh":
                collMeshes.Add(eachObj)
            elif eachObj.Type == "light":
                collLights.Add(eachObj)

        collPasses = xsi.ActiveProject.ActiveScene.Passes
        for eachPass in collPasses:
            if collMeshes.Count > 0:
                oPart = eachPass.CreatePartition(oParam.Value + "_Mesh_Partition", 1)
                oPart.AddMember(collMeshes)
        
            if collLights.Count > 0:    
                oPart = eachPass.CreatePartition(oParam.Value + "_Light_Partition", 2)
                oPart.AddMember(collLights)

        xsi.DeleteObj(oProp)
        XSIUIToolkit.MsgBox("Successfully added new global " + oParam.Value + "partition.!",c.siMsgOkOnly,"ET AssLight Import")
        
        return True
        
    except Exception, e:
        printException(e, sys.exc_info()[2])
        
        return False