"""
Copyright (c) 2009-2012, Eric Thivierge <ethivierge@gmail.com>
All rights reserved.

This file is part of ET_Tools.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the COPYRIGHT OWNER nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# ET SchematicTools

from win32com.client import constants as c
from win32com.client import Dispatch as d

xsi = Application
log = xsi.LogMessage
collSel = xsi.Selection

DEBUG = 0


def XSILoadPlugin( in_reg ):
	in_reg.Author = "Eric Thivierge"
	in_reg.Name = "ET_SchematicTools"
	in_reg.Major = 1
	in_reg.Minor = 15

	in_reg.RegisterMenu("4400","ET_schematic_Menu",False,False)
	in_reg.RegisterCommand("ET_schematic_expandNodes","ET_schematic_expandNodes")
	in_reg.RegisterCommand("ET_schematic_alignNodes","ET_schematic_alignNodes")
	#RegistrationInsertionPoint - do not remove this line

	return True


def XSIUnloadPlugin( in_reg ):
	strPluginName = in_reg.Name
	Application.LogMessage(str(strPluginName) + str(" has been unloaded."),c.siVerbose)
	return True


# ========
# Classes
# ======== 
class schemManager(object):
	
	def __init__(self):
		"""Manager for schematic nodes."""
		
		oSchematic = None

		lSchematics = [x for x in xsi.Desktop.ActiveLayout.Views if x.Name.startswith("Schematic")]
		if lSchematics:
			oSchematic = lSchematics[0]

		if not oSchematic:
			collViews = xsi.Desktop.ActiveLayout.Views
			oViewManager = [x for x in collViews if x.Type == "View Manager"][0]
			lSchematics = [x for x in oViewManager.Views if x.Type == "Schematic"]

		if not oSchematic and lSchematics:
			oSchematic = lSchematics[0]
			oSchematic = d(oSchematic)

		if not oSchematic:
			log("No Schematic View found!",4)
			return False

		self.schematic = oSchematic
		self.topNodes = self.schematic.Nodes
		self.selectedNodes = None
		self.targetNodes = []
		self.dChildNodes = {}
		self.lAvgPos = [0,0]
		
		self.getSelNodes()
		

	# ====================
	# Attribute Functions
	# ====================
	def setChildNodes(self):
		"""Gets child nodes of target nodes."""
		
		dNodeHierarchy = {}
		for eachNode in self.targetNodes:
			collNodeChildren = eachNode.Object.FindChildren2()
			self.dChildNodes[eachNode.Name] = [self.schematic.FindNode(x) for x in collNodeChildren]
		
		return True
	
	
	def getSelNodes(self):
		"""Returns selected nodes."""
		
		self.selectedNodes = [x for x in self.schematic.SelectedNodes]
		
		return True
	
	
	def setTargetNodes(self,lNodes=None):
		"""Sets target nodes to operate on."""
		
		if not lNodes:
			self.targetNodes = self.selectedNodes
		else:
			self.targetNodes = lNodes
		
		return True

	
	def setAvgPos(self):
		"""Gets the average position of the target nodes."""
		
		self.lAvgPos[0] = sum([x.UIInfo[0] for x in self.targetNodes]) / len(self.targetNodes)
		self.lAvgPos[1] = sum([x.UIInfo[1] for x in self.targetNodes]) / len(self.targetNodes)
		
		return True
	
	
	# ===============
	# Node functions
	# ===============
	def moveChildren(self,parentNode,X,Y):
		"""Moves child nodes."""

		for eachChildNode in self.dChildNodes[parentNode.Name]:
			fChildXPos = eachChildNode.UIInfo[0]
			fChildYPos = eachChildNode.UIInfo[1]
			eachChildNode.Move(fChildXPos + X,fChildYPos + Y)


	def spreadNodes(self,fMultiplier,bMoveChildren=True):
		"""Spread or collapse nodes from average position."""
		
		for eachNode in self.targetNodes:
			fXPos = eachNode.UIInfo[0]
			fYPos = eachNode.UIInfo[1]
			
			fNodeRelPosX = fXPos - self.lAvgPos[0]
			fNodeRelPosXScaled = fNodeRelPosX * fMultiplier
			fNewNodePosX = self.lAvgPos[0] + fNodeRelPosXScaled
			
			fNodeRelPosY = fYPos - self.lAvgPos[1]
			fNodeRelPosYScaled = fNodeRelPosY * fMultiplier
			fNewNodePosY = self.lAvgPos[1] + fNodeRelPosYScaled
			
			eachNode.Move(fNewNodePosX,fNewNodePosY)
			
			fChildMovePosX = fNewNodePosX - fXPos
			fChildMovePosY = fNewNodePosY - fYPos
			
			if bMoveChildren:
				self.moveChildren(eachNode,fChildMovePosX,fChildMovePosY)

				"""
				for eachChildNode in self.dChildNodes[eachNode.Name]:
					fChildXPos = eachChildNode.UIInfo[0]
					fChildYPos = eachChildNode.UIInfo[1]
					eachChildNode.Move(fChildXPos + fChildMovePosX,fChildYPos + fChildMovePosY)
				"""

		return True
	

	def alignNodes(self,bAlignX=False,bAlignY=False,bMoveChildren=False):
		"""Aligns nodes to first selected node."""

		fNewXPos = self.selectedNodes[0].UIInfo[0]
		fNewYPos = self.selectedNodes[0].UIInfo[1]

		for eachNode in self.targetNodes:

			fChildMovePosX = fNewXPos - eachNode.UIInfo[0]
			fChildMovePosY = fNewYPos - eachNode.UIInfo[1]

			if bAlignX:
				eachNode.Move(fNewXPos,eachNode.UIInfo[1])

				if bMoveChildren:
					self.moveChildren(eachNode,fChildMovePosX,0)

			if bAlignY:
				eachNode.Move(eachNode.UIInfo[0],fNewYPos)

				if bMoveChildren:
					self.moveChildren(eachNode,0,fChildMovePosY)


# ==========
# Menu
# ==========
def ET_schematic_Menu_Init( in_ctxt ):
	oMenu = in_ctxt.Source

	oETSchemMenu = oMenu.AddSubMenu("ET_SchematicTools")
	oETSchemMenu.AddCallbackItem("Expand Positions","expandNodes")
	oETSchemMenu.AddCallbackItem("Collapse Positions","collapseNodes")
	oETSchemMenu.AddCallbackItem("Align X","alignNodesX")
	oETSchemMenu.AddCallbackItem("Align Y","alignNodesY")
	return True


# =============
# Expand Nodes
# =============
def ET_schematic_expandNodes_Init( in_ctxt ):
	oCmd = in_ctxt.Source

	oCmd.Description = ""
	oCmd.ReturnValue = False

	oArgs = oCmd.Arguments
	oArgs.Add("multiplier",c.siArgumentInput,1.1)
	oArgs.Add("bMoveChildren",c.siArgumentInput,True)
	return True


def ET_schematic_expandNodes_Execute( multiplier, bMoveChildren ):
	
	oSchemMan = schemManager()
	oSchemMan.setTargetNodes()
	oSchemMan.setAvgPos()
	oSchemMan.setChildNodes()
	
	oSchemMan.spreadNodes(multiplier, bMoveChildren)
		
	return True


def expandNodes( in_ctxt ):
	keyboardState = xsi.GetKeyboardState()(1)

	if keyboardState == 2:
		bMoveChildren = True
	else:
		bMoveChildren = False
		
	xsi.ET_schematic_expandNodes(1.10, bMoveChildren)
	
	return


def collapseNodes( in_ctxt ):
	keyboardState = xsi.GetKeyboardState()(1)

	if keyboardState == 2:
		bMoveChildren = True
	else:
		bMoveChildren = False
		
	xsi.ET_schematic_expandNodes(0.90, bMoveChildren)
	
	return


# ============
# Align Nodes
# ============
def ET_schematic_alignNodes_Init( in_ctxt ):
	oCmd = in_ctxt.Source
	oCmd.Description = ""
	oCmd.ReturnValue = False

	oArgs = oCmd.Arguments
	oArgs.Add("bAlignX",c.siArgumentInput,False)
	oArgs.Add("bAlignY",c.siArgumentInput,False)
	oArgs.Add("bMoveChildren",c.siArgumentInput,False)
	return True


def ET_schematic_alignNodes_Execute( bAlignX, bAlignY, bMoveChildren ):
	
	oSchemMan = schemManager()
	oSchemMan.setTargetNodes()
	oSchemMan.setChildNodes()
	
	oSchemMan.alignNodes(bAlignX, bAlignY, bMoveChildren)
		
	return True


def alignNodesX( in_ctxt ):
	keyboardState = xsi.GetKeyboardState()(1)

	if keyboardState == 2:
		bMoveChildren = True
	else:
		bMoveChildren = False
		
	xsi.ET_schematic_alignNodes(True, False, bMoveChildren)
		
	return True


def alignNodesY( in_ctxt ):
	keyboardState = xsi.GetKeyboardState()(1)

	if keyboardState == 2:
		bMoveChildren = True
	else:
		bMoveChildren = False
		
	xsi.ET_schematic_alignNodes(False, True, bMoveChildren)
		
	return True